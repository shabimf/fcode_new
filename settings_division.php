<?php
include "db/connect.php";
$obj = new database();

if($obj->isAdmin() == false) {
    header("location:" . $obj->cloud_url);
    exit;
}

$page = "settings";

if(isset($_POST['add_sub_division'])){

	
	$data = array();
	$data['name'] 	= $obj->escape($_POST['name']);
	$data['division_id'] 	= $_POST['division_id'];
    $data['show_item_name'] = ($_POST['show_item_name']?$_POST['show_item_name']:0);
    $data['is_fcode'] 	= $obj->escape($_POST['status']);
    $obj->con->autocommit(false);
    $error = [];
    if ($obj->check_if_already_exist_master_name("mis_sub_division",$data['name'],$_POST['id'])>0) {
        $error[] = $data['name']." already exists";
    }  else {
        if(isset($_POST['id'])) {
            $obj->update_data("mis_sub_division", $data, $_POST['id']);
            if($_POST['sub_ids']) {
                $sub_ids = trim($_POST['sub_ids'],",");
                $obj->delete_where("fcode_table_code", "WHERE id IN (".$sub_ids.")");
            }
            if($_POST['sub_alt_ids']) {
                $sub_alt_ids = trim($_POST['sub_alt_ids'],",");
                $obj->delete_where("fcode_table_alternative_name", "WHERE id IN (".$sub_alt_ids.")");
            }
            $udata['f_table_val'] = $udata_alt['f_table_val'] = $_POST['id'];
        } else {
            $udata['f_table_val'] = $udata_alt['f_table_val'] = $obj->insert_data("mis_sub_division", $data);	
        }
    
        $flag = 0;
        for ($i=0;$i<count($_POST['focus_code']);$i++) {
            $udata['f_table_id'] = 2;
            if ($_POST['focus_code'][$i]) {
                $udata['focus_code'] = $_POST['focus_code'][$i];
                $udata['is_primary'] = ($_POST['is_primary'][$i]?(int)$_POST['is_primary'][$i]:0);
                if($udata['is_primary'] == 1 && $flag == 0 ) $flag = 1;
                if($_POST['focus_code_id'][$i]>0) {
                    $focus_code_id =  $_POST['focus_code_id'][$i];
                    $obj->update_data("fcode_table_code", $udata, $_POST['focus_code_id'][$i]);
                } else {
                    $focus_code_id =  $obj->insert_data("fcode_table_code", $udata);	
                }
                if ($i==0) $fid = $focus_code_id;
            } 
            
        }


        for ($i=0;$i<count($_POST['alternative_name']);$i++) {
            $udata_alt['name'] = $_POST['alternative_name'][$i];
            $udata_alt['f_table_id'] = 2;
            if ($udata_alt['name']) {
                if ($_POST['alternative_name_id'][$i]>0) {
                    $obj->update_data("fcode_table_alternative_name", $udata_alt, $_POST['alternative_name_id'][$i]);
                } else {
                    $obj->insert_data("fcode_table_alternative_name", $udata_alt);	
                }
            }
        
        }
        if ($flag == 0) {
            $udata_code['is_primary'] = 1;
            $obj->update_data("fcode_table_code", $udata_code, $fid);
        }
    }
    if (count($error) == 0 ) {
        $obj->con->commit();
    } else {
        setcookie("error_sub", json_encode($error), time() + 1);  
    }

    
	header("location:settings_division");	
	exit;

}
if(isset($_POST['add_div'])){
    $data = array();
    $obj->con->autocommit(false);
	$data['name'] = $_POST['name'];
    $error = [];
    if ($obj->check_if_already_exist_master_name("mis_division",$data['name'],$_POST['id'])>0) {
        $error[] = $data['name']." already exists";
    }  else {
        if(isset($_POST['id'])) {
            $obj->update_data("mis_division", $data, $_POST['id']);
            if($_POST['div_ids']) {
                $div_idss = trim($_POST['div_ids'],",");
                $obj->delete_where("fcode_table_code", "WHERE id IN (".$div_idss.")");
            }
            if($_POST['div_alt_ids']) {
                $div_alt_ids = trim($_POST['div_alt_ids'],",");
                $obj->delete_where("fcode_table_alternative_name", "WHERE id IN (".$div_alt_ids.")");
            }
            $udata['f_table_val'] = $udata_alt['f_table_val'] = $_POST['id'];
        }
        $flag = 0;
        for ($i=0;$i<count($_POST['focus_code']);$i++) {
            $udata['f_table_id'] = 1;
            if ($_POST['focus_code'][$i]) {
                $udata['focus_code'] = $_POST['focus_code'][$i];
                $udata['is_primary'] = ($_POST['is_primary'][$i]?(int)$_POST['is_primary'][$i]:0);
                if($udata['is_primary'] == 1 && $flag == 0 ) $flag = 1;
                if($_POST['focus_code_id'][$i]>0) {
                    $focus_code_id =  $_POST['focus_code_id'][$i];
                    $obj->update_data("fcode_table_code", $udata, $_POST['focus_code_id'][$i]);
                } else {
                    $focus_code_id =  $obj->insert_data("fcode_table_code", $udata);	
                }
                if ($i==0) $fid = $focus_code_id;
            } 
            
        }
        if ($flag == 0) {
            $udata_code['is_primary'] = 1;
            $obj->update_data("fcode_table_code", $udata_code, $fid);
        }
        for ($i=0;$i<count($_POST['alternative_name']);$i++) {
            $udata_alt['name'] = $_POST['alternative_name'][$i];
            $udata_alt['f_table_id'] = 1;
            if ($udata_alt['name']) {
                
                if ($_POST['alternative_name_id'][$i]>0) {
                    $obj->update_data("fcode_table_alternative_name", $udata_alt, $_POST['alternative_name_id'][$i]);
                } else {
                    $obj->insert_data("fcode_table_alternative_name", $udata_alt);	
                }
        
            }
        
        }
    }
    if (count($error) == 0 ) {
        $obj->con->commit();
    } else {
        setcookie("error_div", json_encode($error), time() + 1);  
    }
    header("location:settings_division");	
	exit;
}

$result_code = $result_div_code = $result_name = "";
if(isset($_REQUEST['edit_sub_div'])){
    $ed_sub_div = $obj->select_all_by_id("mis_sub_division", $_REQUEST['edit_sub_div']);
    $result_code = $obj->select_all_by("fcode_table_code","f_table_id='2' AND f_table_val='".$_REQUEST['edit_sub_div']."'");
    $result_name  = $obj->select_all_by("fcode_table_alternative_name","f_table_id='2' AND f_table_val='".$_REQUEST['edit_sub_div']."'");
    if (mysqli_num_rows($result_name) == 0) {
        $result_name = "";
    }
}

if(isset($_REQUEST['edit_div'])){
    $ed_div_data = $obj->select_all_by_id("mis_division", $_REQUEST['edit_div']);
    $result_div_code = $obj->select_all_by("fcode_table_code","f_table_id='1' AND f_table_val='".$_REQUEST['edit_div']."'");
    $result_name  = $obj->select_all_by("fcode_table_alternative_name","f_table_id='1' AND f_table_val='".$_REQUEST['edit_div']."'");
    if (mysqli_num_rows($result_name) == 0) {
        $result_name = "";
    }
}   



?>

<!doctype html>
<html lang="en">
<head>

<? include "common/js_n_cs.php";?>


</head>
<body>
<?php include "common/header.php";?>
<div class="container-fluid body_bg" >
  <div class="d-flex flex-row">

    <?php include "common/nav.php";?>

    <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"  data-simplebar>
      <div class="col-sm-12 col-xs-12 inner-pad" >

      <? include "common/title_bar.php";?>

      <? include "common/settings_sub_nav.php";?>


        <div class="row">

            <div class="col-sm-5 col">
                <h5 class="text-white">DIVISION</h5>
                <hr>
                <?
                if(isset($_COOKIE['error_div'])) {
                    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
                    foreach (json_decode($_COOKIE['error_div']) as $errors) {
                        echo $errors ."<br/>";
                    }
                    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>';
                } 
                if(isset($_REQUEST['edit_div'])) {  
                ?>
                <form action="" method="post">
                        
                        <div class="form-group">
                        <label>NAME</label>
                        <input type="text" class="form-control form-control-sm" name="name"  readonly <? if(isset($ed_div_data)){ ?> value="<?=$ed_div_data['name']?>" <? } ?> required>
                        </div>  
                        <?php
                        if(isset($_REQUEST['edit_div']) && $result_name){
                                $i = 1;
                                while($row_name = $result_name->fetch_assoc()){ 
                                    ?>
                                    <div class="form-group <?php echo ($i==1?"after-add-div-name-more":"")?> ">
                                        <div class="add_name_txt">
                                            <label>ALTERNATIVE NAME</label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control form-control-sm" name="alternative_name[]"  <? if(isset($ed_div_data)){ ?> value="<?=$row_name['name']?>" <? } ?> >
                                                <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  <? if(isset($ed_div_data)){ ?> value="<?=$row_name['id']?>" <? } ?>>
                                                <?php 
                                                if($i==1) {
                                                ?>
                                                <div class="input-group-append"  onclick="addAlternativeMore('after-add-div-name-more');">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                                </div>
                                                <?php
                                                } else {
                                                ?>
                                                <div class="input-group-append remove_alt" data-code="<?=$row_name['id']?>" data-ids="div_alt_ids">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>  
                                    </div>
                                    <?php
                                $i++;
                            }
                            
                        } else {
                        ?>    
                        <div class="form-group after-add-sub-name-more">
                            <label>ALTERNATIVE NAME</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control form-control-sm" name="alternative_name[]">
                                <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  value="0">
                                <div class="input-group-append"  onclick="addAlternativeMore('after-add-sub-name-more');">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                </div>
                            </div>
                        </div> 
                        <?php
                        }
                       
                        if($result_div_code){
                            $i = 1;
                            while($row_code = $result_div_code->fetch_assoc()){ 
                                ?>
                                <div class="row add_txt <?php echo ($i==1?"after-div-add-more":"")?>">
                                    <div class="col-sm-8"> 
                                        <div class="form-group">
                                            <label>F CODE</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control form-control-sm" name="focus_code[]"  <? if(isset($ed_div_data)){ ?> value="<?=$row_code['focus_code']?>" <? } ?> required>
                                                <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  <? if(isset($ed_div_data)){ ?> value="<?=$row_code['id']?>" <? } ?>>
                                                <?php 
                                                if($i==1) {
                                                ?>
                                                <div class="input-group-append"  onclick="addMore('after-div-add-more');">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                                </div>
                                                <?php
                                                } else {
                                                ?>
                                                <div class="input-group-append remove" data-code="<?=$row_code['id']?>" data-ids="div_ids">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label> IS PRIMARY </label>
                                            <div class="input-group">
                                                <input type="radio" class="is_primary" name="is_primary_flag"  value="1" <? if(isset($ed_div_data) && $row_code['is_primary'] == 1){ echo "checked";} ?>>
                                                <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  <? if(isset($ed_div_data)  && $row_code['is_primary'] == 1 ){ ?> value="1" <? } ?> >
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                
                                <?php
                            $i++;
                            }
                        } else {
                        ?> 
                        <div class="row after-div-add-more">
                            <div class="col-sm-8"> 
                                <div class="form-group">
                                    <label>F CODE</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control form-control-sm" name="focus_code[]"  required="required">
                                        <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  value="0">
                                        <div class="input-group-append"  onclick="addMore('after-div-add-more');">
                                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                        </div>
                                    </div>
                                </div> 
                            </div> 
                            <div class="col-sm-4">
                            <div class="form-group">
                                    <label> IS PRIMARY </label>
                                    <div class="input-group">
                                        <input type="radio" class="is_primary" name="is_primary_flag"  value="1">
                                        <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  value="0">
                                    </div>
                                </div>
                            </div>
                        </div>  
                       
                        <?php } ?>
                       
                        <input type="hidden" name="div_ids" id="div_ids"/>    
                        <input type="hidden" name="div_alt_ids" id="div_alt_ids"/>    
                        
                        <div class="form-group">
                        <input type="hidden" name="id" value="<?=$ed_div_data['id']?>">
                        <button name="add_div" type="submit" class="btn btn-info">UPDATE</button>
                        <a href="settings_division" class="btn btn-secondary">CANCEL</a>
                        </div> 
                        
                    </form>
                <?php 
                  }
                ?>
                    <table class="table table-dark table-bordered" data-table="mis_division" >
                        <thead>
                            <th>#</th>
                            <th>NAME</th>
                            <th>ALTERNATIVE NAME</th>
                            <th>CODE</th>
                            <th>ACTION</th>
                        </thead>
                        <tbody>
                            <? $i=0;
                                //$result = $obj->select_all("mis_division");
                                $result = $obj->select_all_table("mis_division", 1, NULL);
                                while($row = $result->fetch_assoc()){ 
                                    $focus_code_arr = explode(",",$row['code_id']);
                                    $alt_name_arr = ($row['alt_name_id']?explode(",",$row['alt_name_id']):[]);
                                ?>
                                <tr data-id="<?=$row['id']?>">
                                    <td><?=++$i?></td>
                                    <td><?=$row['name']?></td>
                                    <td> 
                                        <? 
                                            if (isset($alt_name_arr)) {
                                            foreach($alt_name_arr as $name_id) { 
                                                $alt_name_result = $obj->select_all_by_field_id_process('fcode_table_alternative_name','id',$name_id);
                                                echo $alt_name_result['name']."<br/>";
                                            }  
                                            }
                                                
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        foreach($focus_code_arr as $code_id) {     
                                            $fcode_result = $obj->select_all_by_field_id_process('fcode_table_code','id',$code_id);
                                            ?>
                                            <div class="codecls_<?=$fcode_result['id']?>">
                                            <input type="text" class="full_box code_input ev hide txt_cls_<?=$fcode_result['id']?>" id="txt_val_<?=$fcode_result['id']?>" value="<?=$fcode_result['focus_code']?>"> 
                                            <span class="cv f_code_<?=$fcode_result['id']?> txt_edit_cls_<?=$fcode_result['id']?>">
                                              <?=$fcode_result['focus_code']?>
                                              <? if($fcode_result['is_primary']  && count($focus_code_arr)>1) echo '<span class="badge badge-primary">Primary</span>'; ?>
                                            </span>
                                            <i class="fas fa-pen fa-xs cp float-right ev txt_edit_cls_<?=$fcode_result['id']?>" data-toggle='[".cv",".ev"]'  onclick="editable(<?=$fcode_result['id']?>)"></i>
                                            <span class="cv float-right  hide txt_cls_<?=$fcode_result['id']?>" data-toggle='[".cv",".ev"]'>
                                                <button class="btn btn-xs btn-secondary  mr-2 " onclick="noteditable(<?=$fcode_result['id']?>)">CANCEL</button>
                                                <button class="btn btn-xs btn-primary" onclick="updateTextbox(<?=$fcode_result['id']?>)">UPDATE</button>
                                            </span> 
                                            </div>
                                        <?php
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <a href="?edit_div=<?=$row['id']?>">EDIT</a>
                                    </td>
                                </tr>
                            <? } ?>
                        </tbody>
                    </table>
            </div>


            <div class="col-sm-7 col text-white">
                <h5 >SUB DIVISION</h5>
                <hr>
                <?
                    if(isset($_COOKIE['error_sub'])) {
                        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
                        foreach (json_decode($_COOKIE['error_sub']) as $errors) {
                    
                            echo $errors ."<br/>";
                    }
                    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>';
                    } 
                ?>
                    <form action="" method="post">
                    
                        <div class="form-group">
                          <label>NAME</label>
                          <input type="text" class="form-control form-control-sm" name="name"  <? if(isset($ed_sub_div)){ ?> value="<?=$ed_sub_div['name']?>" readonly <? } ?> required>
                        </div>    
                        <?php
                        if(isset($_REQUEST['edit_sub_div']) && $result_name){
                                $i = 1;
                                while($row_name = $result_name->fetch_assoc()){ 
                                    ?>
                                    <div class="form-group <?php echo ($i==1?"after-add-sub-name-more":"")?> ">
                                        <div class="add_name_txt">
                                            <label>ALTERNATIVE NAME</label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control form-control-sm" name="alternative_name[]"  <? if(isset($ed_sub_div)){ ?> value="<?=$row_name['name']?>" <? } ?> >
                                                <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  <? if(isset($ed_sub_div)){ ?> value="<?=$row_name['id']?>" <? } ?>>
                                                <?php 
                                                if($i==1) {
                                                ?>
                                                <div class="input-group-append"  onclick="addAlternativeMore('after-add-sub-name-more');">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                                </div>
                                                <?php
                                                } else {
                                                ?>
                                                <div class="input-group-append remove_alt" data-code="<?=$row_name['id']?>" data-ids="sub_alt_ids">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>  
                                    </div>
                                    <?php
                                $i++;
                            }
                            
                        } else {
                        ?>    
                        <div class="form-group after-add-sub-name-more">
                            <label>ALTERNATIVE NAME</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control form-control-sm" name="alternative_name[]">
                                <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  value="0">
                                <div class="input-group-append"  onclick="addAlternativeMore('after-add-sub-name-more');">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                </div>
                            </div>
                        </div> 
                        <?php
                        }
                        ?>
                        <div class="copy_alt hide">  
                            <div class="add_name_txt">
                                <label>ALTERNATIVE NAME</label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control form-control-sm" name="alternative_name[]">
                                    <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  value="0">
                                    <div class="input-group-append remove_alt">
                                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="form-group">
                          <label> DIVISION</label>
                          <select class="form-control form-control-sm"  name="division_id" required <? if(isset($ed_sub_div)) { echo "readonly"; ?> style="pointer-events:none;"<?}?>>
                              <option value="">SELECT DIVSION </option>
                              <?
                                $result_div = $obj->select_all("mis_division");
                                while($div = $result_div->fetch_assoc()){ ?>
                                <option <? if(isset($ed_sub_div) && $ed_sub_div['division_id'] == $div['id']){  echo "selected";  } ?> value="<?=$div['id']?>">
                                    <?=$div['name']?>
                                </option>
                              <? } ?>
                          </select>
                        </div> 
                        
                        <?php
                            if(mysqli_num_rows($result_code)>0){
                                $i = 1;
                                while($row_code = $result_code->fetch_assoc()){ 
                                    ?>
                                    <div class="row add_txt <?php echo ($i==1?"after-sub-div-add-more":"")?>">
                                        <div class="col-sm-8"> 
                                            <div class="form-group">
                                                <label>F CODE</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control form-control-sm" name="focus_code[]"  <? if(isset($ed_sub_div)){ ?> value="<?=$row_code['focus_code']?>" <? } ?> >
                                                    <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  <? if(isset($ed_sub_div)){ ?> value="<?=$row_code['id']?>" <? } ?>>
                                                    <?php 
                                                    if($i==1) {
                                                    ?>
                                                    <div class="input-group-append"  onclick="addMore('after-sub-div-add-more');">
                                                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                                    </div>
                                                    <?php
                                                    } else {
                                                    ?>
                                                    <div class="input-group-append remove" data-code="<?=$row_code['id']?>" data-ids="sub_ids">
                                                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div> 
                                        </div> 
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label> IS PRIMARY </label>
                                                <div class="input-group">
                                                    <input type="radio" class="is_primary" name="is_primary_flag"  value="1" <? if(isset($ed_sub_div) && $row_code['is_primary'] == 1){ echo "checked";} ?>>
                                                    <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  <? if(isset($ed_sub_div) && $row_code['is_primary'] == 1){ ?> value="1" <? } ?>  >
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                   
                                    <?php
                                $i++;
                                }
                            } else {
                            ?>  
                            <div class="row after-sub-div-add-more">
                                    <div class="col-sm-8"> 
                                        <div class="form-group">
                                            <label>F CODE</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control form-control-sm" name="focus_code[]"  >
                                                <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  value="0">
                                                <div class="input-group-append"  onclick="addMore('after-sub-div-add-more');">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="col-sm-4">
                                    <div class="form-group">
                                            <label> IS PRIMARY </label>
                                            <div class="input-group">
                                                <input type="radio" class="is_primary" name="is_primary_flag"  value="1">
                                                <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  value="0">
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                        <? }?>
                        <div class="copy hide">  
                            <div class="row add_txt">
                                <div class="col-sm-8"> 
                                    <div class="form-group">
                                        <label>F CODE</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-sm" name="focus_code[]">
                                            <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  value="0">
                                            <div class="input-group-append remove">
                                                <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                            </div>
                                        </div>
                                    </div> 
                                </div> 
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label> IS PRIMARY </label>
                                        <div class="input-group">
                                            <input type="radio" class="is_primary" name="is_primary_flag"  value="1">
                                            <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  id="is_primary" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>  
  
                        <div class="form-group">
                            <label>STATUS</label> <br>
                            <select class="form-control form-control-sm" name="status" id="status" required="required">
                                <option value="">Select Status</option>
                                <option value="1" <? if(isset($ed_sub_div) && $ed_sub_div['is_fcode']==1){ ?> selected <? } ?>>Active</option>
                                <option value="0" <? if(isset($ed_sub_div) && $ed_sub_div['is_fcode']==0){ ?> selected <? } ?>>Inactive</option>
                            </select>
                        </div>  
                        <div class="form-group">
                            <label>Show Item Name</label>             
                            <input class="f_input" type="checkbox" value="1" name="show_item_name" id="show_item_name"  <? if(isset($ed_sub_div) && $ed_sub_div['show_item_name'] == 1){ ?> checked <? } ?>/>
                        </div> 
                        <div class="form-group mt-2">
                          <? if(isset($ed_sub_div)){ ?>
                          <input type="hidden" name="id" value="<?=$ed_sub_div['id']?>">
                          <button name="add_sub_division" type="submit" class="btn btn-info">UPDATE</button>
                          <? }else{ ?>
                          <button name="add_sub_division" type="submit" class="btn btn-info">ADD</button>
                          <? } ?>
                          <input type="hidden" name="sub_alt_ids" id="sub_alt_ids"> 
                          <input type="hidden" name="sub_ids" id="sub_ids">
                          <a href="settings_division" class="btn btn-secondary">CANCEL</a>
                          
                        </div> 
                        
                    </form>

                    <table class="table table-dark table-bordered" data-table="mis_sub_division" id="api_sub_division">
                        <thead>
                            <th>#</th>
                            <th>NAME</th>
                            <th>ALTERNATIVE NAME</th>
                            <th>DIVISION</th>
                            <th>CODE</th>
                            <th>STATUS</th>
                            <th class="tableexport-ignore">ACTION</th>
                        </thead>
                        <tbody>
                            <? $i=0;
                                $result = $obj->select_sub_divisions(2);
                                while($row = $result->fetch_assoc()){ 
                                    $focus_code_arr = ($row['code_id']?explode(",",$row['code_id']):[]);
                                    $alt_name_arr = ($row['alt_name_id']?explode(",",$row['alt_name_id']):[]);
                                ?>
                                <tr data-id="<?=$row['id']?>">
                                    <td><?=++$i?></td>
                                    <td><?=$row['name']?></td>
                                    <td>
                                    <? 
                                             $hasComma = false;
                                              if (isset($alt_name_arr)) {
                                                foreach($alt_name_arr as $name_id) { 
                                                    $alt_name_result = $obj->select_all_by_field_id_process('fcode_table_alternative_name','id',$name_id);
                                                    if ($hasComma){ 
                                                        echo ","; 
                                                    }
                                                    echo '<span>'.ucfirst($alt_name_result['name']).'</span>';
                                                    $hasComma=true;
                                                }  
                                              }
                                                
                                            ?>
                                    </td>
                                    <td> <small> <?=$row['division_name']?> </small> </td>
                                    
                                    <? 
                                            $code_arr = [];
                                            foreach($focus_code_arr as $code_id) {  
                                                $fcode_result = $obj->select_all_by_field_id_process('fcode_table_code','id',$code_id);
                                                $primary = "";
                                                if($fcode_result['is_primary'] && count($focus_code_arr)>1) $primary='(Primary)';
                                                $code_arr[$fcode_result['id']] = $fcode_result['focus_code'].$primary;
                                            }  
                                           
                                            if (isset($code_arr)) {
                                              echo '<td class="hide">'.implode(',',$code_arr).'</td>';
                                            }
                                                
                                        ?><td  class="tableexport-ignore"> 
                                             <?php
                                           
                                            foreach($code_arr as $cid => $cname) {  
                                                $carr = explode("(",$cname);  
                                            ?>
                                            
                                            <div class="codecls_<?=$cid?> tableexport-empty">
                                            <input type="text" class="full_box code_input ev hide txt_cls_<?=$cid?>" id="txt_val_<?=$cid?>" value="<?=$carr[0]?>"> 
                                            <span class="cv f_code_<?=$cid?> txt_edit_cls_<?=$cid?>">
                                               <?=$cname?>
                                             
                                            </span>
                                            <i class="fas fa-pen fa-xs cp float-right ev txt_edit_cls_<?=$cid?>" data-toggle='[".cv",".ev"]'  onclick="editable(<?=$cid?>)"></i>
                                            <span class=" cv float-right  hide txt_cls_<?=$cid?>" data-toggle='[".cv",".ev"]'>
                                                <button class="btn btn-xs btn-secondary  mr-2 " onclick="noteditable(<?=$cid?>)">CANCEL</button>
                                                <button class="btn btn-xs btn-primary " onclick="updateTextbox(<?=$cid?>)">UPDATE</button>
                                            </span> 
                                            </div>
                                        <?php
                                            }
                                        
                                        ?>
                                        
                                    </td>
                                    <td>
                                        <span class="badge <?=($row['is_fcode']==1?"bg-success":"bg-danger")?> text-white">
                                            <?=($row['is_fcode']==1?"ACTIVE":"INACTIVE")?>
                                        </span>
                                    </td>
                                    <td class="tableexport-ignore">
                                        <a href="?edit_sub_div=<?=$row['id']?>">EDIT</a>
                                    </td>
                                </tr>
                            <? } ?>
                        </tbody>
                    </table>
            </div>

        </div>





      </div>
      <? include "common/up_icon.php";?>
    </div>
  </div>
</div>
<script src="js/FileSaver.min.js"></script>
<script src="js/xls.core.min.js"></script>
<script src="js/tableexport.js"></script>
<script>
$(document).ready(function(){
    $("#api_sub_division").tableExport({ position: "top",bootstrap: true,formats: ["xlsx","csv"], trimWhitespace: true,fileName:"subdivision" ,  ignoreCSS: ".tableexport-ignore", bootstrap: true,emptyCSS: ".tableexport-empty"       });  
});
</script>
</body>
</html>
