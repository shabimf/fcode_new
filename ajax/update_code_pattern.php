<?
include "../db/connect.php";
$obj = new database();
$data = array();
$table_val_arr = explode(",",$_REQUEST['table_val']);
$data['f_table_id'] = $_REQUEST['table_id'];
$data['parent_id'] = $_REQUEST['parent_id'];
$inserted_count = 0;
if ($_REQUEST['type'] == "code") $table = "fcode_pattern_code";
else $table = "fcode_pattern_name";
$actual_count = count($table_val_arr); 
foreach ($table_val_arr as $val) {
    if ($val) {
        $data['f_table_val'] = $val;
        $id = $obj->insert_data($table, $data);
        if ($id) {
            $inserted_count ++;
        }   
    } 
}
if($inserted_count > 0) {
  echo "Success";
} else {
  echo "Error";
}
?>