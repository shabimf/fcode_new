<?
include "../db/connect.php";
$obj = new database();
$id = $_REQUEST['id'];
$row = $obj->select_all_by_id('fcode_pattern',$id,NULL);
$pattern_arr = $pattern_descr_arr = $required_arr = [];
if ($row['pattern_ids']) {
    $pattern_arr = explode(',',$row['pattern_ids']);
} if ($row['description_ids']) {
    $pattern_descr_arr = explode(',',$row['description_ids']);
} if ($row['required_ids']) {
    $required_arr = json_decode($row['required_ids']);
}

if (count($pattern_arr)>0 || count($pattern_descr_arr)>0) {
    $patterns = array_unique(array_merge($pattern_arr,$pattern_descr_arr), SORT_REGULAR);
    $patterns_ss = array_diff( $patterns, [1,2] ); 
    ?>
    <table class="table table-bordered table-dark table-hover">
        <thead>
            <th>NAME</th>
            <th>REQUIRED</th>
        </thead>
        <tbody id = "itemForm">
        <?
        foreach($patterns_ss as $val) {
            $name = ($val?$obj->select_field_by_id('fcode_table','name',$val):"MANUAL ENTRY");
            echo "<tr>";
            echo "<td>".$name."</td>";
            echo '<td><input type="checkbox" name="locationthemes" class="f_input required_cls"  value="'.$val.'"' ;
            if (in_array($val, $required_arr)) echo "checked";
            echo '></td>';
            echo "</tr>";
        }
        
        ?>
        </tbody>
    </table>
    <button type="button" class="btn btn-primary float-right" onclick="saveRequired(<?=$id?>)" >Save</button>
<?
}

?>