<?php
include "../db/connect.php";
$obj = new database();
if($_REQUEST['action'] == 'add') {
    unset($_POST['add']);
    unset($_POST['action']);
    unset($_POST['id']);
    $where = "email='".$_POST['email']."'";
    $_POST['created_by'] = $_SESSION['user_id'];
    $data = $_POST;
    if($obj->cloud_users_isexists($_POST['cloud_id'])>0) {
        $msg = array("status"=>0,"class"=>"error","message"=>"Cloud ID already exists.");
    } elseif($obj->duplicate_check_where("fcode_user",$where)>0) {
        $msg = array("status"=>0,"class"=>"error","message"=>"Email ID already exists.");
    }  else {
        $obj->insert_data("fcode_user", $data);
        $msg = array("status"=>1,"class"=>"success","message"=>"User was successfully added.");
    }
    echo json_encode($msg);die;
} else if($_REQUEST['action'] == 'edit') {
    $row = $obj->select_all_by_id('fcode_user',$_POST['userId']);
    echo json_encode($row);
    exit;
} else if($_REQUEST['action'] == 'update') {
    $id = $_POST['id'];
    unset($_POST['add']);
    unset($_POST['action']);
    unset($_POST['id']);
    $_POST['new_request'] = (isset($_POST['new_request'])?1:0);
    $_POST['manage_request'] = (isset($_POST['manage_request'])?1:0);
    $_POST['manage_fcode'] = (isset($_POST['manage_fcode'])?1:0);
    $_POST['cancel_request'] = (isset($_POST['cancel_request'])?1:0);
    $data = $_POST;
    $where = "email='".$_POST['email']."' AND id!=".$id;
    if($obj->cloud_users_isexists($_POST['cloud_id'],$id)>0) {
        $msg = array("status"=>0,"class"=>"error","message"=>"Cloud ID already exists.");
    } elseif($obj->duplicate_check_where("fcode_user",$where)>0) {
        $msg = array("status"=>0,"class"=>"error","message"=>"Email ID already exists.");
    }  else {
        $obj->update_data("fcode_user", $data,$id);
        $msg = array("status"=>1,"class"=>"success","message"=>"User was successfully updated.");
    }
    echo json_encode($msg);die;
} else if($_REQUEST['action'] == 'delete') {
   $id = $_POST['id'];
   if($obj->delete_value("fcode_user",$id)) {
    $msg = array("status"=>1,"class"=>"success","message"=>"User was successfully deleted.");
   }
}
