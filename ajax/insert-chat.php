<?php
include "../db/connect.php";
$obj = new database();
$data = [];
if (isset($_POST['message']) && trim($_POST['message'])!="") {
    $data['user_id'] = $_SESSION['user_id'];
    $data['request_id'] = $_POST['request_incoming_id'];
    $data['message'] =  $_POST['message'];
    $data['is_image'] = 0;
    $obj->insert_data("fcode_messages", $data);
} 
if($_FILES['image'])
{
    $path = '../uploads/chat/';
    $valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp' , 'pdf' , 'doc' , 'ppt'); // valid 
    $img = $_FILES['image']['name'];
    $tmp = $_FILES['image']['tmp_name'];
    // get uploaded file's extension
    $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
    // can upload same image using rand function
    $final_image = rand(1000,1000000).$img;
    // check's valid format
    if(in_array($ext, $valid_extensions)) 
    { 
        $final_image = strtolower($final_image);
        $path = $path.$final_image; 
        if(move_uploaded_file($tmp,$path)) 
        {
            $data['user_id'] = $_SESSION['user_id'];
            $data['message'] =  $final_image;
            $data['request_id'] = $_POST['request_incoming_id'];
            $data['is_image'] = 1;
            $obj->insert_data("fcode_messages", $data);
        }
    } 
    else 
    {
    echo 'invalid';
    }
}