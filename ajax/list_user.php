<?php
include "../db/connect.php";
$obj = new database();
$sqlQuery = "SELECT user.*,branch.name as branch_name FROM fcode_user as user
            LEFT JOIN mis_branches as branch ON user.branch_id = branch.id ";
if(!empty($_POST["search"]["value"])){
    $sqlQuery .= 'where( user.name LIKE "%'.$_POST["search"]["value"].'%" ';
    $sqlQuery .= ' OR branch.name LIKE "%'.$_POST["search"]["value"].'%" ';	
    $sqlQuery .= ' OR user.email LIKE "%'.$_POST["search"]["value"].'%" ';				
    $sqlQuery .= ')';			
}
if(!empty($_POST["order"])){
    $sqlQuery .= ' ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
} else {
    $sqlQuery .= ' ORDER BY user.user_type ASC ';
}

$res = $obj->con->query($sqlQuery);
$totalFiltered = mysqli_num_rows($res); 
if($_POST["length"] != -1){
    $sqlQuery .= ' LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}	

$result = $obj->con->query($sqlQuery);

$sqlQuery1 = "SELECT * FROM fcode_user ";
$result1 = $obj->con->query($sqlQuery1);
$numRows = mysqli_num_rows($result1);       
$userData = array();
$i=1;	
while( $user = mysqli_fetch_assoc($result) ) {		
    $userRows = array();
    $access = "";
    if($user['new_request']==1)$access .= "NEW REQUEST,"; 
    if($user['manage_request']==1)$access .= "MANAGE REQUESTS,";	
    if($user['manage_fcode']==1)$access .= "MANAGE FCODE,";	
    if($user['cancel_request']==1)$access .= "CANCEL REQUEST,";	
    $userRows[] = $i++;
    $userRows[] = ucfirst($user['name']);
    $userRows[] =  $user['email'];	
    $userRows[] =  $user['phone_no'];			
    $userRows[] =  ($user['user_type']==1?"Admin":"Normal User");	
    $userRows[] =  $user['branch_name'];	
 
    $userRows[] =  rtrim($access, ',');;			
    $userRows[] =  ($user['status']==1?'<span class="btn btn-success btn-xs update">Active</button>':'<span class="btn btn-danger btn-xs update">Inactive</button>');
    $userRows[] = '<button type="button" name="edit" id="'.$user["id"].'" class="btn btn-primary btn-xs edit" >Edit</button> ';
    $userData[] = $userRows;
}
$output = array(
    "draw"				=>	intval($_POST["draw"]),
    "recordsTotal"  	=>  $numRows,
    "recordsFiltered" 	=> 	$totalFiltered,
    "data"    			=> 	$userData
);
echo json_encode($output);