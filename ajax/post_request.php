<?php
include "../db/connect.php";
$obj = new database();
$table_array = $_POST['f_table'];
$division_id = $table_array[1];
$sub_division_id = isset($table_array[2]) ? $table_array[2] : null;
$brand_id = ((isset($table_array[3]) && $table_array[3]!="add") ? $table_array[3] : null);
$own_brand = null;
$pattern_id = 0;
$pattern_array = [];
if($brand_id)  $own_brand = $obj->select_field_by_id("api_brand", "own_brand", $brand_id); 
if ($sub_division_id>0 && $_POST['offer']!=1) {
    $pattern_id = $obj->select_pattern_id($division_id, $sub_division_id,$brand_id);
    $pattern_array = $obj->select_table_pattern_list($division_id, $sub_division_id, $own_brand,$brand_id);
}

$obj->con->autocommit(false);
$data['pattern_id'] = ($pattern_id?$pattern_id:0);
$data['division_id'] = $division_id;
$data['sub_division_id'] = $sub_division_id;
$data['brand_id'] = $brand_id;
$data['own_brand'] = $own_brand;
$data['name'] = $_POST['name'] ?? null;
$data['name_arabic'] = $_POST['name_arabic'] ?? null;
$data['hsn'] = $_POST['hsn'] ?? null;
$data['barcode'] = $_POST['barcode'] ?? null;
$data['partno'] = $_POST['partno'] ?? null;
$data['remarks'] = $_POST['remarks'] ?? null;
$data['offer'] = $_POST['offer'] ?? 0;
$data['code'] = $_POST['newcode'] ?? null;
$data['name'] = $_POST['description'] ?? null;
if(isset($_FILES['file']['name']) && $_FILES['file']['name']!="")
{   
    $file = rand(1000,100000)."-".$_FILES['file']['name'];
    $file_loc = $_FILES['file']['tmp_name'];
    $file_size = $_FILES['file']['size'];
    $file_type = $_FILES['file']['type'];
    $folder="../requestimg/";
    $new_file_name = strtolower($file);
    $final_file=str_replace(' ','-',$new_file_name);
    if(move_uploaded_file($file_loc,$folder.$final_file))
    {
        $data['image'] = $final_file;
    }
}

if(isset($_REQUEST['request_id'])){
    $request_id = $_REQUEST['request_id'];
    $data['updated_by'] = $_SESSION['user_id'];
    $obj->update_data("fcode_request", $data, $request_id);
    $obj->delete_where("fcode_request_table", " where request_id =".$request_id);
} else{
    //$data['image'] = null;
    $data['branch_id'] = $_SESSION['branch_id'];
    $data['created_by'] = $_SESSION['user_id'];
    if ($data['offer'] ==1) {
        $_SESSION['division_id'] = "";
        $_SESSION['sub_division_id'] ="";
        $_SESSION['brand_id'] = "";
    } else {
        if($data['sub_division_id'] != "add") {
            $_SESSION['division_id'] = $data['division_id'];
            $_SESSION['sub_division_id'] = $data['sub_division_id'];
            $_SESSION['brand_id'] = $data['brand_id'];
        }
      
    }
    $data['status'] = 0;
   
    $request_id = $obj->insert_data("fcode_request", $data);
    $unique_id = "FCDE-".str_pad($request_id,7,"0", STR_PAD_LEFT);
    $filteredData=substr($_POST['img_val'], strpos($_POST['img_val'], ",")+1);
    $unencodedData=base64_decode($filteredData);
    $screenshot_data = $unencodedData;  
    $file_name = $request_id.'.png'; 
    $output_file = '../screenshots/'.$file_name; 
    file_put_contents($output_file, $unencodedData); 
    $obj->update_data("fcode_request", ['unique_id'=>$unique_id], $request_id);
}
if(count($pattern_array)>0) {
    $pattern_arr = array();
    foreach ($pattern_array as $val) {
        $data =  array();
        $data['request_id'] = $request_id;
        $data['f_table_id'] = $val=='0' ? 0 : (int) $val;
        $data['item_name']  = null;
        if ($val == 2) $data['item_name'] =  $_POST['item_name'] ?? null; 
        if (isset($_POST['field_'.$val]) && isset($table_array[$val]) && $table_array[$val] == 'add' && $val!=24) {
            $data['f_table_val'] = $_POST['field_'.$val];
            $data['is_manual'] = 1;
        } else {
            if ($val == 24  && isset($table_array[$val]) && count($table_array[$val])>0) {
                $data['f_table_val'] = $val=='0' ? $table_array[0] : (implode(',',$table_array[$val])??0);
            } else {
                $data['f_table_val'] = $val=='0' ? $table_array[0] : (int) ($table_array[$val]??0);
            }
            $data['is_manual'] = 0;
        }
        $pattern_arr[$data['f_table_id']] = $data['f_table_val'];
        $obj->insert_data("fcode_request_table", $data);
    }
   
} else  {
    $pattern_arr = array();
    foreach ($table_array as $key => $val) {
        if ($val != "") {
            $data = array();
            $data['request_id'] = $request_id;
            $data['f_table_id'] = $key;
            if (isset($_POST['field_'.$key]) && isset($table_array[$key]) && $table_array[$key] == 'add' && $val!=24) {
                $data['f_table_val'] = $_POST['field_'.$key];
                $data['is_manual'] = 1;
            } else {
                if ($val == 24  && isset($table_array[$val]) && count($table_array[$val])>0) {
                    $data['f_table_val'] = $val=='0' ? $table_array[0] : (implode(',',$table_array[$val])??0);
                } else {
                    $data['f_table_val'] = $val=='0' ? $table_array[0] : (int) ($table_array[$key]??0);
                }
                $data['is_manual'] = 0;
            }
            $pattern_arr[$data['f_table_id']] = $data['f_table_val'];
            $obj->insert_data("fcode_request_table", $data);
        }
       
    }
    
}



if(isset($_REQUEST['request_id'])){
    // $update_data['code'] = $obj->generate_fcode($request_id);
    // $update_data['name'] = $obj->generate_description($request_id,$pattern_id);
    // $obj->update_data("fcode_request", $update_data, $request_id);
    $row_r = $obj->select_all_by_id("fcode_request", $request_id);
    include "post_result.php";  
} else  {
    if($pattern_id>0)  {
        echo $obj->generate_fcode($request_id,$pattern_id)."~~".$obj->generate_description($request_id,$pattern_id); 
    } else { 
        echo $_POST['newcode']."~~".$_POST['description']; 
    }
    $obj->update_data("fcode_request", ['requested_pattern'=> serialize($pattern_arr) ], $request_id);
}

$obj->con->commit();   
?>

