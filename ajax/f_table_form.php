<?php
include "../db/connect.php";
$obj = new database();
$sub_division_id = $_REQUEST['sub_division_id'];
$division_id = ($sub_division_id>0?$obj->select_field_by_id("mis_sub_division","division_id",$sub_division_id):$_REQUEST['division_id']);
$own_brand = $_REQUEST['own_brand'];
$brand_id = (isset($_REQUEST['brand_id']) && $_REQUEST['brand_id']>0?$_REQUEST['brand_id']:0);
$offer = $_REQUEST['offer'];
$pattern_array = [];
if($offer == 1) {
 $pattern_id = 0;
} else {
  $pattern_array = $obj->select_table_pattern_list($division_id, $sub_division_id, $own_brand, $brand_id);
  $pattern_id = $obj->select_pattern_id($division_id, $sub_division_id,$brand_id);
}

$required_arr = [];
if(!empty($pattern_array)){
  $pattern_row = $obj->select_all_by_id("fcode_pattern",$pattern_id);
  if ($pattern_row['required_ids']) $required_arr = json_decode($pattern_row['required_ids']);
  ?>
    <input type="hidden" class="form-control form-control-sm" id="pattern_id" value="<?=$pattern_id?>">
    <input type="hidden" class="form-control form-control-sm" id="pattern_ids" value="<?=$pattern_row['pattern_ids']?>">
    <input type="hidden" class="form-control form-control-sm" id="description_ids" value="<?=$pattern_row['description_ids']?>">
  <?
  foreach($pattern_array as $val){
    
      if($val == "0"){?> 
        <label class="">MANUAL INPUT<?if (in_array($val, $required_arr))echo '<span class="error">*</span>';?></label>
        <input type="text" class="form-control f_input form-control-sm clearfix" name="f_table[0]" onchange="getCode()" <?if (in_array($val, $required_arr)) { echo "required"; } ?> >
        
      <? } elseif($val == "3") {
          $brand_required = 0; 
          if (in_array($val, $required_arr)) $brand_required = 1;
          echo '<input type="hidden" class="form-control form-control-sm" id="brand_required" value="'.$brand_required.'" >'; 
          echo '<input type="hidden" class="form-control form-control-sm" id="is_band" value="1" >';
        } else{ 
          $row_table = [];
          if($val>0) $row_table = $obj->select_all_by_id("fcode_table", $val, " AND id!=1 AND id!=2 AND id!=3");
          if(isset($row_table) && isset($row_table['name'])){
            $showInput = $required = "";
            $onchange = "onchange='showModelInput(this.value,".$row_table['id']."),getCode()'";
            if (in_array($val, $required_arr)) { $required ="required"; }
            $showInput = '<div class="row" ><div class="col"><div class="txtcls_'.$row_table['id'].' hide"><label class="text-danger">'.$row_table['name'].' NAME ';
            if (in_array($val, $required_arr)) {  $showInput .= '<span class="error">*</span>'; }
            $showInput .= '</label><input type="text" class="f_input add-form-control" name="field_'.$row_table['id'].'" id="field_'.$row_table['id'].'" placeholder="ENTER '.$row_table['name'].' NAME" onkeyup="getCode(),checkName(this.value,'.$row_table['id'].'); return false;" '.$required.' ><span id="confirmname_'.$row_table['id'].'" class="error_name"></span></div></div></div>';
          ?>
            <label class=""><?=$row_table['name']?><?if (in_array($val, $required_arr))echo '<span class="error">*</span>';?></label>
            <select <?=$required ?> <?php if ($val == 24) {  echo 'class="chosen-select f_input" multiple'; } else{ echo ' class="f_table_selects f_input"';}?> data-placeholder="SELECT <?=$row_table['name']?>" name="<? if ($val == 24) {?>f_table[<?=$row_table['id']?>][]<? } else {?>f_table[<?=$row_table['id']?>]<?}?>"   <? echo $onchange;?>  >
              <? if ($val != 24) { ?>  
              <option value=""></option>
              <option value="add" >Add New</option>
              <? }
                  $result = $obj->select_all($row_table['f_table'], "where status = 1");
                  while($row = $result->fetch_assoc()){ ?>
                  <option value="<?=$row['id']?>"><?=$row['name']?></option>
              <? } ?>
            </select>
          
              <? 
              echo $showInput;
            }
          
      }
  } 
} else { 
  ?>
  <label>CODE<span class="error">*</span></label>
  <input type="text" name="newcode" id="newcode" class="f_input" value="" required>
  <label>DESCRIPTION<span class="error">*</span></label>
  <textarea  class="f_input"  name="description" id="description" required></textarea>
<?
} if($offer != 1) {?>
<label>NAME ARABIC</label>
<input type="text" name="name_arabic" id="name_arabic" class="f_input" value="">
<label>HSN</label>
<input type="text" name="hsn" id="hsn" class="f_input" value="">
<label>BARCODE</label>
<input type="text" name="barcode" id="barcode" class="f_input" value="">
<label>PART NUMBER</label>
<input type="text" name="partno" id="partno" class="f_input" value="">

<div class="mb-3">
  <label for="formFile" class="form-label">ITEM PHOTO</label>
  <input class="f_input" type="file" id="formFile" name="file">
</div>
<? } else {?>
<button type="submit" class="btn btn-sm btn-primary mt-2 submit_request" id="send">SUBMIT</button>
<? }?>
