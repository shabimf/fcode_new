<?php
include "../db/connect.php";
$obj = new database();
$request_id = $_REQUEST['request_id'];
$output = "";
$old_userid = "";
$old_DateTime = "";
$sql = "SELECT M.*,U.id as user_id,U.user_type,U.name  FROM fcode_messages AS M LEFT JOIN fcode_user AS U ON U.id = M.user_id WHERE M.request_id =$request_id ORDER BY M.id";
$query = $obj->con->query($sql);
if(mysqli_num_rows($query) > 0){
    $output .= '<div class="direct-chat-messages">';
    while($row = mysqli_fetch_assoc($query)){
        $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $row['created_at']);//string to datetime
        $right = "left"; $cnamecls = "right";
        $image = $obj->base_url."images/user_3.jpg";
        $uname = Ucfirst(strtolower($row['name']));
        if ($row['user_type'] == $_SESSION['user_type']) {
            $right = "right";
            $cnamecls = "left";
            $image = $obj->base_url."images/user.jpg";
        }
        if ($_SESSION['user_id']==$row['user_id']) $uname = "You";
            $output .= '
            <div class="direct-chat-msg '.$right.'">';
        //<div class="direct-chat-info clearfix">
        // if($old_userid!=$row['user_id']) {
        //     $output .= '<span class="font-weight-bold direct-chat-name pull-'.$cnamecls.'">'. $uname .'</span>';
        // }
        // if($old_DateTime!=$myDateTime->format('M Y g:ia') || $old_userid!=$row['user_id']) { 
        //     $output .= '<span class="direct-chat-timestamp pull-'.$right.'">'.$myDateTime->format('M Y g:ia').'</span>';
        // }
        // $output .= '</div>';
        if($old_userid!=$row['user_id'])$output .= '<img class="direct-chat-img" src="'.$image.'" alt="Message User Image">';
            $output .= '<div class="direct-chat-text">';
            if ($row['is_image'] == 1) {
                $output .= '<a href="uploads/chat/'.$row['message'].'" download><img src="uploads/chat/'.$row['message'].'" class="img-fluid img-thumbnail"/></a>';
            } else {
                $output .= $row['message'];
            }
            $output .= '</div>
            <p class="direct-chat-info clearfix pull-'.$right.'">';
            if($old_DateTime!=$myDateTime->format('M Y g:ia') || $old_userid!=$row['user_id']) {
                $output .= $myDateTime->format('M Y g:ia');
            }  if($old_userid!=$row['user_id']) { 
                $output .= ' | '. $uname;
            } 
            $output .= '</p>
            </div>';
    $old_userid = $row['user_id'];
    $old_DateTime = $myDateTime->format('M Y g:ia');
 } 
 $output .= '</div>';
 $user_ids = $obj->getAllUsers();
 $data = [];
 $data['readed_by'] = $_SESSION['user_id'];
 $data['readed_at'] = date("Y-m-d H:i:s");
 $obj->update_data_with_where("fcode_messages", $data, " where user_id IN('$user_ids') AND request_id=$request_id AND readed_by IS NULL");
} else { 
    $output .= '<div class="direct-chat-msg">No messages are available.<br/> Once you send message they will appear here.</div>';
} 
echo $output;