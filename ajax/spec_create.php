<?
include "../db/connect.php";
$obj = new database();
$table = $_POST['form-table'];
if ($_POST['form-division-id']>0) {
    $data['division_id'] 	= $_POST['form-division-id'];
}
$udatac['focus_code'] = $_POST['form-model-code'];
$udata['is_manual'] = 0;
$data = [];
$row_p = $obj->select_all_by_id("fcode_request_table", $_POST['form-register-id']);
$udatac['f_table_id'] = $row_p['f_table_id'];
if(isset($_POST['exists_name']) && $_POST['exists_name']>0) {
    $udata['f_table_val']  = $_POST['exists_name'];
    $obj->update_data("fcode_request_table", $udata, $_POST['form-register-id']);
    $data['id'] = $udata['f_table_val'];
    $data['table_id'] = $udatac['f_table_id'];
    $data['is_exist'] = 1;
} else {
    $data = array();
    $data['name'] = $_POST['form-model-name'];
    if($id = $obj->insert_data($table, $data)) {
        $udata['f_table_val']  = $id;
        $udatac['f_table_val'] = $id;
        $udatac['is_primary'] = 1;
        $code_id = $obj->insert_data("fcode_table_code", $udatac);
        $obj->update_data("fcode_request_table", $udata, $_POST['form-register-id']);
        $data['id'] = $udata['f_table_val'];
        $data['name'] = $data['name'];
        $data['table_id'] = $udatac['f_table_id'];
        $data['is_exist'] = 0;
    } 
}
echo json_encode($data);
?>
