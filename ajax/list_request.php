<?php
include "../db/connect.php";
$obj = new database();
$sqlQuery = "SELECT request.*,branch.name as branch_name FROM fcode_request as request
            LEFT JOIN mis_branches as branch ON request.branch_id = branch.id ";
$sqlQuery .= ' where 1=1';
if(!empty($_POST["ticket_no"])){
    $sqlQuery .= " AND request.unique_id='".$_POST["ticket_no"]."'";		
}
if($_POST["status"]){
    if($_POST["status"] == 3) $_POST["status"] = 0;
    $sqlQuery .= " AND request.status='".$_POST["status"]."'";		
}
if($_POST["form_date"]){
    $sqlQuery .= " AND request.created_at >= '".date('Y-m-d',strtotime($_POST["form_date"]))."'";		
    
}
if($_POST["to_date"]){
    $sqlQuery .= " AND request.created_at <= '".date('Y-m-d',strtotime($_POST["to_date"]))."'";		
}

if(!empty($_POST["order"])){
    $sqlQuery .= ' ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
} else {
    $sqlQuery .= ' ORDER BY request.id DESC ';
}
//echo $sqlQuery;
$res = $obj->con->query($sqlQuery);
$totalFiltered = mysqli_num_rows($res); 
if($_POST["length"] != -1){
    $sqlQuery .= ' LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}	

$result = $obj->con->query($sqlQuery);

$sqlQuery1 = "SELECT * FROM fcode_request ";
$result1 = $obj->con->query($sqlQuery1);
$numRows = mysqli_num_rows($result1);       
$requestData = array();
$i=1;	
while( $request = mysqli_fetch_assoc($result) ) {		
    $requestRows = array();
    $status = $request['status'];
    if($status == 1) $status_txt = "Approved"; elseif($status == 2) $status_txt = "Rejected"; else $status_txt = "Pending";
    $requestRows[] =  date("d-M-Y", strtotime($request['created_at']));	
    $requestRows[] =  date("h:i:a", strtotime($request['created_at']));		
    $requestRows[] = $request['unique_id'];
    $requestRows[] =  $request['branch_name'];
    $requestRows[] =  $status_txt;	
    $requestRows[] =  ($request['close_date']?date("d-M-Y", strtotime($request['close_date'])):"");	
    $requestRows[] =  ($request['close_date']?date("h:i:a", strtotime($request['close_date'])):"");		
    $requestData[] = $requestRows;
}
$output = array(
    "draw"				=>	intval($_POST["draw"]),
    "recordsTotal"  	=>  $numRows,
    "recordsFiltered" 	=> 	$totalFiltered,
    "data"    			=> 	$requestData
);
echo json_encode($output);