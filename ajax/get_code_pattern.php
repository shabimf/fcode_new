<?
include "../db/connect.php";
$obj = new database();
$table_id = $_REQUEST['table_id'];
$parent_id = $_REQUEST['parent_id'];
$table = $_REQUEST['table'];
$where = "" ;
if ($_REQUEST['type'] == "code") { 
    $table_type = "fcode_pattern_code"; 
    $table_code = "fcode_table_code";
    $field = "focus_code";
    $where = "AND is_primary=0";
    $field_id = "code_id";
} else { 
    $table_type = "fcode_pattern_name";
    $table_code = "fcode_table_alternative_name";
    $field = "name";
    $field_id = "name_id";
}
$result = $obj->get_fcode_pattern_code($table,$table_id,$parent_id,$table_type);
if( mysqli_num_rows($result)>0) {
?>

<table class="table table-bordered table-dark table-hover">
    <tbody>
        <tr class="bg-dark">
            <th width="50%">DIVISION</th>
            <th><?=strtoupper($_REQUEST['type'])?></th>
            <th width="5%">ACTION</th>
        </tr>
        <?  while($row = $result->fetch_assoc()){ ?>
            <tr id="patt_cls_<?=$row['id']?>">
                <td><?=$row['name']?></td>
                <td>
                    <?
                      $rel_id = $obj->select_all_by_id('fcode_table', $row['f_table_id']);
                      $f_table_id = $row['f_table_id'];
                      if ($rel_id['rel_id']) $f_table_id = $rel_id['rel_id'];
                      $row_code = $obj->select_all($table_code, "WHERE f_table_id = ".$f_table_id. " AND f_table_val=".$row['f_table_val']." " .$where);
                    ?>
                    <SELECT class="chosen-select" name="table_code_<?=$row['id']?>" id="table_code_<?=$row['id']?>" data-placeholder="SELECT <?=strtoupper($_REQUEST['type'])?>" onchange="updateCode(this.value,'<?=$_REQUEST['type']?>',<?=$row['id']?>);">
                    <option value=""></option>    
                    <?  while($result_code = $row_code->fetch_assoc()) { ?>
                            
                            <option value="<?=$result_code['id']?>" <? if($result_code['id']==$row[$field_id]) echo "selected";?>><?=$result_code[$field]?></option>
                        <? } ?>
                    </SELECT>
                  
                </td>
                <td>
                    <i class="fa fa-trash remove_code" aria-hidden="true" data-parent-id="<?=$parent_id?>" data-table-id="<?=$table_id?>"  data-id="<?=$row['id']?>" data-type="<?=$_REQUEST['type']?>" data-table="<?=$table?>"></i>
                </td>
            </tr>
        <? }?>
    </tbody>
</table>                    
<? }?>