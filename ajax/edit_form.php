<?php
include "../db/connect.php";
$obj = new database();
$sub_division_id = $_REQUEST['sub_division_id'];
$division_id = ($sub_division_id>0?$obj->select_field_by_id("mis_sub_division","division_id",$sub_division_id):0);
$brand_id =  (isset($_REQUEST['brand_id']) && $_REQUEST['brand_id']>0?$_REQUEST['brand_id']:0);
$offer = $_REQUEST['offer'];
$request_id = $_REQUEST['request_id'];
$result_table = $obj->select_requested_pattern($request_id);
$arr = array();
while($request_row_table = $result_table->fetch_assoc()){
    $arr[$request_row_table['f_table_id']]['val']     = $request_row_table['f_table_val'];
    $arr[$request_row_table['f_table_id']]['is_manual'] =  $request_row_table['is_manual'];
}
$own_brand = 0;
$pattern_array = [];
if($offer == 1) {
 $pattern_id = 0;
} else {
  $pattern_array = $obj->select_table_pattern_list($division_id, $sub_division_id, $own_brand, $brand_id);
  $pattern_id = $obj->select_pattern_id($division_id, $sub_division_id,$brand_id);
}

$required_arr = [];
if(!empty($pattern_array)){
    $pattern_row = $obj->select_all_by_id("fcode_pattern",$pattern_id);
    if ($pattern_row['required_ids']) $required_arr = json_decode($pattern_row['required_ids']);
        foreach ($pattern_array as $val) {
        
        $row_table = $obj->select_all_by_id("fcode_table", $val, " AND id!=1 AND id!=2 AND id!=3 ");
        if(isset($row_table) && isset($row_table['name'])) 
        { 
            $onchange = "onchange='showModelInput(this.value,".$row_table['id']."),getCode()'";
        ?>
        <div class="form-group">
            <label><?=$row_table['name']?>  
            <?if (in_array($row_table['id'], $required_arr)){?><span class="error">*</span><?}?>
            </label>
            
            <select <?php if ($val == 24) {  echo 'class="chosen-select f_input multiple'; } elseif($val==3) { echo 'class="f_table_selects f_input brand_select"';} else{ echo ' class="f_table_selects f_input"';}?> data-placeholder="SELECT <?=$row_table['name']?>" name="<? if ($val == 24) {?>f_table[<?=$row_table['id']?>][]<? } else {?>f_table[<?=$row_table['id']?>]<?}?>"   <? echo $onchange;?>  <?if (in_array($val, $required_arr)) { echo "required"; } ?>>
            <? if ($val != 24) { ?>  
                <option value=""></option>
            <? if (!in_array($row_p['id'], [1,24])) {?>
                <option value="add" <? if(isset($arr[$row_table['id']]['is_manual']) && $arr[$row_table['id']]['is_manual'] == "1"){  echo "selected";}?>>Add New</option>
            <? }
            }
                    $result = $obj->select_all($row_table['f_table'], "where status = 1");
                    while($row = $result->fetch_assoc()){ ?>
                    <option value="<?=$row['id']?>" <? if(isset($arr[$row_table['id']]['is_manual']) && $arr[$row_table['id']]['is_manual'] == "0"){  if ($row_table['id'] == "24"){ if (in_array($row['id'], explode(',',$arr[$row_table['id']]['val']))) { echo "selected";}} else { if($arr[$row_table['id']]['val'] == $row['id']) { echo "selected";}}}?>><?=$row['name']?></option>
                <? } ?>
            </select>
           
        </div>
        <div class="form-group <?if(!in_array($row_table['id'], [1,2])){echo 'f_tables1'; }?> txtcls_<?=$row_table['id']?> <?if(isset($arr[$row_table['id']]['is_manual']) && $arr[$row_table['id']]['is_manual'] == "1"){ } else { echo "hide";}?> ">         
            <label class="error"> 
                <?=$row_table['name']?> NAME
                <?if (in_array($row_table['id'], $required_arr)){?>
                    <span class="error">*</span>
                <?}?>
            </label>
            
            <input type="text" class="f_input add-form-control" name="field_<?=$row_table['id']?>" id="field_<?=$row_table['id']?>"  value="<? if(isset($arr[$row_table['id']]['is_manual']) && $arr[$row_table['id']]['is_manual'] == "1"){ if($arr[$row_table['id']]['val']) { echo $arr[$row_table['id']]['val'];}}?>"  <?if (in_array($row_table['id'], $required_arr)){echo"required";}?>  onkeyup="getCode(),checkName(this.value,<?=$row_table['id']?>); return false;">
            
        </div>                 
<?php 
       } 
    }
} else {?>
<div class="form-group">
    <label for="newcode">Code<span class="error">*</span></label>
    <input type="text" name="newcode" id="newcode" class="f_input" value="<?=$row_r['code']?>" required>
</div>
<div class="form-group">
    <label for="description">Description<span class="error">*</span></label>
    <input type="text" name="description" id="description" class="f_input" value="<?=$row_r['name']?>" required>
</div>
<? }?>