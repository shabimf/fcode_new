<?php

include "../db/connect.php";
$obj = new database();

if(isset($_POST['id']) && isset($_POST['f_table']) && $_POST['f_table'] !=""){

    $exist = $obj->check_table_pattern_exist($_POST['id'], $_POST['f_table']);

    if($exist)
        echo json_encode(array("error"=>"Allredy added"));
    else{
        
        $sort_order = $obj->get_max_sort_order($_POST['id'], $_POST['f_table']);

        $data = array();
        $data['division_id'] = $_POST['id'];
        $data['table_id'] = $_POST['f_table'];
        $data['sort_order'] = ++$sort_order;

        if($obj->insert_data("fcode_pattern", $data)){
            echo json_encode(array("success"=>"Added"));
        }
    }

}else{
    echo json_encode(array("error"=>"Not added"));
}

?>
