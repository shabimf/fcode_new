<?php
include "../db/connect.php";
$obj = new database();

$parent_id = $_REQUEST['id'];
$type = $_REQUEST['type'];

if($type == "code")
    $field = "pattern_ids";
else
    $field = "description_ids";
$ed = $obj->select_all_by_id("fcode_pattern", $parent_id);
$pattern_ids = null;
if($ed[$field])
    $pattern_ids = $ed[$field];
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="" method="post">
                <div class="row">
                    <div class="col-12">
                        <table class="table table-sm table-bordered">
                            <tr>
                                <th>DIVISION</th>
                                <td><?=$obj->select_name_by_id("mis_division", $ed['division_id'])?></td>
                            </tr>
                            <tr>
                                <th>SUB DIVISION</th>
                                <td><?=$obj->select_name_by_id("mis_sub_division", $ed['sub_division_id'])?></td>
                            </tr>
                            <? if ($ed['brand_id']) { ?>
                            <tr>
                                <th>BRAND</th>
                                <td><?=$obj->select_name_by_id("api_brand", $ed['brand_id'])?></td>
                            </tr>
                            <? } ?>
                        </table>
                    </div>
                    <div class="col-12">
                      <button type="button" class="btn btn-info float-right copy"
                        onclick="return copy_pattern();"
                        data-parent-id="<?=$parent_id?>"  
                        data-division="<?=$obj->select_name_by_id("mis_division", $ed['division_id'])?>" 
                        data-subdivision="<?=$obj->select_name_by_id("mis_sub_division", $ed['sub_division_id'])?>">
                        COPY
                      </button>
                    </div>
                    <div class="col-12 mt-3">                 
                        <ul class="nav nav-pills  nav-justified">
                            <li class="nav-item">
                                <a class="nav-link pattern_td cp <? if($type=='code') echo 'active';?>" data-id="<?=$parent_id?>" data-type="code">CODE</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link pattern_td cp <? if($type=='description') echo 'active';?>" data-id="<?=$parent_id?>" data-type="description">DESCRIPTION</a>
                            </li>
                        </ul>
                        <hr>
                        <?
                        $where = null;
                        if($pattern_ids)
                            $where = " where id NOT IN ( " . $pattern_ids . " )"; 
                        $result = $obj->select_all("fcode_table", $where ); ?>
                        <select class="chosen-select master_select" name="pattern" data-placeholder="CREATE PATTERN"
                            data-field="<?=$field?>" data-parent_id="<?=$parent_id?>" data-type="<?=$type?>">
                            <option value=""></option>
                            <? 
                            while($row = $result->fetch_assoc()){ ?>
                            <option value="<?=$row['id']?>"> <?=$row['name']?> </option>
                            <? } ?>
                            <option value="0">MANUAL ENTRY</option>
                        </select>
                    </div>
                </div>
            </form>
            <!-- <ul class="list-group list-group-flush  mt-3" id="post_list" data-id="<?=$ed['id']?>"> -->
               <div class="panel-group list-group list-group-flush" id="post_list"  data-id="<?=$ed['id']?>" role="tablist" aria-multiselectable="true">
                <?
                if(isset($pattern_ids)){
                    $i=0;
                    foreach(explode(',',$pattern_ids) as  $val){ ?>
                    <!-- <li class="list-group-item bg-danger text-white " data-id="<?=$val?>"> -->
                        <!-- <?=++$i?>. -->
                        <? if($val == 0)  {
                            ?>
                            <div class="panel panel-default" data-id="<?=$val?>">
                              <div class="panel-heading" role="tab" id="heading0">
                                <a  role="button" data-toggle="collapse" data-parent="#post_list" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
                                   MANUAL ENTRY
                                </a>
                                <i class="fas fa-minus-circle cp float-right confirm_alert remove_master" data-field="<?=$field?>"  data-type="<?=$type?>" data-id="<?=$val?>"></i>
                              </div>
                            </div>
                        <?
                        }   else  {
                            $table = $obj->select_field_by_id("fcode_table","f_table",$val);
                            $result_table = $obj->get_code($table,$val,$type,$parent_id);
                        ?>
                            <div class="panel panel-default" data-id="<?=$val?>" onclick="return get_pattern_data(<?=$val?>,<?=$parent_id?>,'<?=$table?>','<?=$type?>');">
                                <div class="panel-heading" role="tab" id="heading<?=$val?>">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#post_list" href="#collapse<?=$val?>" aria-expanded="true" aria-controls="collapse<?=$val?>">
                                        <?=$obj->select_name_by_id("fcode_table",$val);?>
                                    </a>
                                    <i class="fas fa-minus-circle cp float-right confirm_alert remove_master" data-field="<?=$field?>"  data-type="<?=$type?>" data-id="<?=$val?>"></i>
                                </div>
                                <div id="collapse<?=$val?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?=$val?>">
                                    <div class="panel-body">
                                        <form>
                                            <div class="form-row align-items-center">
                                                <div class="col-sm-10">
                                                <label class="sr-only" for="inlineFormInput"><?=$obj->select_name_by_id("fcode_table",$val);?></label>
                                                    <select class="chosen-select" name="table_val_<?=$val?>" id="table_val_<?=$val?>" data-placeholder="<?=$obj->select_name_by_id("fcode_table",$val);?>" multiple>
                                                    <? foreach ($result_table as $k=>$v){ ?>
                                                        <option value="<?=$k?>"><?=$v?></option>
                                                    <? } ?>
                                                    </select>
                                                </div>
                                                <div class="col-auto">
                                                <button type="button" class="btn btn-primary mb-2" onclick="return pattern_tbl_add(<?=$val?>,<?=$parent_id?>,'<?=$table?>','<?=$type?>');" >ADD</button>
                                                </div>
                                            </div>
                                        </form> 
                                       
                                    
                                    </div>
                                </div>
                               
                            </div>
                            <div class="table_cls_<?=$val?>" >
                            </div>  
                        
                        <?
                        }  
                        ?>
                    <!-- </li> -->
                <?   }         
                } ?>
                </div>
            <!-- </ul> -->
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#post_list").sortable({
            // placeholder : "ui-state-highlight",
            update: function (event, ui) {
                var post_order_ids = new Array();
                $('#post_list .panel-default').each(function () {
                    post_order_ids.push($(this).data("id"));
                });
                id = $('#post_list').data("id");

                $.ajax({
                    url: "ajax/update_sort_order.php",
                    method: "POST",
                    data: {
                        id: id,
                        post_order_ids: post_order_ids,
                        field: '<?=$field?>'
                    },
                    success: function (data) {
                        if (data) {
                            $(".alert-danger").hide();
                            $(".alert-success").show();
                        } else {
                            $(".alert-success").hide();
                            $(".alert-danger").show();
                        }
                    }
                });
            }
        });
    });
</script>