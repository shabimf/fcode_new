<?

require_once '../excelsupport/PHPExcel.php';
require_once '../excelsupport/PHPExcel/IOFactory.php';

include "../db/connect.php";
$obj = new database();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Retrieve the values from the form
    $divisionId = isset($_POST['divisionSelect']) ? $_POST['divisionSelect'] : null;
    $subDivisionId = isset($_POST['subDivisionSelect']) ? $_POST['subDivisionSelect'] : null;
    $brandSelect = isset($_POST['brandSelect']) ? $_POST['brandSelect'] : null;
    if($brandSelect==''){
        $brandId="brand_id is null";
    }else{
        $brandId="brand_id ='$brandSelect'";
    }

    $fileUpload = $_FILES['fileUpload']['name'];



    $where = "division_id ='$divisionId' and sub_division_id='$subDivisionId' and $brandId";
    $pattern_array = $obj->get_pattern_by_ids($where);


    $fixedHeaders = ["NAME ARABIC", "HSN", "BARCODE", "PART NUMBER", "ADDITIONAL ITEM DESCRIPTION"];

    $reversedPatternArray = array_reverse($pattern_array);

    foreach ($reversedPatternArray as $columnIndex => $columnName) {
        array_unshift($fixedHeaders, $columnName);
    }
    

    // echo implode(', ', $fixedHeaders);
    $file_name = $fileUpload;
    $file_name = date("y-m-d_H-i-s").$file_name;

    $finalFilePath = '../uploads/temp/'.$file_name;

    if(move_uploaded_file( $_FILES['fileUpload']['tmp_name'], $finalFilePath )){
        $objPHPExcel = new PHPExcel();

          $inputFileName = $finalFilePath;
        //   echo $finalFilePath;
        //   exit;
          try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
          } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .$e->getMessage());
          }

          $sheet = $objPHPExcel->getSheet(0);
          $highestRow = $sheet->getHighestRow();
          $highestColumn = $sheet->getHighestColumn();
          $activeSheet = $objPHPExcel->getActiveSheet();
          $fixedHeaders = ["NAME"];

          $error = false;
          $error_message = '';

          foreach ($fixedHeaders as $index => $header) {
            $cellValue = trim($activeSheet->getCellByColumnAndRow($index + 1, 1)->getValue());
    
            if ($cellValue != $header) {
                $error = true;
                $error_message .= "Mismatch in header '$header' at column " . chr($index + 65) . "1. ";
             
            }
        }
    
        if ($error) {
            echo "Error: " . $error_message;
        } else {
            echo "File is valid";
        }


    }

//  echo implode(', ', $fixedHeaders);


    //      echo $error_message;
    // echo "Form Submitted!";
    // header("Location: ../index.php");
    // exit();
} else {
    // Handle cases where the form is not submitted
    echo "Form not submitted!";
}

?>