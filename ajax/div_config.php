<?php
include "../db/connect.php";
$obj = new database();
$division_id = $_REQUEST['id'];

?>
<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" >CREATE PATTERN</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-9">
                    <select class="chosen-select f_table" data-placeholder="SELECT" >
                        <option value=""></option>
                        <? $result = $obj->select_all("fcode_table");
                        while($row = $result->fetch_assoc()){ ?>
                            <option value="<?=$row['id']?>"><?=$row['name']?></option>
                        <? } ?>
                    </select>
                </div>
                <div class="col">
                    <button class="btn btn-info btn-block add_pattern" data-id="<?=$division_id ?>">ADD</button>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                <table class="table table-dark">
                    <tbody>
                        <? $result = $obj->select_table_pattern_list($division_id);
                            while($row = $result->fetch_assoc()){ ?>
                                <tr>
                                    <td><?=$row['table_ref_name']?></td>
                                </tr>
                        <? }?> 
                    </tbody>
                </table>
                </div>
            </div>


            
        </div>

    </div>
</div>
