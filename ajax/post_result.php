        <label class="bold code_tls">Name:</label>
        <span id="p2" class="bold">
            <?
        if($row_r['name']) {
            echo $fcode = $row_r['name'];
        } else {
            echo $fcode = ($row_r['name'] ? $row_r['name'] : $obj->generate_description($request_id,$row_r['pattern_id']));
        }
    ?>
        </span>

        <? if(isset($fcode) && $fcode!="") { ?>
        <div class="form-group  text-left edit_code_txt hide">
            <label for="InputName">Name</label>
            <input type="text" class="form-control" id="fcode_name" value="<?=$fcode?>">
        </div>

        <i class="far fa-copy  cp  copy_icon" style="line-height: unset;" onclick="copyToClipboard('#p2')"></i> <br>

        <? } ?>
        <label class="bold code_tls">Code:</label>
        <span class="fcode bold" id="p1">
            <?
        if($row_r['code'] ) {
            echo $fcode = $row_r['code'];
        } else {
            echo $fcode = ($row_r['code'] ? $row_r['code']  : $obj->generate_fcode($request_id,$row_r['pattern_id']));
        }
        
    ?>
        </span>

        <? if(isset($fcode)) { ?>
        <div class="form-group  text-left edit_code_txt hide">
            <label for="InputCode">Code</label>
            <input type="text" class="form-control" id="fcode_code" value="<?=$fcode?>">
        </div>
        <i class="far fa-copy  cp copy_icon" style="line-height: unset;" onclick="copyToClipboard('#p1')"></i> <br>
        <? } if($row_r['remarks'] ) {?>
        <!-- <div class="form-group  text-left edit_code_txt hide">
            <label for="InputCode">Remarks</label>
            <?=($row_r['remarks']?'<p>'.$row_r['remarks']."</p>":'')?>
        </div> -->
        <span class="errors text-danger"> </span>
        <label class="bold">Remarks:</label>
        <span class="fcode bold" id="p3">
            <? echo $row_r['remarks'];?>
        </span>
        <i class="far fa-copy remarkscp cp copy_icon" style="line-height: unset;" onclick="copyToClipboard('#p3')"></i> <br>
        <?}?>
       
        <div class="form-group">
            <button class="btn btn-primary mt-3 edit_code" data-id="<?=$request_id?>" data-type="2">EDIT</button>
            <button class="btn btn-danger mt-3 cancel_code hide" data-id="<?=$request_id?>"
                data-type="3">CANCEL</button>
            <button class="btn btn-outline-info mt-3 approve_code" data-id="<?=$request_id?>">APPROVE CODE</button>
        </div>