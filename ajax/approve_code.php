<?php
include "../db/connect.php";
$obj = new database();
$request_id = $_POST['id'];
$request = $obj->select_all_by_id("fcode_request", $request_id); 
if (empty($request['new_barcode']) && $request['pattern_id']>0) {
    $division_fcode_result = $obj->select_all_data("fcode_table_code"," f_table_id='1' AND f_table_val='".$request['division_id']."' AND is_primary=1");
    $division_code = $division_fcode_result['focus_code'];

    $subdivision_fcode_result = $obj->select_all_data("fcode_table_code"," f_table_id='2' AND f_table_val='".$request['sub_division_id']."' AND is_primary=1");
    $subdivision_code = $subdivision_fcode_result['focus_code'];
    $data['serialcode'] = $division_code.$subdivision_code;
    $data['new_barcode'] = $obj->generateBarcode($request['division_id'],$request['sub_division_id']);
}

if (empty($request['code']) && $request['pattern_id']>0) {
    $data['code'] = $obj->generate_fcode($request_id);
    $data['name'] = $obj->generate_description($request_id,$request['pattern_id']);
} else {
    $data['code'] = $request['code'];
}
$data['status'] = 1;
$data['close_date'] =  date('Y-m-d H:i:s');
$data['approved_by'] = $_SESSION['user_id'];

if($obj->check_code_exist($data['code'],$request_id)>0){
    echo 0;
} else {
    $obj->update_data("fcode_request", $data, $request_id);
    echo 1;
}
?>

  