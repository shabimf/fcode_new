<?
include "../db/connect.php";
$obj = new database();
$request_id = $_POST['id'];
$row_r = $obj->select_all_by_id("fcode_request", $request_id);
?>
<div class="modal-content dragable_touch">
  <div class="modal-header bg-danger text-white">
    <h5 class="modal-title ls1 font-weight-bold" > APPROVE CODE </h5>
    <button type="button" class="text-white close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <div class="row">
      <?php
      if(empty($is_manualArr)){
      ?>  
      <div class="col align-self-center text-center">
        <h6 class="ls1 code_td post_result"> 
          <? include "post_result.php"; ?>
        </h6>
      </div>
    </div>
    <?php 
      }
    ?>
  </div>
</div>
