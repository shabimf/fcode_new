<?php 
if(mysqli_num_rows($result)>0) {
    while($notifi = $result->fetch_assoc()){
    ?>
    <div class="notification-list notification-list--unread post-id" id="<?php echo $notifi['id']; ?>">
        <div class="notification-list_content">
            <div class="notification-list_detail">
                <a  href="javascript:void(0)" class="chatbox-open" data-for="<?=$notifi['unique_id']?>" id="<?=$notifi['request_id']?>">
                    <p><b><?=Ucfirst($obj->select_field_by_id("fcode_user","name",$notifi['user_id']))?></b> reacted to request Id: <?=$notifi['unique_id']?></p>
                </a>
                <p class="text-muted"><?=$notifi['message']?></p>
                <p class="text-muted"><small><?=$obj->time_elapsed_string($notifi['created_at']);?></small></p>
            </div>
        </div>
    </div>
<? }
} if($obj->getUnreadMessageCount()==0)  {
?>
<div class="notification-list text-white font-weight-bold text-center">
  No notification found
</div>
<?php }?>