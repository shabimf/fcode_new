<?
require_once '../excelsupport/PHPExcel.php';
require_once '../excelsupport/PHPExcel/IOFactory.php';

include "../db/connect.php";
$obj = new database();

// echo 1;
// exit;

$divisionId = $_GET['division_id'];
$subDivisionId = $_GET['sub_division_id'];
// $brandId = $_GET['brand_id'];
// if($brandId==''){
//     $brandId="brand_id is null";
// }else{
//     $brandId="brand_id ='$brandId'";
// }

$where = "division_id ='$divisionId' and sub_division_id='$subDivisionId'";


$pattern_array = $obj->get_pattern_by_ids($where);

$data = $obj->select_all_by_field_id_process('mis_sub_division','id',$subDivisionId);

if (sizeof($pattern_array) != 0) {

if($data['show_item_name']==1){
    $fixedHeaders = ["ITEM NAME","NAME ARABIC", "HSN", "BARCODE", "PART NUMBER", "ADDITIONAL ITEM DESCRIPTION"];
}else{
    $fixedHeaders = ["NAME ARABIC", "HSN", "BARCODE", "PART NUMBER", "ADDITIONAL ITEM DESCRIPTION"];
}




// Create a new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set the active sheet to the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$sheet = $objPHPExcel->getActiveSheet();

// Set column headings from $pattern_array
foreach ($pattern_array as $columnIndex => $columnName) {
    // Convert numeric column index to Excel column letter
    $columnLetter = PHPExcel_Cell::stringFromColumnIndex($columnIndex);

    // Set the column heading
    $sheet->setCellValue($columnLetter . '1', $columnName);
}

// Add fixed headers

foreach ($fixedHeaders as $index => $header) {
    $sheet->setCellValueByColumnAndRow(count($pattern_array) + $index, 1, $header);
}

// Set headers to force download
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="downloaded_spreadsheet.xlsx"');
header('Cache-Control: max-age=0');

// Save the Excel file to the output stream
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

}else{
    header("location:../request_bulk_excel_upload.php?error=1");
}

exit;
?>