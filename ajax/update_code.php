<?
include "../db/connect.php";
$obj = new database();

$table = $_POST['table'];
$id = $_POST['id'];
$data['focus_code'] = $obj->escape($_POST['val'])!="" ? $obj->escape($_POST['val']) : NULL;
if($obj->update_data($table, $data, $id))
    echo $data['focus_code'];
else
    echo "<span class='text-danger'><small>Duplicate (Not Updated) </small></span>";
