<?php
include "../db/connect.php";
$obj = new database();
if(isset($_POST['view'])){
   $user_ids = $obj->getAllUsers();
   $sqlQuery = $obj->getNotification(5);
   $output = '<div class="notifications-wrapper">';
   if(mysqli_num_rows($sqlQuery) > 0)
   {
      while ($rowQuery = $sqlQuery->fetch_assoc()) {
        $unique_id = $obj->select_field_by_id("fcode_request","unique_id",$rowQuery['request_id']);
        $output .= '
            <a href="javascript:void(0)" class="top-text-block" data-for='.$unique_id.' id='.$rowQuery['request_id'].'>
            <div class="top-text-heading">'.$rowQuery['message'].'</div>
            <div class="top-text-light">'.$obj->time_elapsed_string($rowQuery['created_at']).'</div>
            </a>';
      } if($obj->getUnreadMessageCount()>6){
        $output .= '<li class="divider"></li><div class="notification-footer pull-right"><a href="notification" ><h4 class="menu-title">View all<i class="fa fa-arrow-circle-right"></i></h4></div>';
      }
   }
   else{
      $output .= '<div class="text-bold text-italic text-center text-danger">No Notification Found</div>';
   }
   $output .= '</div>';
   $data = array(
      'notification' => $output,
      'unseen_notification'  => $obj->getUnreadMessageCount()
   );
   echo json_encode($data);
}