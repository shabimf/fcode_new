<?php
include "db/connect.php";
$obj = new database();

$page = "request";
if(isset($_SESSION['fcode_manage_request']) && $_SESSION['fcode_manage_request'] ==0) {
  header("location:" . $obj->cloud_url);
	exit;
}
if(isset($_GET['reject'])){

  $data['status'] = "2";
  $data['close_date'] =  date('Y-m-d H:i:s');
  $data['approved_by'] = $_SESSION['user_id'];
  $obj->update_data("fcode_request", $data, $_GET['reject'] );

  header("location:request");
  exit;

}
if(isset($_GET['create'])){
  $data['is_create'] = "1";
  $data['is_create_date'] = date('Y-m-d H:i:s');
  $obj->update_data("fcode_request", $data, $_GET['create'] );
  header("location:request");
  exit;
}
$where = "";
$conditionsChanged = false;

if (!isset($_SESSION['status'])) {
    $_SESSION['status'] = 2;
}

if (isset($_GET['branch'])) {
    // Check if conditions change
    $newStatus = $_GET['status'];
    if ($newStatus != $_SESSION['status']) {
        $_SESSION['status'] = $newStatus;
        $conditionsChanged = true;
    }

    if ($_GET['branch'] != '') {
        $where .= " AND req.branch_id =" . $_GET['branch'];
     
    }
    if ($_GET['ticket_num'] != '') {
      $where .= " AND req.unique_id  = '" . $_GET['ticket_num'] . "'";
    }

    if ($_GET['code'] != '') {
        $where .= " AND req.code ='" . $_GET['code'] . "'";
      
    }
}

$status = $_SESSION['status'];

$sort="desc";
if ($status == 2) {
    $sort="asc";
    $where .= " AND req.status=0";
} elseif ($status == 3) {
    $where .= " AND req.is_create is null AND req.status=1";
} elseif ($status == 4) {
    $where .= " AND req.status = 2 ";
} elseif ($status == 5) {
    $where .= " AND req.status = 3";
}

$limit = 10;

// Set $page_num and $paginationStart to 1 if conditions changed, otherwise use the original logic
if ($conditionsChanged) {
    $page_num = 1;
} else {
    $page_num = isset($_GET['page_num']) && is_numeric($_GET['page_num']) ? $_GET['page_num'] : 1;
}

$paginationStart = ($page_num - 1) * $limit;

// Prev + Next
$prev = $page_num - 1;
$next = $page_num + 1;





?>

<!doctype html>
<html lang="en">
<head>

<? include "common/js_n_cs.php";?>

</head>
<body>
<?php include "common/header.php";?>
<div class="container-fluid body_bg " >
  <div class="d-flex flex-row">

    <?php include "common/nav.php";?>

    <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"  data-simplebar>
      <div class="col-sm-12 col-xs-12 inner-pad " >

        <? include "common/title_bar.php";?>

        <h6>SEARCH</h6>
        <div class="card">
          <div class="card-body">
            <form action="" method="get">
              <div class="row">

                <div class="col-sm-2">
                    <input type="hidden" name="page_num" value="<?=$page_num ?>" >
                    <input type="text" class="f_input" name="code" value="<?=$_GET['code'] ?? ''?>" placeholder="CODE" >
                </div>
 		<div class="col-sm-2">
                    <input type="text" class="f_input" name="ticket_num" value="<?=$_GET['ticket_num'] ?? ''?>" placeholder="TICKET NUMBER" >
                </div>
                <div class="col-sm-2">
                    <select class="chosen-select " data-placeholder="SELECT BRANCH" name="branch"  >
                      <option value=""></option>
                      <? $result = $obj->branch_by_access();
                          while($row = $result->fetch_assoc()){ ?>
                          <option <? if(isset($_GET['branch']) && $_GET['branch']==$row['id']){ ?> selected <? } ?> value="<?=$row['id']?>" ><?=$row['name']?></option>
                      <? } ?>
                    </select>
                </div>

                <div class="col-sm-2 hide">
                    <select class=" status" data-placeholder="SELECT STATUS" name="status"  >
                      <option <? if(isset($_GET['status']) && $_GET['status']==1){ ?> selected <? } ?> value="1">ALL</option>
                      <option <? if(isset($_GET['status']) && $_GET['status']==2){ ?> selected <? } ?> value="2">Pending</option>
                      <option <? if(isset($_GET['status']) && $_GET['status']==3){ ?> selected <? } ?> value="3">Approved</option>
                      <option <? if(isset($_GET['status']) && $_GET['status']==4){ ?> selected <? } ?> value="4">Rejected</option>
                      <option <? if(isset($_GET['status']) && $_GET['status']==5){ ?> selected <? } ?> value="5">Cancel</option>
                    </select>
                </div>

                <div class="col-sm-2">
                    <button class="btn btn-primary btn-block search_btn">SEARCH</button>
                </div>

                <div class="col-sm-2">
                    <a href="request" class="btn btn-secondary btn-block">RESET</a>
                </div>
 		<div class="col-sm-2"> 
                    <a href="export_request?conditions=1<?=$where?>" class="btn btn-success btn-block">EXPORT</a>
                </div>
              </div>

            </form>

          </div>
        </div>
        <hr>
        <? include "common/request_sub_nav.php";?>
       

        
        <div class="table-responsive">
          <table class="table table-bordered table-hover  table-dark">
              <tbody>

                  <tr class="bg-dark">
                      <th>Ticket No</th>
                      <th>DATE</th> 
                      <th>BRANCH</th>
                      <th>REQUESTED</th> 
                      <th>NEW REQUESTED</th> 
                      <th>NAME</th> 
                      <th>CODE</th> 
                      
                     
                      <th>HSN</th>
                      <th>PART NO</th>
                      <th>PRODUCT BARCODE</th>
                      <th>BARCODE</th>  
                      <th>REMARKS</th>  
                     
                      <th></th>
                  </tr>

                  <?
                      $result = $obj->select_all_request($where,$paginationStart, $limit,$sort);
                      $allRecrods = $obj->count_all_request($where);
                      $totoalPages = ceil($allRecrods / $limit);
                      $i=$paginationStart;
                      while($row = $result->fetch_assoc()){
                      ?>
                      <tr data-id="<?=$row['id']?>">
                          <td><?=$row['unique_id']?><br/>
                          <button type="button" class="btn chatbox-open position-relative" data-for="<?=$row['unique_id']?>" id="<?=$row['id']?>">
                            <i class="fa fa-comment " aria-hidden="true"></i> <span class="badge position-absolute top-0 start-100 translate-middle bg-success chat_count_<?=$row['id']?>"><?=$obj->getUnreadMessageCount($row['id'])?></span>
                          </button>
                          </td>
                          <td> <?=date("d-M-Y h:i:a", strtotime($row['created_at']))?>
                        
                          <? if($row['offer'] ==1) {?>
                          <span class="w3-badge label-danger">Bundle Offer</span>
                          <? }?>
                          </td>
                          <td> <?=$obj->select_name_by_id("mis_branches", $row['branch_id']);?> 
                          <br/>(<?=$row['user_name'];
                              if($row['email']) echo "<br/>".$row['email'];
                              if($row['phone_no']) echo "<br/>".$row['phone_no'];
                            ?>)<br/>
                            <? if ($row['approved_by']>0) {
                                echo ($row['status']==1?"Approved By: ":"Rejected BY: ");
                                echo "<span class='text-danger'>".$obj->select_field_by_id("fcode_user", "name", $row['approved_by'])."</span>";
                              }
                            ?>
                          </td>
                          <td><a data-image="screenshots/<?=$row['id'].".png";?>" href="javascript:void(0)" class="imagecls"><?
                           $color = "";
                           $pattern =[];
                           $pattern_arr = array();
                           $result_table = $obj->select_requested_pattern($row['id']);
                           $is_manual = 0;
                           while($row_table = $result_table->fetch_assoc()){
                              if($row_table['f_table_id'] == 0){ // manual
                               $pattern[] = "MANUAL  => <small class='text-warning'>".$row_table['f_table_val']."   </small> <br/>";
                              }else{
                               $row_p = $obj->select_all_by_id("fcode_table", $row_table['f_table_id']);
                               if($row_table['is_manual'] == 1) {
                                 $is_manual = 1;
                                 $pattern[] = $row_p['name']."  => <small class='text-warning'> ".$row_table['f_table_val']." </small> <br/>";
                               } else {
                                 $code = "";
                                 if($row_table['f_table_val']>0 && $row_table['f_table_id']>0) {
                                   $fcode_result = $obj->select_all_data("fcode_pattern_code"," parent_id='".$row['pattern_id']."' AND f_table_id='".$row_table['f_table_id']."' AND f_table_val='".$row_table['f_table_val']."' AND code_id IS NOT NULL");
                                   if($fcode_result) {
                                     $code_id = $fcode_result['code_id'];
                                     $code_row = $obj->select_all_by_id("fcode_table_code", $code_id);
                                     $code = $code_row['focus_code'];
                                   } else {
                                     if($row_table['f_table_id'] == 24)
                                     {
                                       $color = "text-danger fs-3";
                                       $code = "<span class='text-danger font-weight-bold'>".$obj->groupConcatCondition($row_table['f_table_id'], $row_table['f_table_val'])."</span>";
                                     } else {
                                       $fcode_result = $obj->select_all_data("fcode_table_code"," f_table_id='".$row_table['f_table_id']."' AND f_table_val='".$row_table['f_table_val']."' AND is_primary=1");
                                       $code = ($fcode_result?$fcode_result['focus_code']:"");
                                     }
                                     
                                   }
                                 }
                                 // if($code) {
                                   $pattern[] = $row_p['name']."  => <small class='text-warning'> ".$code." </small> <br/>";
                                 // }
                                 
                               }
                               
                              }
                              $pattern_arr[$row_table['f_table_id']] = $row_table['f_table_val']; 
                           }
                          $requested_pattern = $row['requested_pattern'];
                          $requested_pattern_arr = unserialize($requested_pattern);
                          foreach ($requested_pattern_arr as $k=>$v) {
                            $row_p = $obj->select_all_by_id("fcode_table", $k);
                            if(is_int($v) || $k == 24) {
                              $fcode_result = $obj->select_all_data("fcode_pattern_code"," parent_id='".$row['pattern_id']."' AND f_table_id='".$k."' AND f_table_val='".$v."' AND code_id IS NOT NULL");
                              if($fcode_result) {
                                $code_id = $fcode_result['code_id'];
                                $code_row = $obj->select_all_by_id("fcode_table_code", $code_id);
                                $code = $code_row['focus_code'];
                              } else {
                                if($k == 24)
                                {
                                  $color = "text-danger fs-3";
                                  $code = "<span class='text-danger font-weight-bold'>".$obj->groupConcatCondition($k, $v)."</span>";
                                } else {
                                
                                  $fcode_result = $obj->select_all_data("fcode_table_code"," f_table_id='".$k."' AND f_table_val='".$v."' AND is_primary=1");
                                  $code = ($fcode_result?$fcode_result['focus_code']:"");
                                }
                                
                              }
                            }  else {
                              $code = $v;
                            }
                            echo $row_p['name']."  => <small class='text-warning'> ";
                            echo $code." </small> <br/>";
                          }
                          ?></td>
                          <td> 
                              <?
                                $diff = array_diff_assoc( $requested_pattern_arr, $pattern_arr);
                                
                                if(count($diff)>0) {
                                  ?>
                                  <a href='edit_request?request_id=<?=base64_encode($row['id'])?>&view=1'><?=implode("  ", $pattern)?></a>
                                  <?
                                }
                                  
                              ?> 
                          </td>
                          <td> 



                          <?
                          $res=$obj->generate_description($row['id'],$row['pattern_id']);
                          $modifiedRes = preg_replace_callback('/\b(\d+)([a-zA-Z])/', function($matches) {
                            return $matches[1] . strtoupper($matches[2]);
                        }, ucwords(strtolower($res)));

                          $modifiedName = preg_replace_callback('/\b(\d+)([a-zA-Z])/', function ($matches) {
                              return $matches[1] . strtoupper($matches[2]);
                          }, ucwords(strtolower($row['name'])));

                            echo  ($row['name'] ? $modifiedName : $modifiedRes);
                          ?>
                          </td>
                          <td class="code_td">
                              <span class="<?=$color?> text-center">
                               <?
                               
                                echo $row['code']? strtoupper($row['code']) : strtoupper($obj->generate_fcode($row['id'],$row['pattern_id']));
                               ?>
                              </span>

                              <i class="far fa-copy float-right fa-lg cp hide copy_icon" style="line-height: unset;"></i>
                          </td>

                       
                          <td>
                          <?=($row['hsn']?$row['hsn']:"--")?>
                          </td>
                          <td>
                          <?=($row['partno']?$row['partno']:"--")?>
                          </td>
                          <td>
                            <?=($row['barcode']?$row['barcode']:"--")?>
                          </td>
                          <td>
                            <?=($row['new_barcode']?$row['serialcode'].$row['new_barcode']:"--")?>
                          </td>
                         <td>
                            <?=($row['remarks']?$row['remarks']:"--")?>
                          </td>
                          <td class="text-right">
                            <? if ($row['is_create'] == 1 && $row['status']==1) {
                                echo '<label class="font-weight-bold text-success fs-3">Created</label>';
                              } else if ($row['status']==0) {
                               echo '<label class="font-weight-bold text-warning fs-3">Pending</label>';
                              } else if ($row['status']==1) {
                              echo '<label class="font-weight-bold text-success fs-3">Approved</label>';
                              } else if ($row['status']==2) {
                              echo '<label class="font-weight-bold text-danger fs-3">Rejected</label>';
                              } else if ($row['status']==3) {
                              echo '<label class="font-weight-bold text-danger fs-3">Cancelled</label>';
                              } ?>
                            <div class="btn-group btn-group-sm"><a href="#" class="btn btn-danger">Action</a>
                              <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <div class="dropdown-menu dropdown-menu-right">
                                <?if($obj->isAdmin() == true && $row['status']==1){ 
                                  if($row['is_create']!=1) { ?>
                                <a class="dropdown-item" href="edit_request?request_id=<?=base64_encode($row['id'])?>">Edit</a>
                                <a class="dropdown-item" href="?create=<?=$row['id']?>">Create</a>
                                <? }?>
                                <a class="dropdown-item" onClick="return confirm('R u Sure?')" href="?reject=<?=$row['id']?>">Reject</a>
                                <?} if($row['status']==0) {?>
                                <a class="dropdown-item" href="edit_request?request_id=<?=base64_encode($row['id'])?>">Edit</a>
                                <? } else { ?>
                                  <a class="dropdown-item" href="edit_request?request_id=<?=base64_encode($row['id'])?>&view=1">View</a>
                                <?}if(isset($_SESSION['fcode_request_cancel']) && $_SESSION['fcode_request_cancel'] ==1 && $row['status']==0){?>
                                <a class="dropdown-item" href="#"  data-toggle="modal" data-target="#my-modal"  onclick="confirmDeleteModal(<?=$row['id']?>)">Cancel</a>
                                <? } if($obj->isAdmin() == true && $is_manual ==0 && (  $row['status']==0 || $row['status']==2)) { ?>
                                <a class="dropdown-item approve_btn cp">Approve</a>
                                <? } ?>
                              </div>
                            </div>  
                          </td>
                      </tr>
                  <? } ?>
              </tbody>
          </table>
        </div>

        <!-- Pagination -->
<nav aria-label="Page navigation example mt-5 float-right">
    <ul class="pagination justify-content-end">
        <li class="page-item <?php if($page_num <= 1){ echo 'disabled'; } ?>">
            <a class="page-link"
                href="<?php if($page_num <= 1){ echo '#'; } else { echo "?page_num=" . $prev; } ?>">Previous</a>
        </li>

        <?php
        $maxPagesToShow = 5; // You can adjust this number based on your preference
        $halfMax = floor($maxPagesToShow / 2);
        $startPage = max(1, $page_num - $halfMax);
        $endPage = min($totoalPages, $startPage + $maxPagesToShow - 1);

        if ($startPage > 1) {
            echo '<li class="page-item"><a class="page-link" href="?page_num=1&code=' . $_GET['code'] . '&branch=' . $_GET['branch'] . '&status=' . $_GET['status'] . '">1</a></li>';
            if ($startPage > 2) {
                echo '<li class="page-item disabled"><span class="page-link">...</span></li>';
            }
        }

        for ($i = $startPage; $i <= $endPage; $i++):
        ?>
            <li class="page-item <?php if($page_num == $i) {echo 'active'; } ?>">
                <a class="page-link" href="request?page_num=<?= $i; ?>&code=<?= $_GET['code']; ?>&branch=<?= $_GET['branch'] ?>&status=<?= $_GET['status'] ?>"> <?= $i; ?> </a>
            </li>
        <?php
        endfor;

        if ($endPage < $totoalPages) {
            if ($endPage < $totoalPages - 1) {
                echo '<li class="page-item disabled"><span class="page-link">...</span></li>';
            }
            echo '<li class="page-item"><a class="page-link" href="?page_num=' . $totoalPages . '&code=' . $_GET['code'] . '&branch=' . $_GET['branch'] . '&status=' . $_GET['status'] . '">' . $totoalPages . '</a></li>';
        }
        ?>

        <li class="page-item <?php if($page_num >= $totoalPages) { echo 'disabled'; } ?>">
            <a class="page-link"
                href="<?php if($page_num >= $totoalPages){ echo '#'; } else {echo "?page_num=". $next; } ?>">Next</a>
        </li>
    </ul>
</nav>



      </div>
      <? include "common/up_icon.php";?>
    </div>
  </div>
</div>
<div id="imagemodal" class="modal">
  <span class="close" data-dismiss="modal" aria-label="Close">&times;</span>
  
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>

<!-- Modal -->
<div class="modal fade" id="MyModal" >
  <div class="modal-dialog modal-xs modal-dialog-centered" >
  </div>
</div>

<div class="modal fade in" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="modal-register-label" aria-hidden="true" >
<div class="modal-dialog">
  <div class="modal-content">
  <div class="modal-header bg-danger text-white">
    <h5 class="modal-title ls1 font-weight-bold" > MODEL ADD </h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
    <div class="modal-body">
    <div class="alert alert-danger alert-dismissible fade msg-model hide" role="alert">
      <strong>Focus Code!</strong> already exists.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form role="form" action="" method="post" class="registration-form" id="registration-form">
        <div class="form-group">
          <label class="" for="form-model-name">Name</label>
            <input type="hidden" name="form-register-id"  id="form-register-id"/>
            <input type="hidden" name="form-table"  id="form-table"/>
            <input type="hidden" name="form-request-id"  id="form-request-id"/>
            <input type="hidden" name="form-division-id"  id="form-division-id"/>
            <input type="text" name="form-model-name" placeholder="Name..." class="form-model-name form-control" id="form-model-name" required>
          </div>
          <div class="form-group">
            <label class="" for="form-model-code">F CODE</label>
            <input type="text" name="form-model-code" placeholder="F code..." class="form-model-code form-control" id="form-model-code" onkeypress="return /[0-9a-zA-Z]/i.test(event.key)" required>
          </div>
          <div class="form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Check me already exists</label>
          </div>
          <div id="txtExists_name" class="form-group" style="display:none">
          <label class="" for="form-model-name">Name</label>
            <select class="chosen-select " data-placeholder="SELECT OPTION" name="exists_name" id="exists_name"  >
              <option value=""></option>
            </select>
          </div>
          <button type="button" class="btn btn-primary float-right mt-3 modelAdd">ADD</button>
      </form> 
    </div>
  </div>
</div>
</div>



<div id="my-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="deleteModalLabel">Confirm Deletion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body ">
                    Are you sure you want to delete this request?
                </div>
                <div class="modal-footer">
                    
                    <button type="button" class="btn btn-danger" id="confirmDelete">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    
</div>


</body>
<?php include "common/choosen-init.php";?>

<script>

$('#exampleCheck1').click(function() {
  if($("#exampleCheck1").is(':checked')){
    $.ajax({
      type: "POST",
      url: "ajax/ajaxExistsData",
      data: {'table':$("#form-table").val()},
      success: function(data) {
        $('#exists_name').empty();
        $('#exists_name').html(data);
        $('#exists_name').trigger("chosen:updated");
        $("#txtExists_name").show();  // checked
      }
    }); 
   
  }
  else {
    $("#txtExists_name").hide();  // unchecked
  }

});
function confirmDeleteModal(id) {
  $("#my-modal .modal-body").text("Are you sure you want to cancel request ID: " + id + "?");
  $("#confirmDelete").off("click").on("click", function () {
      $.ajax({
      type: "POST",
      url: "ajax/delete_request",
      data: {'id':id},
      success: function(data) {
        $("#my-modal").modal("hide");
        window.location.reload(true);
      }
    }); 
  });
}
function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text().trim()).select();
  document.execCommand("copy");
  $temp.remove();


}
$(document).on("click", ".status_link", function(e) {

  var status = $(this).data("status");
  $(".status").val(status);
  $(".search_btn").click();

});
$(".modelAdd").on('click', function(e){
  var id = $("#form-request-id").val();
  $.ajax({
    type: "POST",
    url: "ajax/spec_create",
    data: $('#registration-form').serialize(),
    success: function(msg) {
      if(msg > 0) {
        $("#modal-register").modal("hide");
        approveRequestShow(id);
        //location.reload(true);
      } else {
        $(".msg-model").removeClass("hide");
        $(".msg-model").addClass("show")
      }
    }
  }); 
});
$(".imagecls").on('click', function(e){
  var image = $(this).attr("data-image");
  $('#imagemodal').modal('show');
  var modalImg = document.getElementById("img01");
  modalImg.src = image;
});
function approveRequestShow(id) {
  $("#MyModal").modal({
    backdrop: false,
    show: true,
  });
  // reset modal if it isn't visible
  if (!$(".modal.in").length) {
    $(".modal-dialog").css({
      top: 20,
      left: 100,
    });
  }

  $(".modal-dialog").draggable({
    cursor: "move",
    handle: ".dragable_touch",
  });
  $.ajax({
    type: "POST",
    url: "ajax/request_details",
    data: { 'id': id },
    success: function(msg) {
      $("#MyModal").find(".modal-dialog").html(msg);
      $(".chosen-select").chosen();
    }
  }); 
}

$(document).on("click", ".approve_btn", function(e) {
  id = $(this).closest("tr").data("id");
  approveRequestShow(id);
});


$(document).on("click", ".approve_code", function(e) {
  var id = $(this).data("id");
  $.ajax({
    type: "POST",
    url: 'ajax/approve_code',
    data: {'id' : id},
    success: function(msg) {
      if (msg == 1) {
        $("#MyModal").modal("hide");
        location.reload(true);
      } else {
        $(".errors").text("Can't generate, code already exist");
      }
      
    }
  });
});

$(document).on("click", ".approve_model", function(e) {
  var title = $(this).attr("data-title");
  var code = $(this).attr("data-value");
  newCode = code.replace(/[_\W]+/g, '');
  alert(newCode);
  $("#modal-register").find('.modal-title').text(title + " ADD");
  $("#form-model-name").val($(this).attr("data-value"));
  $("#form-request-id").val($(this).attr("data-request-id"));
  $("#form-division-id").val($(this).attr("data-division-id"));
  $("#form-model-code").val(newCode);
  $("#form-register-id").val($(this).attr("data-id"));
  $("#form-table").val($(this).attr("data-table"));
  $("#txtExists_name").hide(); 
  $("#exampleCheck1").prop( "checked", false );
  $('#exists_name').html('');
  $('#exists_name').trigger("chosen:updated");
  $("#modal-register").modal('show');
});
$(document).on("click", ".edit_code", function(e) {
  $("#p2").addClass("hide");
  $(".edit_code_txt").removeClass("hide");
  $("#p1").addClass("hide");
 // $("#p3").addClass("hide");
  $(".code_tls").addClass("hide");
  $(".fa-copy").addClass("hide");
  $(".remarkscp").removeClass("hide");
  $(".approve_code").addClass("hide");
  $(".cancel_code").removeClass("hide");
  $(".edit_code").html("UPDATE");
  $(".errors").html('');
  $(".edit_code").addClass("update_code");
  $(".update_code").removeClass("edit_code");
});
$(document).on("click", ".update_code,.cancel_code", function(e) {
  if($("#fcode_name").val() == 'undefined') {
    var fcode_name = '';
  } else {
    var fcode_name = $("#fcode_name").val();
  }
  var form_data = "name="+fcode_name+"&code="+$("#fcode_code").val()+"&request_id="+$(this).attr("data-id")+"&type="+$(this).attr("data-type");
 
  if ($(this).attr("data-type") == 3) {
    $(".errors").html("");
    $(".update_code").html("EDIT");
    $(".update_code").addClass("edit_code");
    $(".edit_code").removeClass("update_code");
    $("#p2").removeClass("hide");
    $(".edit_code_txt").addClass("hide");
    $("#p1").removeClass("hide");
    $("#p3").removeClass("hide");
    $(".fa-copy").removeClass("hide");
    $(".approve_code").removeClass("hide");
    $(".cancel_code").addClass("hide");
    $(".code_tls").removeClass("hide");
  } else {
   
    $.ajax({
    type: "POST",
    url: 'ajax/update_request_code',
    data: form_data,
    success: function(msg) {
      if (msg == 1) {
        $(".update_code_name").html('UPDATE'); 
        $(".update_code").html("EDIT");
        $(".update_code").addClass("edit_code");
        $(".edit_code").removeClass("update_code");
        $("#p2").removeClass("hide");
        $(".edit_code_txt").addClass("hide");
        $("#p1").removeClass("hide");
        $("#p3").removeClass("hide");
        $(".fa-copy").removeClass("hide");
        $(".approve_code").removeClass("hide");
        $("#p1").html($("#fcode_code").val());
        $("#p2").html($("#fcode_name").val());
        $(".cancel_code").addClass("hide");
        $(".code_tls").removeClass("hide");
      } else {
        $(".errors").text("Can't generate, code already exist"); 
      }
      
      
    }
  });
  }
  
});
</script>
</html>
