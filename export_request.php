<?php
include "db/connect.php";
$obj = new database();


// Filter the excel data 
function filterData(&$str){ 
    $str = preg_replace("/\t/", "\\t", $str); 
    $str = preg_replace("/\r?\n/", "\\n", $str); 
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"'; 
}  
 
// Excel file name for download 
$fileName = "request-data_" . date('Y-m-d') . ".csv"; 
 
// Column names 
$fields = array('TICKET NO','NAME','SUB DIVISION','CODE','BRAND','ARABIC NAME','HSN','PART NO','PRODUCT BARCODE','BARCODE'); 
 
// Display column names as first row 
$excelData = implode(",", array_values($fields))  . "\r\n";

$where = $_REQUEST['conditions'];
// Fetch records from database 
 $result = $obj->select_all_request_export($where);

if($result){ 
    // Output each row of the data  
    $i = 1;
    while($row = $result->fetch_assoc()){

        $res = $obj->generate_description($row['id'], $row['pattern_id']);
        $modifiedRes = preg_replace_callback('/\b(\d+)([a-zA-Z])/', function($matches) {
            return $matches[1] . strtoupper($matches[2]);
        }, ucwords(strtolower($res)));
        
        // Replace multiple spaces with a single space
        $modifiedRes = preg_replace('/\s+/', ' ', $modifiedRes);
        
        // Trim leading and trailing spaces
        $modifiedRes = trim($modifiedRes);

        $modifiedName = preg_replace_callback('/\b(\d+)([a-zA-Z])/', function ($matches) {
            return $matches[1] . strtoupper($matches[2]);
        }, ucwords(strtolower($row['name'])));

        // Replace multiple spaces with a single space
        $modifiedName = preg_replace('/\s+/', ' ', $modifiedName);
        
        // Trim leading and trailing spaces
        $modifiedName = trim($modifiedName);


        $lineData = array($row['unique_id'],$row['name'] ? $modifiedName : $modifiedRes,$obj->select_name_by_id("mis_sub_division", $row['sub_division_id']),$row['code']? strtoupper($row['code']) : strtoupper($obj->generate_fcode($row['id'],$row['pattern_id'])),$obj->select_name_by_id("api_brand", $row['brand_id']),$row['name_arabic'],$row['hsn'],$row['partno'],$row['barcode'],$row['new_barcode']?$row['serialcode'].$row['new_barcode']:"--"); 
        array_walk($lineData, 'filterData'); 
        $excelData .= implode(",", array_values($lineData)) . "\r\n";
        $i++;
       
    } 
}else{ 
    $excelData .= 'No records found...'. "\n"; 
} 


 
// Headers for download 
header("Content-Type: application/vnd.ms-excel"); 
header("Content-Disposition: attachment; filename=\"$fileName\""); 
echo "\xEF\xBB\xBF"; // UTF-8 BOM
echo $excelData;
exit;