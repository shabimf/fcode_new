<?php
include "db/connect.php";
$obj = new database();
if (isset($_REQUEST['view']) && $_REQUEST['view'] == 1) {
  $page = "VIEW REQUEST";
} else {
  $page = "EDIT REQUEST";
}

$request_id = base64_decode($_REQUEST['request_id']);
$row_r = $obj->select_all_by_id("fcode_request", $request_id);



if (isset($_POST['updatecn'])) {

  $data['name'] = $_POST['name'];
  $data['code'] = $_POST['code'];
  $obj->update_data("fcode_request", $data, $_POST['rid']);
  header("Location: request.php");
}


?>
<!doctype html>
<html lang="en">

<head>
  <? include "common/js_n_cs.php"; ?>
  <? include("common/data_table.php"); ?>
</head>

<body>
  <?php include "common/header.php"; ?>
  <div class="container-fluid body_bg ">
    <div class="d-flex flex-row">
      <?php include "common/nav.php"; ?>
      <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?= $_SESSION['container'] ?>" id="content_box" data-simplebar>
        <div class="col-sm-12 col-xs-12 inner-pad ">
          <? include "common/title_bar.php"; ?>
          <div class="row">
            <div class="col-sm-6">
              <form action="" method="post" class="requestForm" name="requestForm" id="requestForm">
                <input type="text" name="request_id" id="request_id" value="<?= $request_id ?>">
                <input type="hidden" name="offer" id="offer" value="<?= $row_r['offer'] ?>">
                
                <?php
                $required_arr = [1, 2];
                $prequired_arr = [];
                if ($row_r['pattern_id'] > 0) {
                  $pattern_row = $obj->select_all_by_id("fcode_pattern", $row_r['pattern_id']);
                  if ($pattern_row['required_ids']) {
                    $prequired_arr = json_decode($pattern_row['required_ids']);
                    $required_arr = array_unique(array_merge($required_arr, $prequired_arr));
                  }
                }


                $is_manualArr = [];
                $result_table = $obj->select_requested_pattern($request_id);
                while ($row_table = $result_table->fetch_assoc()) {
                  $row_p = $obj->select_all_by_id("fcode_table", $row_table['f_table_id']);
                  $disabled = "";
                  if ($_SESSION['user_type'] == 2 && in_array($row_p['id'], [1, 2]) && $row_r['offer'] == 0) {
                    $disabled = "disabled";
                  }


                ?>
          
                  <div class="form-group <? if (!in_array($row_p['id'], [1, 2, 3])) {
                                            echo 'f_tables1';
                                          } ?>">
                    <label>
                      <?= $row_p['name'] ?>
                      <? if (in_array($row_p['id'], $required_arr)) { ?>
                        <span class="error">*</span>
                      <? } ?>
                    </label>
               
                  

                    <select class="chosen-select-deselect f_input <? if ($row_p['id'] == 1) {
                                                                    echo 'division_select';
                                                                  } elseif ($row_p['id'] == 2) {
                                                                    echo 'subdivision_select';
                                                                  } elseif ($row_p['id'] == 3) {
                                                                    echo 'brand_select';
                                                                  } ?>" <? if ($row_p['id'] == 1) echo "disabled";
                                                                        else echo $disabled; ?> onchange="showModelInput(this.value,<?= $row_table['f_table_id'] ?>),getCode();" data-placeholder="SELECT <?= $row_p['name'] ?>" <? if ($row_p['id'] == "24") { ?> name="f_table[<?= $row_p['id'] ?>][]" multiple <? } else { ?> name="f_table[<?= $row_p['id'] ?>]" <? } ?> <? if (in_array($row_p['id'], $required_arr)) {
                                                                            echo "required";
                                                                                                                                                                                                                                                                                                                                                                                                                                            } ?>>
                      <option value=""></option>
                      <? if (!in_array($row_p['id'], [1, 24])) { ?>
                        <option value="add" <? if ($row_table['is_manual'] == 1) {
                                              echo "selected";
                                            } ?>>Add New</option>
                      <? }
                      $result = $obj->select_all($row_p['f_table'], $search);

                      while ($row = $result->fetch_assoc()) {

                      ?>

                        <option value="<?= $row['id'] ?>" <? if ($row_p['id'] == "24") {
                                                            if (in_array($row['id'], explode(',', $row_table['f_table_val']))) {
                                                              echo "selected";
                                                            }
                                                          } else {
                                                            if ($row_table['f_table_val'] == $row['id']) { ?> selected <? }
                                                                                                                    } ?>><?= $row['name'] ?></option>
                      <? } ?>
                    </select>

                  </div>
                  <div class="<? if (!in_array($row_p['id'], [1, 2, 3])) {
                                echo 'f_tables1';
                              } ?> txtcls_<?= $row_table['f_table_id'] ?> <? if ($row_table['is_manual'] == 0) {
                                                                            echo "hide";
                                                                          } ?>">
                    <div class="form-group ">
                      <label class="text-danger">
                        <?= $row_p['name'] ?> NAME
                        <? if (in_array($row_p['id'], $required_arr)) { ?>
                          <span class="error">*</span>
                        <? } ?>
                      </label>
                      <div class=" <? if ($obj->isAdmin() == true && $row_table['is_manual'] == 1) echo 'input-group mb-3'; ?>">
                        <input type="text" class="form-control add-form-control form-input" placeholder="Enter <?= ucfirst(strtolower($row_p['name'])) ?>" name="field_<?= $row_table['f_table_id'] ?>" id="field_<?= $row_table['f_table_id'] ?>" value="<?= ($row_table['is_manual'] == 1 ? $row_table['f_table_val'] : '') ?>" <? if (in_array($row_p['id'], $required_arr)) {
                                                                                                                                                                                                                                                                                                                                    echo "required";
                                                                                                                                                                                                                                                                                                                                  } ?> onkeyup="getCode(),checkName(this.value,<?= $row_table['f_table_id'] ?>); return false;">
                        <span id="confirmname_<?= $row_table['f_table_id'] ?>" class="error_name"></span>
                        <? if ($obj->isAdmin() == true && $row_table['is_manual'] == 1) { ?>
                          <div class="input-group-append divbtn_<?= $row_table['f_table_id'] ?>">
                            <button class="btn btn-danger  approve_model" type="button" data-division-id="<?= ($row_table['f_table_id'] == 2 ? $row_r['division_id'] : 0) ?>" data-request-id="<?= $request_id ?>" data-value="<?= $row_table['f_table_val'] ?>" data-id="<?= $row_table['id'] ?>" data-table="<?= $row_p['f_table'] ?>" data-fid="<?= $row_table['f_table_id'] ?>" data-title="<?= $row_p['name'] ?>">APPROVE <?= $row_p['name'] ?></button>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                  <? if ($row_p['id'] == 2) { ?>
                    <div class="form-group show_item <? if ($row_table['item_name'] == "") echo "hide"; ?>">
                      <label>ITEM NAME</label>
                      <input type="text" name="item_name" id="item_name" class="f_input" value="<?= $row_table['item_name'] ?>" onchange="getCode()">
                    </div>
                <? }
                } ?>
                      <input type="text" name="divpattern" id="divpattern" value="<? echo $pattern_row['pattern_ids'] ?>">


                      <input type="text" name="mdivselect" id="mdivselect" value="">
                <div class="f_tables">
                </div>
                <?
                if ($row_r['offer'] == 1 || ($row_r['code'] != "" && $row_r['pattern_id'] == 0)) { ?>
                  <div class="form-group f_tables1">
                    <label for="newcode">Code<span class="error">*</span></label>
                    <input type="text" name="newcode" id="newcode_old" class="f_input" value="<?= $row_r['code'] ?>">
                  </div>
                  <div class="form-group  f_tables1">
                    <label for="description">Description<span class="error">*</span></label>
                    <input type="text" name="description" id="description_old" class="f_input" value="<?= $row_r['name'] ?>">
                  </div>
                <? }
                if ($row_r['offer'] == 0) { ?>
                  <div class="form-group">
                    <label for="name_arabic">NAME ARABIC</label>
                    <input type="text" name="name_arabic" id="name_arabic" class="f_input" value="<?= $row_r['name_arabic'] ?>">
                  </div>
                  <div class="form-group">
                    <label for="hsn">HSN</label>
                    <input type="text" name="hsn" id="hsn" class="f_input" value="<?= $row_r['hsn'] ?>">
                  </div>
                  <div class="form-group">
                    <label for="barcode">BARCODE</label>
                    <input type="text" name="barcode" id="barcode" class="f_input" value="<?= $row_r['barcode'] ?>">
                  </div>
                  <div class="form-group">
                    <label for="partno">PART NUMBER</label>
                    <input type="text" name="partno" id="partno" class="f_input" value="<?= $row_r['partno'] ?>">
                  </div>
                  <div class="form-group">
                    <label for="remarks">ADDITIONAL ITEM DESCRIPTION</label>
                    <input type="text" name="remarks" id="remarks" class="f_input" value="<?= $row_r['remarks'] ?>">
                  </div>

                  <div class="form-group">
                    <label for="formFile">ITEM PHOTO</label>
                    <input class="f_input" type="file" id="formFile" name="file">
                  </div>
                  <?php if (!empty($row_r['image'])) { ?>
                    <div class="form-group mt-2">

                      <div class="img-box" id="imgb_<?php echo $row_r['id']; ?>">
                        <a href="requestimg/<?= $row_r['image'] ?>" download>
                          <img src="requestimg/<?= $row_r['image'] ?>" height="60" width="75" class="img-thumbnail" />
                        </a>
                      </div>

                    </div>

                <?php }
                }
                ?>

                <?php
                if ($row_r['status'] == 0 || ($obj->isAdmin() == true && $row_r['status'] == 1)) { ?>
                  <div class="form-group mt-2">
                    <button type="submit" class="btn btn-sm btn-primary submit_request" id="send">UPDATE</button>
                    <a href="request" class="btn btn-sm  btn-secondary">CANCEL</a>
                  </div>
                <?php } ?>
              </form>
            </div>
            <div class="col-sm-6 <? if ($row_r['offer'] == 1) echo 'hide'; ?>">
              <h4 class="code_head">
                <?
                $res = $obj->generate_description($row_r['id'], $row_r['pattern_id']);
                $modifiedRes = preg_replace_callback('/\b(\d+)([a-zA-Z])/', function ($matches) {
                  return $matches[1] . strtoupper($matches[2]);
                }, ucwords(strtolower($res)));
                echo $code = $row_r['code'] ? strtoupper($row_r['code']) : strtoupper($obj->generate_fcode($row_r['id'], $row_r['pattern_id']));
                if ($code) echo "<hr/>"; ?>
              </h4>
              <h4 class="name_head">
                <?
                echo $name = ($row_r['name'] ? ucwords(strtolower($row_r['name'])) : $modifiedRes);
                if ($name) echo "<hr/>"; ?>
              </h4>
              <p class="missingColumns" style="background:#840a22; 
                        padding:10px; 
                        padding-left:20px; 
                        font-size:1rem; 
                        color:#fff; 
                        border-radius: 15px;
                        padding-right:20px;  
                        width:100%;    
                        letter-spacing:1.5px;"></p>



              <form action="" method="post">
                <?php if (($obj->isAdmin() == true)  && $row_r['status'] == 1) { ?>
                  <div class="form-group">
                    <label for="code">CODE </label>
                    <input type="text" name="code" id="code" class="f_input" value="<?= $row_r['code'] ? strtoupper($row_r['code']) : strtoupper($obj->generate_fcode($row_r['id'], $row_r['pattern_id'])) ?>" <?php echo isset($_POST['editcn']) ? '' : 'readonly'; ?>>
                  </div>
                  <div class="form-group">
                    <label for="name">NAME </label>
                    <input type="text" name="name" id="name" class="f_input" value="<?= ($row_r['name'] ? ucwords(strtolower($row_r['name'])) : $modifiedRes) ?>" <?php echo isset($_POST['editcn']) ? '' : 'readonly'; ?>>
                  </div>
                  <input type="hidden" name="rid" value="<?= $row_r['id'] ?>">
                  <div class="form-group mt-2">
                    <?php if (!isset($_POST['editcn'])) { ?>
                      <button type="submit" name="editcn" class="btn btn-sm btn-primary" id="edit">Edit</button>
                    <?php } else { ?>
                      <button type="submit" name="updatecn" class="btn btn-sm btn-primary" id="update">UPDATE</button>
                    <?php } ?>
                    <a href="request" class="btn btn-sm btn-secondary">CANCEL</a>
                  </div>
                <?php } ?>
              </form>
            </div>
          </div>

        </div>
        <? include "common/up_icon.php"; ?>
      </div>
    </div>
  </div>
  <div class="modal fade in" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="modal-register-label" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-danger text-white">
          <h5 class="modal-title ls1 font-weight-bold"> MODEL ADD </h5>
          <button type="button" class="text-white close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger alert-dismissible fade msg-model hide" role="alert">
            <strong>Focus Code!</strong> already exists.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form role="form" action="" method="post" class="registration-form" id="registration-form">
            <div class="form-group">
              <label class="" for="form-model-name">Name</label>
              <input type="hidden" name="form-register-id" id="form-register-id" />
              <input type="hidden" name="form-table" id="form-table" />
              <input type="hidden" name="form-request-id" id="form-request-id" />
              <input type="hidden" name="form-division-id" id="form-division-id" />
              <input type="hidden" name="form-ftable" id="form-ftable" class="form-ftable" />
              <input type="text" name="form-model-name" placeholder="Name..." class="form-model-name form-control" id="form-model-name" required>
            </div>
            <div class="form-group">
              <label class="" for="form-model-code">F CODE</label>
              <input type="text" name="form-model-code" placeholder="F code..." onkeypress="return /[0-9a-zA-Z]/i.test(event.key)" class="form-model-code form-control" id="form-model-code" required>
            </div>
            <!-- <div class="form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">Check me already exists</label>
            </div> -->

            <div id="txtExists_name" class="form-group">

            </div>

            <label class="" for="form-model-name">Name</label>
            <select class="chosen-select " data-placeholder="SELECT OPTION" name="exists_name" id="exists_name">
              <option value=""></option>
            </select>
            <button type="button" class="btn btn-danger float-right mt-3 modelAdd">ADD</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</body>
<script>

$(".missingColumns").hide(); 

$(document).on("change", ".subdivision_select", function(e) {
var request_id = $("#request_id").val();

var sub_division_id = $(".subdivision_select").val();
var pattern_id = $("#divpattern").val();
    $.ajax({
        url: "ajax/check_pattern",
        data: {
            "request_id": request_id,
            "sub_division_id": sub_division_id,
            "pattern_id": pattern_id
        },
        success: function(data) {
          const names = JSON.parse(data);
          var names_txt = "";
          for (let x in names) {
            names_txt += names[x] + ", ";
          }
          //console.log(names_txt);
          if (names_txt === "") {
            $(".missingColumns").hide(); 
                // names_txt = "";
            } else {
              
              names_txt = "Previous Values : " + names_txt;
              names_txt = names_txt.replace(/,\s*$/, ""); // Remove the last comma and any trailing spaces
              $(".missingColumns").show();
              $(".missingColumns").text(names_txt);

            }
            

        }
    });
  });




  $(document).on("keyup", ".form-model-name", function(e) {
    //   alert(1);
    var val = $(this).val();
    var table_id = $(".form-ftable").val();
    checkNameNew(val, table_id);
  });

  document.getElementById('edit').addEventListener('click', function() {
    document.getElementById('code').removeAttribute('readonly');
    document.getElementById('name').removeAttribute('readonly');
  });
</script>


<?php include "common/choosen-init.php"; ?>
<script src="<?= $obj->base_url ?>js/request.js"></script>

</html>