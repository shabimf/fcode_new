<?php
include "db/connect.php";
$obj = new database();
if($obj->isAdmin() == false) {
    header("location:" . $obj->cloud_url);
    exit;
}


$page = "settings";

if(isset($_POST['division_id'])){

    
    $data['division_id'] = $_POST['division_id'];
    $data['sub_division_id'] = ($_POST['sub_division_id'] ? $_POST['sub_division_id'] : NULL);  
    $data['brand_id'] = ($_POST['brand_id']? $_POST['brand_id'] : NULL);
     
    // $data['pattern_ids'] = isset($_POST['pattern']) ? implode(",",$_POST['pattern']) : NULL;
    $data['origin_brand_alternative'] = $_POST['origin_brand_alternative'] ?? 0 ;
    
    $patternExist = $obj->check_pattern_assigned($data['division_id'], $data['sub_division_id'], $data['brand_id'], $_POST['id'] ?? null);

    if($patternExist == true)
        die("pattern already exist");
    else if(isset($_POST['id']))
        $obj->update_data("fcode_pattern", $data, $_POST['id']);
    else
        $obj->insert_data("fcode_pattern", $data);

    header("location:".$obj->currentPage()); exit;
    

}

if(isset($_REQUEST['edit']))
    $ed = $obj->select_all_by_id("fcode_pattern", $_REQUEST['edit']);

$division_id_search = $sub_division_id_search = $brand_id_search = NULL;
if(isset($_REQUEST['division_id_search']) )  $division_id_search  = $_REQUEST['division_id_search'];
if(isset($_REQUEST['sub_division_id_search']) )  $sub_division_id_search  = $_REQUEST['sub_division_id_search'];
if(isset($_REQUEST['brand_id_search']) )  $brand_id_search  = $_REQUEST['brand_id_search'];
?>

<!doctype html>
<html lang="en">

<head>

    <? include "common/js_n_cs.php";?>

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <?
    if(isset($ed)){
    ?>
    <!-- <script>
        $(function() {
            $(".division_select").trigger('change');
            $('.subdivision_select').val(<?=$ed['sub_division_id']?>);
            $('.subdivision_select').trigger("chosen:updated");
        });
    </script> -->
    <? } ?>

</head>

<body>
    <?php include "common/header.php";?>
    <div class="container-fluid body_bg">
        <div class="d-flex flex-row">

            <?php include "common/nav.php";?>

            <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"
                data-simplebar>
                <div class="col-sm-12 col-xs-12 inner-pad">

                    <? include "common/title_bar.php";?>
                    <? include "common/settings_sub_nav.php";?>

                    <form action="" method="post">

                        <div class="row">

                            <div class="col-md-2">
                                <label>DIVISION</label>
                                <select class="chosen-select division_select " name="division_id" required
                                    data-placeholder="SELECT DIVISION" required onchange="return getSubdivision(this.value,1)">
                                    <option value=""></option>
                                    <? $result = $obj->select_all("mis_division");
                            while($row = $result->fetch_assoc()){ ?>
                                    <option value="<?=$row['id']?>" <? if(isset($ed) &&
                                        $ed['division_id']==$row['id']){?> selected
                                        <? } ?> >
                                        <?=$row['name']?>
                                    </option>
                                    <? } ?>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label>SUB DIVISION</label>
                                <select class="chosen-select  subdivision_select1" name="sub_division_id"
                                    data-placeholder="SELECT SUB DIVISION" required>
                                    <option value=""></option>
                                    <? $result = $obj->select_all("mis_sub_division");
                        while($row = $result->fetch_assoc()){ ?>
                                    <option value="<?=$row['id']?>" <? if(isset($ed) &&
                                        $ed['sub_division_id']==$row['id']){ ?> selected
                                        <? } ?>>
                                        <?=$row['name']?>
                                    </option>
                                    <? } ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Brand</label>
                                <select class="chosen-select" name="brand_id"
                                    data-placeholder="SELECT BRAND">
                                    <option value=""></option>
                                    <? $result = $obj->select_all("api_brand");
                                        while($row = $result->fetch_assoc()){ ?>
                                       <option value="<?=$row['id']?>" <? if(isset($ed) &&
                                        $ed['brand_id']==$row['id']){ ?> selected
                                        <? } ?>>
                                        <?=$row['name']?>
                                      </option>
                                    <? } ?>
                                </select>
                            </div> 
                            <!-- <div class="col">
                                <label>PATTERN</label>
                                <select class="chosen-select " multiple name="pattern[]"
                                    data-placeholder="CREATE PATTERN">
                                    <option value=""></option>
                                    <? $result = $obj->select_all("fcode_table");
                        while($row = $result->fetch_assoc()){ ?>
                                    <option value="<?=$row['id']?>" <? if(isset($ed) &&
                                        in_array($row['id'],explode(',',$ed['pattern_ids']))){ ?> selected
                                        <? } ?> ><?=$row['name']?></option>
                                    <? } ?>
                                </select>
                            </div> -->

                            <!-- </div>

                        <div class="row justify-content-end mt-3"> -->

                            <div class="col-sm-3">
                                <label>OWN BRAND ALTERNATIVE</label> <br>
                                <input type="checkbox" class="f_input" name="origin_brand_alternative" value="1" <?
                                    if(isset($ed) && $ed['origin_brand_alternative']==1) { ?> checked
                                <? } ?> >
                            </div>

                            <div class="col-sm-3 mt-3">
                                <button type="submit" class="btn btn-primary float-right">
                                    <? if(isset($ed)) echo 'UPDATE'; else echo 'SUBMIT'; ?>
                                </button>
                                <? if(isset($ed)){ ?>
                                <input type="hidden" value="<?=$ed['id']?>" name="id">
                                <a href="settings" class="btn btn-secondary float-right mr-2">CANCEL</a>
                                <? } ?>
                            </div>
                        </div>

                    </form>

                    <!-- <hr>
                    <button class="btn btn-primary float-right" data-toggle="modal"
                        data-target="#MyModal">CREATE</button> -->
                    <div class="clearfix"></div>
                    <hr>
                    <form action="" method="post">

                        <div class="row">

                            <div class="col-md-2">
                                <label>DIVISION</label>
                                <select class="chosen-select division_select" name="division_id_search"
                                    data-placeholder="SELECT DIVISION" onchange="return getSubdivision(this.value,2)" required>
                                    <option value=""></option>
                                    <? 
                                        $result = $obj->select_all("mis_division");
                            while($row = $result->fetch_assoc()){ ?>
                                    <option value="<?=$row['id']?>" <? if(isset($division_id_search) &&
                                        $division_id_search==$row['id']){?> selected
                                        <? } ?> >
                                        <?=$row['name']?>
                                    </option>
                                    <? } ?>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label>SUB DIVISION</label>
                                <select class="chosen-select subdivision_select2" name="sub_division_id_search"
                                    data-placeholder="SELECT SUB DIVISION" required>
                                    <option value=""></option>
                                    <?
                                        if(isset($division_id_search))
                                        $result = $obj->select_sub_divisions_by_divsion($division_id_search);
                                        else
                                        $result = $obj->select_all("mis_sub_division");
                                        while($row = $result->fetch_assoc()){ 
                                    ?>
                                    <option value="<?=$row['id']?>" <? if(isset($sub_division_id_search) &&
                                        $sub_division_id_search==$row['id']){ ?> selected
                                        <? } ?>>
                                        <?=$row['name']?>
                                    </option>
                                    <? } ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Brand</label>
                                <select class="chosen-select" name="brand_id_search"
                                    data-placeholder="SELECT BRAND">
                                    <option value=""></option>
                                    <? $result = $obj->select_all("api_brand");
                                        while($row = $result->fetch_assoc()){ ?>
                                       <option value="<?=$row['id']?>" <? if(isset($brand_id_search) &&
                                        $brand_id_search==$row['id']){ ?> selected
                                        <? } ?>>
                                        <?=$row['name']?>
                                      </option>
                                    <? } ?>
                                </select>
                            </div> 
                           

                            <div class="col-md-6">
                                 
                                <button type="submit" class="btn btn-primary btn-margin">
                                   SEARCH
                                </button>
                                <a href="export_pattern?division_id=<?=$division_id_search?>&sub_division_id=<?=$sub_division_id_search?>&brand_id=<?=$brand_id_search?>" class="btn btn-primary btn-margin float-right">
                                   EXPORT
                                </a>
                              
                            </div>  
                        </div>

                    </form>
                    <div class="clearfix"></div>
                    
                    <div class="row">
                        <div class="col">
                            <table class="table table-bordered table-dark table-hover">
                                <tbody>
                                    <tr>
                                    <tr class="bg-dark">
                                        <th width="40">#</th>
                                        <th>DIVISION</th>
                                        <th>SUB DIVISION</th>
                                        <th>BRAND</th>
                                        <th>CODE</th>
                                        <th>DESCRIPTION</th>
                                        <th>HASPATTERN</th>
                                        <th width="150"></th>
                                    </tr>
                                    </tr>
                                    <? 
                                    $i=0;
                                    $result = $obj->select_all_pattern($division_id_search,$sub_division_id_search,$brand_id_search);
                                    while($row = $result->fetch_assoc()){  ?>
                                    <tr class="<? if($row['origin_brand_alternative']==1) echo 'text-warning'?>">
                                        <td> <?=++$i?> </td>
                                        <td> <?=$row['divsion']?> </td>
                                        <td> <?=$row['sub_division']?> </td>
                                        <td> <?=$row['brand_name']?> </td>
                                        <td class="pattern_td cp" data-id="<?=$row['id']?>" data-type="code">
                                            <?=$obj->get_pattern($row['id']);?> 
                                        </td>
                                        <td class="pattern_td cp" data-id="<?=$row['id']?>" data-type="description">
                                            <?=$obj->get_pattern_description($row['id']);?> 
                                        </td>
                                        <td><? if($obj->get_pattern($row['id'])=="" && $obj->get_pattern_description($row['id'])=="") { 
                                           echo "NO";
                                        } else { echo"YES";}?></td>
                                        <td class="text-info cp ">
                                            <a href="javascript:void(0);" data-href="ajax/get_pattern.php?id=<?=$row['id']?>" class="openPopup"><button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete">REQUIRED</button></a>
                                            <a href="settings?edit=<?=$row['id']?>"><button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button></a>
                                        </td>
                                    </tr>
                                    <? }?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <? include "common/up_icon.php";?>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal-label" aria-hidden="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-black">
                    <h5 class="modal-title ls1 font-weight-bold" >SET VALIDATION</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible hide">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Required Set <strong>Successfully!</strong>
                </div>
                <div class="modal-body">
                   
                    
                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="codeModel">

    </div>
    <div class="modal fade in" id="modal-copy" tabindex="-1" role="dialog" aria-labelledby="modal-copy-label" aria-hidden="true" >
        <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header bg-primary text-white">
            <h5 class="modal-title ls1 font-weight-bold" ></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">
                <form role="form" action="" method="post" class="copy-form" id="copy-form">
                <div class="col-12">
                    <table class="table table-sm table-bordered">
                        <tr>
                            <th>DIVISION</th>
                            <td id="divcls"></td>
                        </tr>
                        <tr>
                            <th>SUB DIVISION</th>
                            <td id="subdivcls"></td>
                        </tr>
                    </table>
                
                <div class="form-group">
                    <label>Brand</label>
                    <input type="hidden" id="copy_parent_id" name="copy_parent_id"/>
                    <select class="chosen-select" name="copy_brand_id[]" id="copy_brand_id"
                        data-placeholder="SELECT BRAND" multiple required>
                        <? $result = $obj->select_all("api_brand");
                            while($row = $result->fetch_assoc()){ ?>
                            <option value="<?=$row['id']?>" <? if(isset($ed) &&
                            $ed['brand_id']==$row['id']){ ?> selected
                            <? } ?>>
                            <?=$row['name']?>
                            </option>
                        <? } ?>
                    </select>
                </div>
                </div>
                <button type="button" class="btn btn-primary float-right mt-3 copy-btn">COPY</button> 
                </form> 
            </div>
        </div>
        </div>
    </div>
</body>



<script>
    $('.openPopup').on('click',function(){
        var dataURL = $(this).attr('data-href');
        $('.modal-body').load(dataURL,function(){
            $('#myModal').modal({show:true});
        });
    }); 
    function saveRequired(id) {
        var itemForm = document.getElementById('itemForm'); 
        var checkBoxes = itemForm.querySelectorAll('input[type="checkbox"]'); 
        result = [];
        checkBoxes.forEach(item => { 
            if (item.checked) {  
                result.push(item.value); 
            }
        })
        
        $.ajax({
            url: "ajax/update_pattern_required.php",
            method: "POST",
            data: {
                id: id,
                required_ids: result,
            },
            success: function (data) {
                $(".alert-success").removeClass('hide');
            }
        });
        
    }
    $(document).on("change", ".master_select", function () {
        id = $(this).data("parent_id");
        field = $(this).data("field");
        type = $(this).data("type");
        pattern_id = $(this).val();
        $.ajax({
            url: "ajax/add_pattern.php",
            method: "POST",
            data: {
                id: id,
                pattern_id: pattern_id,
                field: field,
                type: type
            },
            success: function (data) {
                open_pattern(id,type);
            }
        });
    });

    $(document).on("click", ".remove_master", function () {
        if (!confirm("R u sure?")) 
        return false;
        id = $('#post_list').data("id");
        delete_id = $(this).data("id");
        field = $(this).data("field");
        type = $(this).data("type");
        $.ajax({
            url: "ajax/update_pattern.php",
            method: "POST",
            data: {
                id: id,
                delete_id: delete_id,
                field: field,
                type: type
            },
            success: function (data) {
                open_pattern(id,type);
            }
        })
    });

    $(document).on("click", ".pattern_td", function () {
        var ref = $(this);
        id = ref.data("id");
        type = ref.data("type");
        open_pattern(id, type);
    });

    function open_pattern(id, type) {
        $.ajax({
            url: "ajax/pattern_details",
            data: {
                "id": id,
                "type": type
            },
            success: function (msg) {
                $("#codeModel").html(msg);
                $("#codeModel").modal("show");
                $(".chosen-select").chosen();
            }
        });
    }
    function copy_pattern() {
        $("#divcls").html( $(".copy").attr("data-division"));
        $("#subdivcls").html( $(".copy").attr("data-subdivision"));
        $("#copy_parent_id").val( $(".copy").attr("data-parent-id"));
        $("#modal-copy").modal("show");
    }
    $(".copy-btn").click(function(){
        if($("#copy_brand_id").val().length > 0) {
            $.ajax({
            url: "ajax/copy_pattern.php",
            method: "POST",
            data: $('#copy-form').serialize(),
            success: function (data) {
                alert("Patterns copied successfully");
                $('#modal-copy').modal('hide');
                location.reload();
            }
           })
        } else {
            alert("Please select brand");
        }
    }); 
</script>

<?php include "common/choosen-init.php";?>

</html>