$(document).on("change", ".subdivision_select,  .brand_select", function(e) {
    showPattern();
});


function showPattern() {

    var division_id = $(".division_select").val();
    var offer = $("#offer").val();
    var sub_division_id = $(".subdivision_select").val();
    var brand_id = $(".brand_select").val();
    var request_id = $("#request_id").val();
    if (sub_division_id == "") $(".f_tables").html('');
    $(".code_head").html('');    
    $(".name_head").html('');   
    if (offer == 0) {
        $("#newcode_old").val('');
        $("#description_old").val('');
        $(".f_tables1").html('');
        $(".f_tables1").addClass('hide');

        $.ajax({
            url: "ajax/edit_form",
            data: {
                "division_id": division_id,
                "sub_division_id": sub_division_id,
                "brand_id": brand_id,
                "offer": offer,
                "request_id": request_id
            },
            success: function(data) {
                $("#offer").val(offer);
                $(".f_tables").html(data);
                $(".f_table_selects").chosen({
                    allow_single_deselect: true
                }).trigger("chosen:updated");
                $(".chosen-select").chosen();
                $(".brand_select").val(brand_id).trigger("chosen:updated");
               
            }
        });
    }
}



function checkNameNew(val, table_id) {
    var message = $('#confirmname_' + table_id);
    // document.getElementById("send").disabled = true;
    $.ajax({
        url: "ajax/check_name.php",
        type: "POST",
        data: {
            val: val,
            table_id: table_id
        },
        success: function(response) {
            if (response == 0) {
                $("#txtExists_name").show();
                $("#txtExists_name").html("<p style='color: green;'>Valid Name</p>");
                // $(".modelAdd").prop("disabled", false);
            } else if (response == 1) {
                $("#txtExists_name").show();
                $("#txtExists_name").html("<p style='color: red;'>Name Already Exists</p>");
                // $(".modelAdd").prop("disabled", true);
            }
        }
    });
}


$('#exampleCheck1').click(function() {
    if ($("#exampleCheck1").is(':checked')) {
        // $.ajax({
        //     type: "POST",
        //     url: "ajax/ajaxExistsData",
        //     data: {
        //         'table': $("#form-table").val()
        //     },
        //     success: function(data) {
        //         $('#exists_name').empty();
        //         $('#exists_name').html(data);
        //         $('#exists_name').trigger("chosen:updated");
        //         $("#txtExists_name").show();
        //     }
        // });

      
        $.ajax({
            url: "ajax/check_name_model.php",
            type: "POST",
            data: {
                val: $("#form-model-name").val(),
                table_id: $("#form-table").val()
            },
            success: function(response) {

                if (response == 0) {
                 
                    $("#txtExists_name").hide()
                }
                if (response == 1) {
              
                    $("#txtExists_name").show()
    
                }
            }
        });



    } else {
        $("#txtExists_name").hide();
    }
});

$(".modelAdd").on('click', function(e) {
    var id = $("#form-request-id").val();
    $.ajax({
        type: "POST",
        url: "ajax/spec_create",
        data: $('#registration-form').serialize(),
        success: function(data) {
            console.log(data)
            if (data) {
                const obj = JSON.parse(data);
                if (obj.is_exist == 1) {
                    $('select[name="f_table[' + obj.table_id + ']"]').val(obj.id).trigger("chosen:updated");
                } else if (obj.is_exist == 0) {
                    optText = obj.name;
                    optValue = obj.id;
                    $('select[name="f_table[' + obj.table_id + ']"]').append(new Option(optText, optValue));
                    $('select[name="f_table[' + obj.table_id + ']"]').val(obj.id).trigger("chosen:updated");
                }
                $('.txtcls_' + obj.table_id).addClass('hide');
                $(".divbtn_" + obj.table_id).addClass('hide');
                $('.field_' + obj.table_id).val('');
                $("#modal-register").modal("hide");
            } else {
                $(".msg-model").removeClass("hide");
                $(".msg-model").addClass("show")
            }
        }
    });
});

$(document).on("click", ".approve_model", function(e) {
    var title = $(this).attr("data-title");
    var code = $(this).attr("data-value");
    var table = $(this).attr("data-table");
    newCode = code.replace(/[_\W]+/g, '');
    $("#modal-register").find('.modal-title').text(title + " ADD");
    $("#form-model-name").val($(this).attr("data-value"));
    $("#form-request-id").val($(this).attr("data-request-id"));
    $("#form-division-id").val($(this).attr("data-division-id"));
    $("#form-model-code").val(newCode);
    $("#form-register-id").val($(this).attr("data-id"));
    $("#form-table").val($(this).attr("data-table"));
    $("#form-ftable").val($(this).attr("data-fid"));
    $("#txtExists_name").hide();
    $("#exampleCheck1").prop("checked", false);
    $('#exists_name').html('');
    $('#exists_name').trigger("chosen:updated");
    $("#modal-register").modal('show');



    getModelTable(table);
});


function getModelTable(table) {

        $.ajax({
            type: "POST",
            url: "ajax/ajaxExistsData",
            data: {
                'table': table
            },
            success: function(data) {
                $('#exists_name').empty();
                $('#exists_name').html(data);
                $('#exists_name').trigger("chosen:updated");
            
            }
        });

}