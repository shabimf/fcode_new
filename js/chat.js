const form = document.querySelector(".typing-area"),
inputField = form.querySelector(".input-field"),
sendBtn = form.querySelector("button"),
chatBox = document.querySelector(".chat-box");

$(document).on('click','.chatbox-open,.top-text-block',function(){
  $(".chat-title").html("Chat - "+$(this).attr('data-for'));
  $(".chat_count_"+$(this).attr('id')).html('');
  $("#request_incoming_id").val(this.id);
  $(".chatbox-popup, .chatbox-close").fadeIn();
 scrollToBottom();
});
$(".chatbox-close").on("click", function() {
  $(".chatbox-popup, .chatbox-close").fadeOut();
  $("#request_incoming_id").val(0);
});
$(".chatbox-panel-close").on("click", function() {
  $(".chatbox-popup, .chatbox-close").fadeOut();
  $("#request_incoming_id").val(0);
});



form.onsubmit = (e)=>{
    e.preventDefault();
}

inputField.focus();
inputField.onkeyup = ()=>{
    if(inputField.value != ""){
        sendBtn.classList.add("active");
    }else{
        sendBtn.classList.remove("active");
    }
}
chatBox.onmouseenter = ()=>{
  chatBox.classList.add("active");
}

chatBox.onmouseleave = ()=>{
  chatBox.classList.remove("active");
}
sendBtn.onclick = ()=>{
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "ajax/insert-chat", true);
    xhr.onload = ()=>{
      if(xhr.readyState === XMLHttpRequest.DONE){
          if(xhr.status === 200){
              inputField.value = "";
              scrollToBottom();
          }
      }
    }
    let formData = new FormData(form);
    xhr.send(formData);
}


setInterval(() =>{
  let xhr = new XMLHttpRequest();
  let request_id = $('#request_incoming_id').val();
  if(request_id>0) {
    xhr.open("POST", "ajax/get-chat.php", true);
    xhr.onload = ()=>{
      if(xhr.readyState === XMLHttpRequest.DONE){
        if(xhr.status === 200){
          let data = xhr.response;
          chatBox.innerHTML = data;
          if(!chatBox.classList.contains("active")){
            scrollToBottom();
          }
        }
      }
    }
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("request_id="+request_id);
  }
}, 500);

function scrollToBottom(){
  chatBox.scrollTop = chatBox.scrollHeight;
}

$('#uploadButton').on('click',function(evt){
  evt.preventDefault();
  $('#fileupload').trigger('click');
  $('#uploadButton').addClass('active');
});
$("#fileupload").change(function () {
  var form = $('#typing-area')[0];
  var formData = new FormData(form);
  formData.append( 'message', "" );
  $.ajax({
    url: "ajax/insert-chat",
    type: "POST",
    data:  formData,
    processData: false,
    contentType: false,
    success: function(data)
    {
      if(data=='invalid')
      {
      }
      else
      {
        $('#uploadButton').removeClass('active');
        scrollToBottom();
      }
      $('#fileupload').val('');
    }         
  });
});