// SLide Toggle
$(document).on("click", ".sToggle", function(e) {
    var element = $(this).data("hide");
    $("." + element).slideToggle();

});

$(document).on("click", ".sToggleTable", function(e) {
    var element = $(this).data("hide");
    $(this).closest("table").find("." + element).slideToggle();

});

$(document).on("click", ".sToggleTdArray", function(e) {
    var myArray = $(this).data("toggle");
    classes = myArray.join(",");
    $(this).closest("td").find(classes).slideToggle();
});

$(document).on("click", ".sToggleArray", function(e) {
    var myArray = $(this).data("toggle");
    classes = myArray.join(",");
    $(classes).toggleClass("hide");

});

$(document).on("click", ".sToggleClasses", function(e) {
    var element = $(this).data("hide");
    $(element).slideToggle();

});

$(document).on("click", ".sToggleHide", function(e) {
    var element = $(this).data("class");
    $(this).hide();
    $("." + element).slideToggle();

});


// Update Focus Code
$(document).on("click", ".update_code", function(e) {
    var ref = $(this);
    table = ref.closest("table").data("table");
    id = ref.closest("tr").data("id");
    val = ref.closest("td").find(".code_input").val();

    $.ajax({
        type: "POST",
        url: "ajax/update_code",
        data: { 'table': table, 'id': id, 'val': val },
        success: function(msg) {
            ref.closest("td").find(".f_code").html(msg);
        }

    });

});


$(document).on("change", ".division_select", function(e) {
    id = $(this).val();
    //getSubdivision(id);
});
function getSubdivision (id,val) {
    $.ajax({
        url: "ajax/sub_division_by_divsion",
        data: { "id": id },
        success: function(msg) {
            $(".subdivision_select"+val).html(msg).trigger("chosen:updated");
            $(".subdivision_select"+val).chosen({allow_single_deselect:true});
           
        }
    });
}

function addMore(val) {
    var html = $(".copy").html();  
    $("."+val).after(html);    
}

function addAlternativeMore(val) {
    var html = $(".copy_alt").html();  
    $("."+val).after(html);    
}

$(document).on("click", ".remove", function(e) {
    if ($(this).attr('data-code')) {
        var ids = $(this).attr('data-ids');
        var arr = [$( "#"+ids ).val(),$(this).attr('data-code')];
        $( "#"+ids ).val( arr.join( ", " ) );
    }
    $(this).parents(".add_txt").remove();  
});   

$(document).on("click", ".remove_alt", function(e) {
    if ($(this).attr('data-code')) {
        var ids = $(this).attr('data-ids');
        var arr = [$( "#"+ids ).val(),$(this).attr('data-code')];
        $( "#"+ids ).val( arr.join( ", " ) );
    }
    $(this).parents(".add_name_txt").remove();  
});  
    
function editable(id) {
  $(".codecls_"+id).css("margin-bottom","23px");
  $(".txt_cls_"+id).removeClass("hide");
  $(".txt_edit_cls_"+id).addClass("hide");
}

function noteditable(id) {
  $(".codecls_"+id).css("margin-bottom","0px");
  $(".txt_cls_"+id).addClass("hide");
  $(".txt_edit_cls_"+id).removeClass("hide"); 
}

function updateTextbox(id) {
    var table = "fcode_table_code";
    var val = $("#txt_val_"+id).val();
    $.ajax({
        type: "POST",
        url: "ajax/update_code",
        data: { 'table': table, 'id': id, 'val': val },
        success: function(msg) {
            $(".codecls_"+id).css("margin-bottom","0px");
            $(".txt_cls_"+id).addClass("hide");
            $(".txt_edit_cls_"+id).removeClass("hide");   
            $(".f_code_"+id).html(msg);
        }
    });
}

$(document).on("click", ".is_primary", function(e) {
    $(".is_primary_flag").val(0);
    if($(this).prop('checked')== true) {
        $(this).next('input').val(1);
    } 
  
});

function pattern_tbl_add (id, parent_id,table,type) {
    var table_val = $("#table_val_"+id).val();
    var form_data = 'table_id='+id+'&table_val='+table_val+'&parent_id='+parent_id+'&type='+type;
    if (table_val) {
        $.ajax({
            type: "POST",
            url: "ajax/update_code_pattern",
            data: form_data,
            success: function(data) {
                $("#table_val_"+id).val('').trigger("chosen:updated");
                if (data == "Success") {
                   get_pattern_data(id,parent_id,table,type);
                } else {
                    alert(data);
                }
            }
        });
    }
}

function get_pattern_data(id,parent_id,table,type) {
    var form_data = 'table_id='+id+'&parent_id='+parent_id+'&table='+table+'&type='+type;
    $.ajax({
        type: "GET",
        url: "ajax/get_code_pattern",
        data: form_data,
        success: function(data) {
          $(".table_cls_"+id).html(data);
          $(".chosen-select").chosen();
          $(".chosen-select").data("placeholder","Select").chosen();
          //$('.chosen-select').attr('data-placeholder', 'Please select a option');
          $('.chosen-select').trigger('chosen:updated');
        
        }
    });
}
$(document).on("click", ".remove_code", function(e) {
   var result = confirm("Want to delete?");
   if (result) {
    var id = $(this).attr('data-id');
    var type = $(this).attr('data-type');
    var table = $(this).attr('data-table');
    var parent_id = $(this).attr('data-parent-id');
    var table_id = $(this).attr('data-table-id');
    var form_data = 'id='+id+'&type='+type;
    $.ajax({
            type: "POST",
            url: "ajax/remove_code_pattern",
            data: form_data,
            success: function(data) {
                get_pattern_data(table_id,parent_id,table,type);
            }
        });
    }
});

function updateCode (val,type,id) {
   var form_data = 'id='+id+'&type='+type+'&val='+val;
   $.ajax({
    type: "POST",
    url: "ajax/update_multple_code_pattern",
    data: form_data,
        success: function(data) {
          
        }
    });

}
function checkName(val, table_id) {
    var message = $('#confirmname_' + table_id);
     document.getElementById("send").disabled = true;
    $.ajax({
        url: "ajax/check_name.php",
        type: "POST",
        data: {
            val: val,
            table_id: table_id
        },
        success: function(response) {
            if (response == 0) {
                message.removeClass("text-danger");
                message.addClass("text-success mt-3");
                message.html("Valid Name");
                if ($(".error_name").hasClass("text-danger")) {
                    document.getElementById("send").disabled = true;
                } else {
                    document.getElementById("send").disabled = false;
                }
            }
            if (response == 1) {
                message.removeClass("text-success");
                message.addClass("text-danger mt-3");
                message.html("Already Used");
                if ($(".error_name").hasClass("text-danger")) {
                    document.getElementById("send").disabled = true;
                } else {
                    document.getElementById("send").disabled = false;
                }

            }
        }
    });
}
function getDivision(id) {
    $.ajax({
        type: 'post',
        url: 'ajax/getdivisionid.php',
        data: {
            'id': id
        },
        success: function(response) {
            $('#mdivselect').val(response);
             $(".division_select").val(response).trigger("chosen:updated");
             $('.subdivision_select').val(id).trigger("chosen:updated");

        }
    });
}

function showModelInput(val, tbl_id) {

//   alert($('.division_select').val());

    $("#field_" + tbl_id).val('');
    if (val == "add") {
        $('.txtcls_' + tbl_id).removeClass('hide');
    } else {
        $('.txtcls_' + tbl_id).addClass('hide');
    }
    if (tbl_id == 2) {
        if (val == "add") {
            $(".division_select").prop('disabled', false);
            $(".division_select").val('').trigger("chosen:updated");
        } else {
            $(".division_select").prop('disabled', true);
            $(".division_select").val('').trigger("chosen:updated");
            getDivision(val);
           
        }

        getItemName(val);
    }

}
function getItemName (sub_division_id) {

    $.ajax({
        url: "ajax/show_text_field",
        data: {
          "sub_division_id": sub_division_id,
        },
        success: function(msg) {
          if(msg == 1) {
            $(".show_item").removeClass('hide');
          } else {
            $("#item_name").val("");
            $(".show_item").addClass('hide');
          }
        
        }
    });
}