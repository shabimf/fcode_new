var url=  "http://192.168.1.23/ash-reciept/api";

function fetch_customer(){
	var UserUUID =localStorage.getItem("UserUUID");
	$("#customer_id").html('<option value="0">SELECT CUSTOMER</option>');
	if(UserUUID){
		$.ajax({
	          type : "POST",
	          url: url+"/s/api/mobile/user/customers",
	          data: {"user":UserUUID},
	          success: function(response){
	             var obj = JSON.parse(response);
	             if(obj.success==1){
	             	for (var i = 0; i < obj.userData.length; i++) {
	             		var othr = "";
	             		if(obj.userData[i].trn_no!=""){
	             			othr = " (TRN:"+obj.userData[i].trn_no+")";	
	             		}	

	             		$("#customer_id").append('<option value="'+obj.userData[i].id+'">'+obj.userData[i].name+othr+'</option>');	
	             	}		
	                
	             }
	          },
	          error:function (xhr, ajaxOptions, thrownError) {

	          }
	    });
	}else{
		window.location="index.html"; 
	}
}

function fetch_reciepts(){
	var UserUUID =localStorage.getItem("UserUUID");
	if(UserUUID){
		$.ajax({
	          type : "POST",
	          url: url+"/s/api/mobile/user/reciepts",
	          data: {"user":UserUUID},
	          success: function(response){
	             var obj = JSON.parse(response);
	             if(obj.success==1){

	                
	             }else{
	                
	             }
	          },
	          error: function (xhr, ajaxOptions, thrownError) {
	           
	          }
	    });
	}else{
		window.location="index.html"; 
	}
}

function reset_form(form_id){
	$("#"+form_id).find("input[type=text], textarea").val("");
	$("#"+form_id).find("select").val(0);
	$(".cheque_input").addClass("d-none");
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
};

function generate_reciept_click(){
     $(".error_text").html("");
     $(".error_text").css("display", "none");

     var error = 0;
     var customer_id 	= $("#customer_id").val();
     var customer_name  = $("#customer_id option:selected").text();
     var payment_type 	= $("#payment_type").val();
     var amount 		= $("#amount").val();

     if(customer_id==0){
     	error = 1;
     }
	 if(amount==''){
     	error = 1;
     }else if(!$.isNumeric(amount)){
     	error = 1;
     }

     if($("#send_email").is(":checked")){
     	var email = $("#email").val();
     	if(email==''){
	     	error = 1;
	    }else  if (!isValidEmailAddress(email)) {
	    	error = 1;
	    }	
     }

     var payemnt = "Cash";
     if(payment_type>0){
		payemnt = "Cheque";
		var bank 			= $("#bank").val();
     	var cheque_no 		= $("#cheque_no").val();
		var cheque_date 	= $("#cheque_date").val();

		if(bank==''){
     		error = 1;
     	}
     	if(cheque_no==''){
     		error = 1;
     	}
     	if(cheque_date==''){
     		error = 1;
     	}
		
	 }

     if(error==0){
     	 $('#reciept_view .customer_name').html(customer_name);
     	 $('#reciept_view .payment_type').html(payemnt);
     	 $('#reciept_view .cheque_det').addClass("d-none");
		 
		 if(payment_type>0){
     	 	$('#reciept_view .bank').html(bank);
     	 	$('#reciept_view .cheque_no').html(cheque_no);
     	 	$('#reciept_view .cheque_date').html(cheque_date);
     	 	$("#reciept_view .cheque_det").removeClass("d-none");
     	 }

     	 $('#reciept_view .amount').html(amount);
     	 $('#reciept_view').modal('show');

     }else{

     	$(".error_text").html("Please fill all fields!");
     	$(".error_text").css("display", "block");

     }
}


function generate_reciept(){
     $(".error_text").html("");
     $(".error_text").css("display", "none");

     $(".reciept_btns").attr("disabled", true);
     $("#confirm_reciept").html("Generating..");
     $(".reciept_btns").css("display", "none");

     $(".reciept_content").addClass("d-none");
     $(".reciept_load").removeClass("d-none");
     $(".close").addClass("d-none");

     var error = 0;
     var customer_id 	= $("#customer_id").val();
     var customer_name  = $("#customer_id option:selected").text();
     var payment_type 	= $("#payment_type").val();
     var amount 		= $("#amount").val();

     if(customer_id==0){
     	error = 1;
     }
	 if(amount==''){
     	error = 1;
     }else if(!$.isNumeric(amount)){
     	error = 1;
     }
     var email = "";
     if($("#send_email").is(":checked")){
     	email = $("#email").val();
     	if(email==''){
	     	error = 1;
	    }else  if (!isValidEmailAddress(email)) {
	    	error = 1;
	    }	
     }

     var bank 			= '';
     var cheque_no 		= '';
	 var cheque_date 	= '';

     if(payment_type>0){

		bank 			= $("#bank").val();
     	cheque_no 		= $("#cheque_no").val();
		cheque_date 	= $("#cheque_date").val();

		if(bank==''){
     		error = 1;
     	}
     	if(cheque_no==''){
     		error = 1;
     	}
     	if(cheque_date==''){
     		error = 1;
     	}
		
	 }

     if(error==0){
     	
     	var UserUUID =localStorage.getItem("UserUUID");
		if(UserUUID){
			$.ajax({
		          type : "POST",
		          url: url+"/s/api/mobile/reciept/create",
		          data: {"user_id":UserUUID, "customer_id":customer_id, "payment_type":payment_type, "cheque_bank":bank, "cheque_no":cheque_no, "cheque_date":cheque_date, "amount":amount, "status":1, "email":email},
		          success: function(response){
		             var obj = JSON.parse(response);
		             if(obj.success==1){

		             	/*	
		             	var txt =  "{center}NVOICE{br}{center}{b}"+address+"{/b}{br}";  
						txt+="{left}{b}--------------------------------{/b}{br}";	 
						txt+="{left}Date - "+d+"  {right}Time: "+h+":"+m+"{br}";	 
						if(invoice>0){
							txt+="{left}Inv-"+invoice+"{br}";	
						}
						txt+="{left}{b}--------------------------------{/b}{br}";	 
		             	*/
		             	$(".modal-title").addClass("d-none");
		             	$("#reciept_view .modal-body").html("<h3 class='text-center'>Reciept Generated</h3><center><a  href='create.html' class='btn btn-success'>done!</a></center>");

		            }else{

		             	$(".reciept_btns").attr("disabled", false);
    					$("#confirm_reciept").html("Generate");
    					$(".reciept_btns").css("display", "block");
    					$(".reciept_content").removeClass("d-none");
     					$(".reciept_load").addClass("d-none");
     					$(".close").removeClass("d-none");

		             }
		          },
		          error: function (xhr, ajaxOptions, thrownError) {

		          }	
		    });
		}else{
			window.location="index.html"; 
		}

     }else{

	     	$(".error_text").html("Please fill all fields!");
	     	$(".error_text").css("display", "block");
	     	$('#reciept_view').modal('hide');
	     	$(".reciept_btns").attr("disabled", false);
	     	$(".reciept_btns").css("display", "block");
	    	$(".reciept_content").removeClass("d-none");
	     	$(".reciept_load").addClass("d-none");
	     	$(".close").removeClass("d-none");

     }
}


$( document ).ready(function() {

	$('#payment_type').on('change', function() {
		if(this.value==1){
	      	$(".cheque_input").removeClass("d-none");
		}else{
			$(".cheque_input").addClass("d-none");
		}
	});

	$('#send_email').on('change', function() {
		if(this.checked==1){
	      	$(".email_input").removeClass("d-none");
		}else{
			$(".email_input").addClass("d-none");
		}
	});
});

var UserUUID =localStorage.getItem("UserUUID");
if(!UserUUID){
    window.location="index.html"; 
}





	
	
	