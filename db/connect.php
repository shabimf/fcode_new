<?php
require("connection.php");
class database extends connection
{

	
	function __construct()
	{

		if(isset($_SERVER["SERVER_NAME"])){

			// $parts=explode('.', $_SERVER["SERVER_NAME"]);

			// if( ( isset($parts[0]) && $parts[0]=="cloud" ) ||  $_SERVER['REQUEST_URI']=="/fcode/"){
				
				$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);

				$current_page = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") ."://". $_SERVER['HTTP_HOST'] . $uri_parts[0];

				if(!isset($_SESSION['user_id']) && $current_page != $this->login_url){
					//  var_dump($current_page); exit;
					header("location:".$this->cloud_url); exit;
				}
			// }

		}


		$this->con = mysqli_connect( $this->host, $this->user, $this->pswd, $this->db) or die( "can't connect to DB" );
		mysqli_set_charset($this->con,'latin1');
		

		// error_reporting(0);

		if(!isset($_SESSION['csh_nav']))
			$_SESSION['csh_nav'] = false;

		$_SESSION['nav']  = $_SESSION['container'] = ""; 

		if($_SESSION['csh_nav']==false){
			$_SESSION['nav'] = "shrinked_nav";
			$_SESSION['container'] = "shriked_container";
		}

	}
	

	
	function login_check_cloud_user( $userid=0)
	{
		$result = $this->con->query("SELECT * from fcode_user where  cloud_id='$userid' and status=1");
		return $result;

	}

	function login_check( $username, $password)
	{

		$result = $this->con->query("SELECT * from mis_user where username='$username' AND password='$password' and status=1");
		return $result;

	}

	function thispage()
	{
		return strtok($_SERVER["REQUEST_URI"], '?');
	}



	function isAdmin(){
		if($_SESSION['user_type'] == '1')
			return true;
		else
			return false;
	}
	

	function currentPage(){
		$variable = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
		$variable = substr($variable, 0, strpos($variable, ".php"));
		return $variable;
	}

	function clean($string)
	{
	   $string = strtolower(str_replace(' ', '-', $string)); // Replaces all spaces with hyphens.
	
	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
	
	function escape($string)
	{
		return  $this->con->escape_string(trim($string));	
	}

	function Error(){
		die(mysqli_error($this->con));
	}

	function in_array_all($needles, $haystack) {
		return empty(array_diff($needles, $haystack));
	 }
	
	function delete_value($table,$id=0)
	{
		$this->con->query(" DELETE from $table where id = $id") or $this->Error();
	}
	
	function delete_where($table,$where="1!=1")
	{
		$sql = " DELETE from $table  $where";
		// echo $sql;
		$this->con->query($sql) or $this->Error();
	}
	
	function select_all($table, $search=" where 1=1 ")
	{	
		$sql = "SELECT * from $table  $search ";
		//echo $sql;exit;
		$result = $this->con->query($sql) or $this->Error();
		return $result;
		
	}
			
	function select_all_by_id($table, $id, $additional_query=null)
	{		
	
		$sql= "SELECT * from $table where id=$id ".$additional_query;
		$result = $this->con->query($sql) or $this->Error();
		return mysqli_fetch_assoc($result);
		
	}
	
	function select_all_by_field_id($table, $field, $id)
	{		
		
		$id = $this->escape($id);
		
		$result = $this->con->query("SELECT * from $table where REPLACE($field, ' ', '') = REPLACE('$id', ' ', '')") or $this->Error();
		return mysqli_fetch_assoc($result);
		
	}

	function select_all_data($table,  $where="")
	{	
		$result = $this->con->query("SELECT * from $table where  $where");
		return mysqli_fetch_assoc($result);
		
	}

	function select_all_fcode_table() {
		$result = $this->con->query("SELECT * FROM fcode_table");
		return mysqli_fetch_all($result, MYSQLI_ASSOC);
	}


	
	function select_all_by_field_id_process($table, $field, $id)
	{		
		
		$id = $this->escape($id);
		$result = $this->con->query("SELECT * from $table where $field='$id' ") or $this->Error();
		return mysqli_fetch_assoc($result);
		
	}

	function select_id_by_name($table, $name){
		$id = null;
		if($name!=""){
			$result = $this->con->query("SELECT id from $table where name='$name' ");
			$row = $result->fetch_assoc();
			if(isset($row['id']))
				$id =  $row['id'];
		}

		return $id;
	}
	

	function select_id_by_fnames($table, $name){
	

			$result = $this->con->query("SELECT id from $table where f_table='$name' ");
			$row = $result->fetch_assoc();
			if(isset($row['id']))
				$id =  $row['id'];
		

		return $id;
	}
	function select_id_by_fnames_new($table, $name){
	

		$result = $this->con->query("SELECT id from $table where f_table='$name' and rel_id!=''");
		$row = $result->fetch_assoc();
		if(isset($row['id']))
			$id =  $row['id'];
	

	return $id;
}

	
	function select_active_by_field_id($table, $field, $id)
	{		
		
		$id = $this->escape($id);
		$result = $this->con->query("SELECT * from $table where $field='$id' AND status=1") or $this->Error();
		return mysqli_fetch_assoc($result);
		
	}
	
	function select_all_by_field_id_array($table, $field, $id)
	{		
		$result = $this->con->query("SELECT * from $table where $field='$id' ") or $this->Error();
		return $result;
		
	}
	
	
	function select_all_by_field_id_array_order($table, $field, $id, $order)
	{		
		$result = $this->con->query("SELECT * from $table where $field='$id' ORDER BY $order ASC ") or $this->Error();
		return $result;
		
	}

	function get_code($table, $table_id, $type, $parent_id) {

		$rows = array();
		$rel_id = $this->select_all_by_id('fcode_table',$table_id);
		if ($rel_id['rel_id']) $table_id = $rel_id['rel_id'];
		if ($type == "description") $tbl_patt_table = "fcode_pattern_name";
		else $tbl_patt_table = "fcode_pattern_code";

		$implode_arr = array();
		$_fbexclude = $this->con->query("SELECT f_table_val FROM ".$tbl_patt_table." WHERE f_table_id = $table_id AND parent_id=$parent_id");
		while($row = mysqli_fetch_assoc($_fbexclude)) {
			$implode_arr[] = $row['f_table_val'];
		}
		
		$result = $this->con->query("SELECT ".$table.".*,count(fcode_table_code.f_table_val) as record_count from $table LEFT JOIN fcode_table_code ON ".$table.".id = fcode_table_code.f_table_val where fcode_table_code.f_table_id='$table_id' and fcode_table_code.is_primary=0 GROUP BY ".$table.".id  ORDER BY ".$table.".name ASC ") or $this->Error();
		if ($type == "description"){
			$result = $this->con->query("SELECT ".$table.".*,count(fcode_table_alternative_name.f_table_val) as record_count from $table LEFT JOIN fcode_table_alternative_name ON ".$table.".id = fcode_table_alternative_name.f_table_val where fcode_table_alternative_name.f_table_id='$table_id'  GROUP BY ".$table.".id  ORDER BY ".$table.".name ASC ") or $this->Error();
		}
		if($table_id && mysqli_num_rows($result) >0) {
		  while($row =  mysqli_fetch_assoc($result)) {
			if (!in_array($row['id'], $implode_arr)) {
			   $rows[$row['id']] = $row['name'];
			}
		  }
		}
		return $rows;
	}

	function select_field_by_id($table, $field, $id){
		$result = $this->con->query("SELECT $field from $table where id = $id");
		$row = $result->fetch_assoc();
		return $row[$field];
	}
	
	function branch_by_access(){
		$result = $this->con->query("SELECT * from  mis_branches as branch where 1=1 ".$_SESSION['branch_search']) or $this->Error();
		return $result;
	}
	
	
	function select_all_orderby($table)
	{		
		$result = $this->con->query("SELECT * from $table order by id desc  ") or $this->Error();
		return $result;
		
	}
	
	function select_all_active($table, $where="")
	{		
		//echo "SELECT * from $table where status=1 $where";
		$result = $this->con->query("SELECT * from $table where status=1 $where") or $this->Error();
		return $result;
		
	}
	
	function select_all_active_where($table,  $where="1!=1")
	{		
		$result = $this->con->query("SELECT * from $table where $where AND status=1 ORDER BY name") or $this->Error();
		return $result;
		
	}
	
	function select_all_by($table, $where="1!=1")
	{		
	    //echo "SELECT * from $table where $where ";
		$sql = "SELECT * from $table where $where ";
		
		$result = $this->con->query($sql) or $this->Error();
		return $result;
		
	}
	
	function change_status($table, $status, $id)
	{
		$this->con->query("UPDATE $table set status = $status where id = $id");
	}
		
	function select_name_by_id($table,$id, $additional_query=null)
	{		
		$name = null;
		if($id){
			$result = $this->con->query("SELECT name from $table where id = $id ".$additional_query) or $this->Error();
			$row =  mysqli_fetch_assoc($result);
			$name = $row['name'];
		}
		return $name;
		
	}

	function select_table_by_id($id){
		$result = $this->con->query("SELECT f_table from fcode_table where id = $id");
		$row = $result->fetch_assoc($result);
		return $row['f_table'];
	}

	function select_sub_divisions_by_divsion($division_id){

		$result = $this->con->query("SELECT name, id from mis_sub_division where division_id = $division_id AND is_fcode=1") or $this->Error();
		return $result;
	}
		

	function select_sub_divisions($table_id){
		$sql = "SELECT mis_sub_division.*, d.name as division_name,  (SELECT GROUP_CONCAT(id SEPARATOR ',') FROM fcode_table_code WHERE f_table_val = mis_sub_division.id and f_table_id = ".$table_id.") as code_id, (SELECT GROUP_CONCAT(id SEPARATOR ',') FROM fcode_table_alternative_name WHERE f_table_val = mis_sub_division.id and f_table_id = ".$table_id.") as alt_name_id FROM mis_sub_division left join mis_division as d ON d.id = mis_sub_division.division_id WHERE mis_sub_division.is_fcode=1";

		// $result = $this->con->query("SELECT sub.*, d.name as division_name, GROUP_CONCAT(r.id) AS code_id from mis_sub_division as sub 
		// left join mis_division as d ON d.id = sub.division_id
		// INNER JOIN fcode_table_code AS r ON sub.id=r.f_table_val AND r.f_table_id =".$table_id."
		// GROUP BY sub.id order by sub.division_id") or $this->Error();
		$result = $this->con->query($sql);
		return $result;
	}

	function duplicate_check($table, $field, $value){
		$result = $this->con->query("SELECT count(*) as total from $table where $field='$value'");
		$row = mysqli_fetch_assoc($result);
		if($row['total']==0)
			return false;
		else
			return true;
	}

	function select_code($table, $id){
		$result = $this->con->query("SELECT focus_code from $table where id =".$id);
		$row = $result->fetch_assoc();
		return ($row['focus_code']??" ");
	}


	function check_table_pattern_exist($division_id, $table_id){

		$result = $this->con->query("SELECT count(*) as total from fcode_pattern where division_id=".$division_id." && table_id = ".$table_id) or $this->Error();
		$row = $result->fetch_assoc();
		if($row['total']==0)
			return false;
		else
			return true;
	}

	function get_max_sort_order($division_id, $table_id){

		$result = $this->con->query("SELECT max(sort_order) as sort_order from fcode_pattern where division_id=".$division_id);
		$row = $result->fetch_assoc();
		if(isset($row['sort_order']))
			return $row['sort_order'];
		else
			return 0;
		
	}

	function select_pattern_id($division_id, $sub_division_id, $brand_id){
        if($division_id>0) {
			$where =" where division_id=".$division_id;
			if($sub_division_id>0)
				$where .=" AND sub_division_id=".$sub_division_id;
			else
				$where .=" AND sub_division_id is NULL";
			if($brand_id)
				$where .=" AND brand_id=".$brand_id;
			else
				$where .=" AND brand_id IS NULL";
		
			$result = $this->con->query("SELECT id from fcode_pattern ".$where) or $this->Error();
			$row = $result->fetch_assoc();
			if($brand_id && mysqli_num_rows($result) == 0) {
				return $this->select_pattern_id($division_id,$sub_division_id,NULL);
			} else {
				return $row ? $row['id'] : null;
			}
	    } else {
			return null;
		}
		

	}
	
	function select_table_pattern_list($division_id, $sub_division_id,$own_brand,$brand_id){

			// $where =" where division_id=".$division_id;
			// if($sub_division_id)
			// 	$where .=" AND sub_division_id=".$sub_division_id;
			// else
			// 	$where .=" AND sub_division_id is NULL";

			$pattern_id = $this->select_pattern_id($division_id, $sub_division_id,$brand_id);
			$pattern_array = [];
			if($pattern_id){
                // echo "SELECT CONCAT(pattern_ids, ',', description_ids) as pattern_ids , origin_brand_alternative  from fcode_pattern where id =".$pattern_id;
				// exit;
				$result = $this->con->query("SELECT CONCAT_WS(',', pattern_ids, description_ids)  as pattern_ids , origin_brand_alternative  from fcode_pattern where id =".$pattern_id) or $this->Error();

				
				if(mysqli_num_rows($result)>0){
					$row_pattern = $result->fetch_assoc();
				
					if($row_pattern['pattern_ids']){

						$pattern_array = array_filter(explode(",", $row_pattern['pattern_ids'] ));
						//var_dump($pattern_array);exit;
						// echo "hello";

						if($own_brand == 1 && $row_pattern['origin_brand_alternative']==1 && $this->in_array_all([3,5],$pattern_array) && !in_array(4, $pattern_array)  ){ // 
						
							$new_pattern_array = [];
							foreach (array_keys($pattern_array, ["3","5","4"]) as $key) {
								unset($pattern_array[$key]);
							}
							
							foreach($pattern_array as $val){
								if($val==5) // model
									array_push($new_pattern_array, "3","5","4");
								else if(!in_array($val, ["3","5","4"] ))
									$new_pattern_array[] = $val;							
							}
							// $pattern_array = (array_unique($new_pattern_array)); 
				
						
							$pattern_array = $new_pattern_array;
							
						}
						
					}
				}

			}
		
			return array_unique($pattern_array);
	}



	function select_division_with_pattern(){

		$sql = "SELECT division.*, GROUP_CONCAT(f_table.name ORDER by pattern.sort_order SEPARATOR ' - ' ) as table_pattern 
				from mis_division as division 
				join fcode_pattern as pattern on pattern.division_id = division.id
				join fcode_table as f_table on f_table.id = pattern.table_id
				group by division.id";

		$result = $this->con->query($sql) or $this->Error();
		return $result;
	}


	function select_table_pattern($division_id){

		$sql = "SELECT GROUP_CONCAT(f_table.name  SEPARATOR ' - ' ) as name 
				from fcode_pattern as pattern	
				join fcode_table as f_table on f_table.id = pattern.table_id
				where pattern.division_id = $division_id
				group by pattern.division_id";

		$result = $this->con->query($sql) or $this->Error();
		$row = $result->fetch_assoc();

		return $row['name'];

	}


	function select_requested_pattern($request_id){

		$sql = "SELECT * from fcode_request_table where request_id =".$request_id." order by id ";
	
		$result = $this->con->query($sql) or $this->Error();
		return $result;

	}

	function select_requested_pattern_edit($request_id){

		$sql = "SELECT * from fcode_request_table where request_id =".$request_id." order by f_table_id ";
	
		$result = $this->con->query($sql) or $this->Error();
		return $result;

	}

	function select_all_request($where,$paginationStart, $limit,$sort){

		 $sql = "SELECT req.*,user.name as user_name,user.email,user.phone_no from fcode_request  req 
				 left join mis_branches as branch ON branch.id = req.branch_id
				 left join fcode_user as user ON user.id = req.created_by
				 where bulk_upload is null 
				 $where".$_SESSION['branch_search']."
				 order by id $sort LIMIT $paginationStart, $limit";
		
		$result = $this->con->query($sql) or $this->Error();
		return $result;
	} 

	function select_all_request_export($where){
	
		$sql = "SELECT req.*,user.name as user_name,user.email,user.phone_no from fcode_request  req 
				left join mis_branches as branch ON branch.id = req.branch_id
				left join fcode_user as user ON user.id = req.created_by
				where bulk_upload is null and 
				$where".$_SESSION['branch_search']."
				 order by id desc";
	   
	   $result = $this->con->query($sql) or $this->Error();
	   return $result;
   } 

   function select_all_created_export($where){
	
		$sql = "SELECT req.*,user.name as user_name,user.email,user.phone_no from fcode_request  req 
		left join mis_branches as branch ON branch.id = req.branch_id
		left join fcode_user as user ON user.id = req.created_by
		where  req.code is not null and req.is_create=1 and req.code!='' && req.status=1
		$where".$_SESSION['branch_search']."
				order by req.created_at desc";

		$result = $this->con->query($sql) or $this->Error();
		return $result;
	} 
   

	function count_all_request($where){

		$sql = "SELECT req.* from fcode_request req 
				 left join mis_branches as branch ON branch.id = req.branch_id
				 where bulk_upload is null
				 $where".$_SESSION['branch_search'];

		$result = $this->con->query($sql) or $this->Error();
		return mysqli_num_rows($result);
	}  
	
	function select_all_request_approved($where,$paginationStart, $limit){

		
		$sql = "SELECT req.*,user.name as user_name,user.email,user.phone_no from fcode_request  req 
				left join mis_branches as branch ON branch.id = req.branch_id
				left join fcode_user as user ON user.id = req.created_by
				where  req.code is not null and req.is_create=1 and req.code!='' && req.status=1
				$where".$_SESSION['branch_search']."
				order by req.created_at desc LIMIT $paginationStart, $limit";
	   
	   $result = $this->con->query($sql) or $this->Error();
	   return $result;
   } 


   function count_all_request_approved($where){

	$sql = "SELECT req.*,user.name as user_name,user.email,user.phone_no from fcode_request  req 
	left join mis_branches as branch ON branch.id = req.branch_id
	left join fcode_user as user ON user.id = req.created_by
	where  req.code is not null and req.is_create=1 and req.code!='' && req.status=1
	$where".$_SESSION['branch_search']."";

	$result = $this->con->query($sql) or $this->Error();
	return mysqli_num_rows($result);
}  


	function select_approved_code($where, $limit=null){
		
		$sql = "SELECT req.*, 
				 divis.name as division_name,
				 sub_divis.name as sub_division_name, 
				 brand.name as brand_name
				 from fcode_request req 
				 left join mis_branches as branch ON branch.id = req.branch_id

				 left join api_brand as brand ON brand.id = req.brand_id
				 left join mis_division as divis ON divis.id = req.division_id
				 left join mis_sub_division as sub_divis ON sub_divis.id = req.sub_division_id

				 where  req.code is not null and req.is_create=1 and req.code!='' && req.status=1 
				 $where  ORDER BY req.created_at desc $limit";

				//  die($sql);

		$result = $this->con->query($sql) or $this->Error();
		return $result;
		
	}


	function select_all_pattern($division_id, $sub_division_id, $brand_id){

        $where = "where 1=1 ";
		$sql = "SELECT pattern.*, d.name as divsion, sub_d.name as sub_division, brand.name as brand_name
				from fcode_pattern as pattern
				left join mis_division as d ON d.id = pattern.division_id
				left join mis_sub_division as sub_d ON sub_d.id = pattern.sub_division_id
				left join api_brand as brand ON brand.id = pattern.brand_id ";
		if($division_id) {
			$where .= "AND pattern.division_id=$division_id";
		} if ($sub_division_id) {
			$where .= " AND pattern.sub_division_id=$sub_division_id";
		} if ($brand_id) {
			$where .= " AND pattern.brand_id=$brand_id";
		}
		$sql .= $where." order by d.id";
		
		$result = $this->con->query($sql) or $this->Error();
		return $result;
	}


	function check_pattern_assigned($division_id, $sub_division_id, $brand_id, $id=null){

		$sql = "SELECT count(*) as total from fcode_pattern where division_id = $division_id";

		if($sub_division_id)
			$sql.= " AND sub_division_id = $sub_division_id ";
		else
			$sql.= " AND sub_division_id is null";

		if($brand_id)
			$sql.= " AND brand_id = $brand_id ";
		else
			$sql.= " AND brand_id is null";

		if($id)
			$sql.= " AND id != $id";

		//die($sql);

		$result = $this->con->query($sql) or $this->Error();

		$row = $result->fetch_assoc();

		if($row['total'] == 0)
			return false;
		else
			return true;
 
	}

	function get_pattern($id){
		$result = $this->con->query("SELECT pattern_ids
									from fcode_pattern as pattern
									where pattern.id = $id") or $this->Error();
			
		$row = $result->fetch_assoc();

		$pattern_array = array();
		if($row['pattern_ids']){
			foreach(explode(",", $row['pattern_ids'] ) as $val){
				if($val == "0")
					$pattern_array[] = "<i>MANUAL ENTRY</i>";
				else
					$pattern_array[] = $this->select_name_by_id("fcode_table", $val);
			}
		}

		return implode(" - ", $pattern_array);
	}

	function get_pattern_by_ids($where){
		$result = $this->con->query("SELECT pattern_ids
									from fcode_pattern as pattern
									where $where") or $this->Error();
		$row = $result->fetch_assoc();
		$pattern_array = array();
		if($row['pattern_ids']){
			foreach(explode(",", $row['pattern_ids'] ) as $val){
				if($val == "0")
					$pattern_array[] = "<i>MANUAL ENTRY</i>";
				else
					$pattern_array[] = $this->select_name_by_id("fcode_table", $val);
			}
		}

		return $pattern_array;
	}



	function get_pattern_description($id){
		$result = $this->con->query("SELECT description_ids
									 from fcode_pattern as pattern
									 where pattern.id = $id") or $this->Error();
			
		$row = $result->fetch_assoc();

		$pattern_array = array();
		if($row['description_ids']){
			foreach(explode(",", $row['description_ids'] ) as $val){
				if($val == "0")
					$pattern_array[] = "<i>MANUAL ENTRY</i>";
				else
					$pattern_array[] = $this->select_name_by_id("fcode_table", $val);
			}
		}

		return implode(" - ", $pattern_array);
	}


	function get_pattern_by_request($field, $request_id){
		$result = $this->con->query("SELECT pattern.$field
									 from fcode_request as request
									 left join fcode_pattern as pattern ON request.pattern_id = pattern.id
									 where request.id = $request_id
									 ") or $this->Error();
			
		$row = $result->fetch_assoc();

		return $row[$field];
	}

	function select_f_table($id){

		$result = $this->con->query("SELECT f_table from fcode_table where id=".$id);
		$row = $result->fetch_assoc();
		return $row['f_table'];

	}
	
	function getCodeValueByRequestTable($request_id, $table_id, $pattern_id){
		$result = $this->con->query("SELECT is_manual,f_table_val,item_name from fcode_request_table  where request_id = $request_id AND f_table_id = $table_id") or $this->Error();
		$row = $result->fetch_assoc();
		$val = NULL;
		if($row){
			if($table_id==0)
				$val = $row['f_table_val'] ; 
			elseif($row['is_manual'] == 1) 
			    $val =  $this->clean($row['f_table_val'] );
			else{
				$code = "";
				if($table_id >0 && $row['f_table_val']>0) {
					//if($row['item_name']!="") $code = $row['item_name'];
					$code = $this->getCodeAutoGenerate($row['f_table_val'],$table_id,$pattern_id);
				}
				$val = $code;	
			}
		}
		return $val;
	}

	function getCodeAutoGenerate($f_table_val, $table_id, $pattern_id){
		
		if($table_id==0) {
			$val = $this->clean($f_table_val); 
		} else {
			if ($table_id == 24) {
				$fcode_result = $this->select_all_data("fcode_pattern_code"," parent_id='".$pattern_id."' AND f_table_id='".$table_id."' AND f_table_val IN(".$f_table_val.") AND code_id IS NOT NULL");
			} else {
				$fcode_result = $this->select_all_data("fcode_pattern_code"," parent_id='".$pattern_id."' AND f_table_id='".$table_id."' AND f_table_val='".$f_table_val."' AND code_id IS NOT NULL");
			}
			if($fcode_result) { 
				$code_id = $fcode_result['code_id'];
				$code_row = $this->select_all_by_id("fcode_table_code", $code_id);
				$code = $code_row['focus_code'];
			} else {
				$rel_id = $this->select_all_by_id('fcode_table',$table_id);
                if ($rel_id['rel_id']) $table_id = $rel_id['rel_id'];
				if ($table_id == 24) {
					$code = $this->groupConcatCondition($table_id, $f_table_val);
				} else {
					$fcode_result = $this->select_all_data("fcode_table_code"," f_table_id='".$table_id."' AND f_table_val='".$f_table_val."' AND is_primary=1");
					$code = $fcode_result['focus_code'];
				}
				
			}
			$val = $code;	
		}
        return $val;
	}

	function groupConcatCondition($table_id, $f_table_val) {
		
		$result = $this->con->query("SELECT GROUP_CONCAT(focus_code SEPARATOR '') AS code FROM fcode_table_code WHERE f_table_id='".$table_id."' AND f_table_val IN(".$f_table_val.") AND is_primary=1") or $this->Error();
		$row = $result->fetch_assoc();
		return ($row?$row['code']:"");
	}
	
	
	function getNameValueByRequestTable($request_id, $table_id, $pattern_id){
		$result = $this->con->query("SELECT f_table_val,is_manual,item_name from fcode_request_table  where request_id = $request_id AND f_table_id = $table_id") or $this->Error();
		$row = $result->fetch_assoc();
		
		$val = null;
		if($row){
			if($table_id==0)
				$val = $row['f_table_val']; 
			elseif($row['is_manual'] == 1) 
			    $val =  $row['f_table_val'];
			else{
				$name = " ";
				if($table_id >0 && $row['f_table_val']>0) {
					$name = ($row['item_name']?$row['item_name']:$this->getNameAutoGenerate($row['f_table_val'],$table_id,$pattern_id));
			    }
				$val = $name;
			}
		}
		return $val;
	}

	function getNameAutoGenerate($f_table_val, $table_id, $pattern_id){
		if($table_id==0) {
			$val = $f_table_val; 
		} else {
			if ($table_id == 24) {
			    $name_result = $this->select_all_data("fcode_pattern_name"," parent_id='".$pattern_id."' AND f_table_id='".$table_id."' AND f_table_val='".$f_table_val."' AND name_id IS NOT NULL");
			} else {
				$name_result = $this->select_all_data("fcode_pattern_name"," parent_id='".$pattern_id."' AND f_table_id='".$table_id."' AND f_table_val IN(".$f_table_val.") AND name_id IS NOT NULL");
			}
			if($name_result) { 
				$name_id = $name_result['name_id'];
				$name_row = $this->select_all_by_id("fcode_table_alternative_name", $name_id);
				$name = $name_row['name'];
			} else {
				$rel_id = $this->select_all_by_id('fcode_table',$table_id);
                if ($rel_id['rel_id']) $table_id = $rel_id['rel_id'];
				$table = $this->select_f_table( $table_id);
				if($table_id == 24) {
					$name = $this->groupConcatConditionName($table, $f_table_val);
				} else {
					$name = $this->select_name_by_id($table, $f_table_val);
				}
				
			}
			$val = $name ;	
		}
		return ucwords(strtolower($val));
       
	}

	function groupConcatConditionName($table, $f_table_val) {
		
		$result = $this->con->query("SELECT GROUP_CONCAT(name SEPARATOR ' ') AS name FROM $table WHERE id IN(".$f_table_val.")") or $this->Error();
		$row = $result->fetch_assoc();
		return ($row?$row['name']:"");
	}
    

	function codePatternCheck($f_table_val,$table_id,$pattern_id,$new_val){
        $pattern_array = $this->select_all_by_id("fcode_pattern",$pattern_id);
		$pattern = $pattern_array['pattern_ids'];
		$code = "";
		if($pattern) {
			foreach(explode(",",$pattern) as $val)
			{   
				if ($val == $table_id)  {
					if($f_table_val > 0){

						$code = $this->getCodeAutoGenerate($f_table_val,$table_id,$pattern_id); 
					} else {
						$code = $this->clean($new_val);
					}
					
				} 
			}
		}
		return $code;
	}

	function namePatternCheck($f_table_val,$table_id,$pattern_id,$new_val){
        $pattern_array = $this->select_all_by_id("fcode_pattern",$pattern_id);
		$pattern = $pattern_array['description_ids'];
		$name = "";
		if($pattern) {
			foreach(explode(",",$pattern) as $val)
			{
				if ($val == $table_id)  {
					if($f_table_val > 0){
					   $name = $this->getNameAutoGenerate($f_table_val,$table_id,$pattern_id)." "; 
					} else {
						$name = $new_val." "; 
					}
				} 
			}
		}
		return ucwords(strtolower($name));
	}
	


	function generate_fcode($request_id,$pattern_id = NULL){
		
		// $pattern = [];
		// $result_table = $this->select_requested_pattern($request_id);
		// while($row_table = $result_table->fetch_assoc()){
		// 	if($row_table['f_table_id'] != 0 ){
		// 		$row_p = $this->select_all_by_id("fcode_table", $row_table['f_table_id']);
		// 		$pattern[] = $this->select_code($row_p['f_table'], $row_table['f_table_val']); 
		// 	}                              
		// }
		// return implode("", $pattern);
		
		
		$pattern_array = [];
		$pattern = $this->get_pattern_by_request("pattern_ids",$request_id);
		if($pattern){	
			foreach(explode(",",$pattern) as $key => $table_id) {
				$pattern_array[] = $this->getCodeValueByRequestTable($request_id, $table_id, $pattern_id);
			}
		}
		if (in_array("24", explode(",",$pattern))) {
			$result = $this->con->query("SELECT f_table_val from fcode_request_table  where request_id = $request_id AND f_table_id = 24") or $this->Error();
		    $row = $result->fetch_assoc();
			if ($row['f_table_val']>0) {
				$oldval = array_search('24',explode(",",$pattern));
				$arr = explode(",",$row['f_table_val']);
				$postition = $this->select_field_by_id("api_condition", "position", $arr[0]);
				if ($postition == "Last") $index_key = $key; else  $index_key = 0; 
				$this->moveElement($pattern_array, $oldval, $index_key);	
			}		
		}
		
		return implode("", $pattern_array);
	}
    

	function moveElement(&$array, $a, $b) {
		$out = array_splice($array, $a, 1);
		array_splice($array, $b, 0, $out);
	}
	
	function generate_description($request_id, $pattern_id = NULL){
		$pattern_array = [];
		$pattern = $this->get_pattern_by_request("description_ids",$request_id);
		if($pattern){			
			foreach(explode(",",$pattern) as $key =>  $table_id)
				$pattern_array[] = $this->getNameValueByRequestTable($request_id, $table_id, $pattern_id);			
		}
		if (in_array("24", explode(",",$pattern))) {
			$result = $this->con->query("SELECT f_table_val from fcode_request_table  where request_id = $request_id AND f_table_id = 24") or $this->Error();
		    $row = $result->fetch_assoc();
			if ($row['f_table_val']>0) {
				$oldval = array_search('24',explode(",",$pattern));
				$arr = explode(",",$row['f_table_val']);
				$postition = $this->select_field_by_id("api_condition", "position", $arr[0]);
				if ($postition == "Last") $index_key = $key; else  $index_key = 0; 
				$this->moveElement($pattern_array, $oldval, $index_key);	
			}		
		}
		return implode(" ", $pattern_array);
	}

	function check_code_exist($fcode,$request_id){

		$result = $this->con->query("SELECT count(*) as total from fcode_request where code='".$fcode."' AND id!=$request_id");
		$row = $result->fetch_assoc();
		if($row['total']==0)
		   return false;
		else
		  return true;
		
	}

	function check_code_generated($request_id){

		$result = $this->con->query("SELECT code from fcode_request where id=".$request_id);
		$row = $result->fetch_assoc();
		
		if($row['code'] && $row['code'] != "" )
			return true;
		else
			return false;
		
	}

	function generatedCode($request_id){
		$result = $this->con->query("SELECT code from fcode_request where id=".$request_id);
		$row = $result->fetch_assoc();
		return ( $row['code'] && $row['code'] != ""  ) ? $row['code'] : null;
	}

	function check_if_already_exist_name($table, $name, $division_id=null){

		$sql = "SELECT count(id) as total from  $table where name = '$name'  ";
		if($division_id)
			$sql .= " AND division_id=".$division_id;
        // echo $sql;
		// exit;
		$result = $this->con->query($sql) or $this->Error();
		$row = mysqli_fetch_assoc($result);
		return $row['total'];
	}

	function check_if_already_exist_master_name($table, $name, $id=null){
        $where = " ";
		if($table == "mis_sub_division") $where = " AND is_fcode=1";
		$sql = "SELECT count(id) as total from  $table where name = '$name'  $where";
		if($id)
			$sql .= " AND id!=".$id;
        // echo $sql;
		// exit;
		$result = $this->con->query($sql) or $this->Error();
		$row = mysqli_fetch_assoc($result);
		return $row['total'];
	}

	function get_if_already_exist_id($table, $name, $division_id=null){

		$sql = "SELECT id from  $table where name = '$name'  ";
		if($division_id)
			$sql .= " AND division_id=".$division_id;

		$result = $this->con->query($sql) or $this->Error();
		$row = mysqli_fetch_assoc($result);
		if($row)
			return $row['id'];
		else
			return NULL;
	}

	function check_if_already_exist_code($table, $code, $division_id=null){

		$sql = "SELECT count(id) as total from  $table where focus_code = '$code'  ";
		if($division_id)
			$sql .= " AND division_id=".$division_id;

		$result = $this->con->query($sql) or $this->Error();
		$row = mysqli_fetch_assoc($result);
		return $row['total'];
	}

	function check_if_already_exist_f_code( $code, $division_id=null){

		$sql = "SELECT count(id) as total from  fcode_request where code = '$code'  ";
		if($division_id)
			$sql .= " AND division_id=".$division_id;

		$result = $this->con->query($sql) or $this->Error();
		$row = mysqli_fetch_assoc($result);
		return $row['total'];
	}

	function get_duplicate($arr) {

		$arr_unique = array_unique($arr);
		$arr_duplicates = array_diff_assoc($arr, $arr_unique);
	  
		return $arr_duplicates;
	}

	public function Name_Check($table, $name){

        $id = $this->select_id_by_name($table, $this->escape($name));
		// echo $id."-"; 
        if($id)
            return $id;
        else
            return false;        

    }


	function select_id_by_name_sub_div($table, $name,$mdiv){
		$id = null;
		if($name!=""){
			$result = $this->con->query("SELECT id from $table where division_id=$mdiv and name='$name'");
			$row = $result->fetch_assoc();
			if(isset($row['id']))
				$id =  $row['id'];
		}
            return $id;
	}

	function existing_pattern($division_id, $sub_division_id, $brand_id){

		$sql = "SELECT id from fcode_pattern where division_id = $division_id";

		if($sub_division_id)
			$sql.= " AND sub_division_id = $sub_division_id ";
		else
			$sql.= " AND sub_division_id is null";

		if($brand_id)
			$sql.= " AND brand_id = $brand_id ";
		else
			$sql.= " AND brand_id is null";
	
		$result = $this->con->query($sql);
		$row = $result->fetch_assoc();

		if($row)
			return $row['id'];
		else
			return NULL;

	}


	function process_code_pattern_excel($file){
		

		$error = array();        

		$file_name = $file['code_pattern']['name'];
		$file_name = date("y-m-d_H-i-s").$file_name;

		$finalFilePath = 'uploads/temp/pattern/'.$file_name;
		
		if(move_uploaded_file( $file['code_pattern']['tmp_name'], $finalFilePath )){

			  $objPHPExcel = new PHPExcel();

			  $inputFileName = $finalFilePath;

			  try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			  } catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .$e->getMessage());
			  }

			  $sheet = $objPHPExcel->getSheet(0);
			  $highestRow = $sheet->getHighestRow();
			  $highestColumn = $sheet->getHighestColumn();
			  $activeSheet = $objPHPExcel->getActiveSheet();

			  $head_1 = $this->escape($activeSheet->getCell('A1')->getValue());
			  $head_2 = $this->escape($activeSheet->getCell('B1')->getValue());
			  $head_3 = $this->escape($activeSheet->getCell('C1')->getValue());
			  $head_4 = $this->escape($activeSheet->getCell('D1')->getValue());
			  $head_5 = $this->escape($activeSheet->getCell('E1')->getValue());
				

				$this->con->autocommit(false);

			    if( $head_1 != "DIVISION"  || $head_2 != "SUB DIVISION" || $head_3 != "BRAND" || $head_4 != "CODE PATTERN" || $head_5 != "DESCRIPTION PATTERN"   ) {
					$error["Invalid_Format"][] = "Inavlid Excel File";
				}
				else{
					
					$succes_count = $updated_count =0;
					for ($row = 2; $row <= $highestRow; $row++) {

						$division = $activeSheet->getCell('A' . $row)->getValue();
						$sub_division = $activeSheet->getCell('B' . $row)->getValue();
						$brand = $activeSheet->getCell('C' . $row)->getValue();
						$pattern = $activeSheet->getCell('D' . $row)->getValue();
						$description_pattern = $activeSheet->getCell('E' . $row)->getValue();

						$division_id = $this->Name_Check("mis_division", $division);
						$sub_division_id= $this->select_division_id_by_name("mis_sub_division", $sub_division,$division_id);
						$brand_id = $this->Name_Check("api_brand", $brand);

						$sub_error = $brand_error = false;
						if($division_id == false){
							$sub_error = true;
							$error['division_not_found'][] = $division;
						}if($sub_division_id == false){
							$sub_error = true;
							$error['sub_division_not_found'][] = $sub_division;
						}
						// if($brand_id == false){
						// 	$brand_error = true;
						// 	$sub_error = true;
						// 	$error['brand_not_found'][] = $brand_id;
						// }


						
						
						$new_array = [];
						
						$pattern_array = array_map('trim',explode("-", $pattern));
						
						if(isset($pattern) && $pattern!="") {
							
							foreach($pattern_array as $val){
								$table_id = $this->Name_Check("fcode_table", $val);
								if($table_id==false){
									$sub_error = true;
									$error['pattern_code_master_not_found'][] = $val;
								}else{
									$new_array[] = $table_id;
								}
							}
						} 
						

						$description_pattern_array = array_map('trim',explode("-", $description_pattern));
						$description_new_array = [];
						if(isset($description_pattern) && $description_pattern!="") {
							foreach($description_pattern_array as $val){
								$table_id = $this->Name_Check("fcode_table", $val);
								if($table_id==false){
									$sub_error = true;
									$brand_error = true;
									$error['pattern_code_master_not_found'][] = $val;
								}else{
									$description_new_array[] = $table_id;
								}

							}
						}

						if($sub_error == false){
							
							$data = [];
							$data['division_id'] = $division_id;
							$data['sub_division_id'] = $sub_division_id;
							$data['brand_id'] = $brand_id;
							$data['pattern_ids'] = implode(",",$new_array);
							$data['description_ids'] = implode(",",$description_new_array);
							$exist_id = $this->existing_pattern($division_id, $sub_division_id, $brand_id);

							if($exist_id){
								$this->update_data("fcode_pattern", $data, $exist_id);
                    			++$updated_count;
							}else{
								$this->insert_data("fcode_pattern", $data);
                    			++$succes_count;
							}
							
						}

					}

					
				}

				
				if(!empty($error)){	
					unlink($finalFilePath);
					$report = $this->super_unique($error);	
					ob_start();
					?>
						<h3> Following Masters Not Found </h3>
						<hr>

						<? foreach($report as $master => $master_val) { ?>
						<strong> <?=$master?> : </strong> <?=implode(", ",$master_val)?> <br>
						<?
					}
					$error = ob_get_contents();
					ob_end_clean();
					// echo $error; exit;
					$return =  array("error"=>$error);

					// var_dump($error); exit;
					
					
				}else{
					// exit;
					$this->con->commit();
					$return =  array("success"=>$succes_count." Rows Added, ".$updated_count." Rows Updated");
				}

				return $return;
				

		}

	}



	function process_focus_excel($file){

		$error  = $uploaded_and_atleast_one_record = false;
		$error_message ="";
		$excel_code_array = $excel_name_array = $existing_name_list = $existing_code_list = $duplicate_name_in_excel = $duplicate_code_in_excel=
		$divsion_array = $sub_divsion_array = $brand_array = array();

		$file_name = $file['focus_excel']['name'];
		$file_name = date("y-m-d_H-i-s").$file_name;

		$finalFilePath = 'uploads/temp/'.$file_name;

		if(move_uploaded_file( $file['focus_excel']['tmp_name'], $finalFilePath )){
			$objPHPExcel = new PHPExcel();

			  $inputFileName = $finalFilePath;

			  try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			  } catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .$e->getMessage());
			  }

			  $sheet = $objPHPExcel->getSheet(0);
			  $highestRow = $sheet->getHighestRow();
			  $highestColumn = $sheet->getHighestColumn();
			  $activeSheet = $objPHPExcel->getActiveSheet();


			  $head_1 = $this->escape($activeSheet->getCell('A1')->getValue());
			  $head_2 = $this->escape($activeSheet->getCell('B1')->getValue());
			  $head_3 = $this->escape($activeSheet->getCell('C1')->getValue());
			  $head_4 = $this->escape($activeSheet->getCell('D1')->getValue());
			  $head_5 = $this->escape($activeSheet->getCell('E1')->getValue());
			  $head_6 = $this->escape($activeSheet->getCell('F1')->getValue());
			  $head_7 = $this->escape($activeSheet->getCell('G1')->getValue());
			  $head_8 = $this->escape($activeSheet->getCell('H1')->getValue());

			  
			  	if( $head_1 != "NAME" || $head_2 != "CODE"  || $head_3 != "DIVISION"  || $head_4 != "SUB DIVISION"  || $head_5 != "BRAND"   || $head_6 != "ARABIC NAME"   || $head_7 != "BARCODE"   || $head_8 != "HSN/SAC"  ) {
				// if($head_3!="DVISION"){
					$error = true;
					// exit;
					$error_message = "Inavlid Excel File";
				}
				else{
					
					// echo $head_3; exit;
					// echo $head_1.$head_2.$head_3.$head_4.$head_5; exit;
					for ($row = 2; $row <= $highestRow; $row++) {
						$name = $this->escape($activeSheet->getCell('A' . $row)->getValue());
						$code = $this->escape($activeSheet->getCell('B' . $row)->getValue());

						if($name !="" && $code!=""){

							$uploaded_and_atleast_one_record =true;

							$excel_name_array[] = $name;
							$excel_code_array[] = $code;

							$divsion = $this->escape($activeSheet->getCell('C' . $row)->getValue());
							$sub_division = $this->escape($activeSheet->getCell('D' . $row)->getValue());
							$brand = $this->escape($activeSheet->getCell('E' . $row)->getValue());

							if(!$this->select_id_by_name("mis_division",$divsion))
								$divsion_array[] = $divsion;
							if(!$this->select_id_by_name("mis_sub_division",$sub_division))
								$sub_divsion_array[] = $sub_division;
							if(!$this->select_id_by_name("api_brand",$brand))
								$brand_array[] = $brand;

						 	// $existing_name = $this->check_if_already_exist_name("fcode_request",$name);

							// if($existing_name !=0){
							// 	$error = true;
							// 	$existing_name_list[]= $name;
							// }

							// $existing_code = $this->check_if_already_exist_f_code($code);

							// if($existing_code !=0){
							// 	$error = true;
							// 	$existing_code_list[]= $code;
							// }
							
						}
					}

					$duplicate_name_in_excel = $this->get_duplicate($excel_name_array);
					$duplicate_code_in_excel = $this->get_duplicate($excel_code_array);

					 if(count($duplicate_name_in_excel)>0)
					  	$error = true;
					if(count($duplicate_code_in_excel)>0)
					  	$error = true;

					if(count($divsion_array)>0 || count($sub_divsion_array)>0 || count($brand_array)>0)
					  	$error = true;

				}

				if($error == false && $uploaded_and_atleast_one_record ==true ){

					$success_count = $updated_count = 0;
					$this->con->autocommit(false);
					for ($row = 2; $row <= $highestRow; $row++) {
						$data["name"] = $name = $activeSheet->getCell('A' . $row)->getValue();
						$data["code"] = $code = $activeSheet->getCell('B' . $row)->getValue();

						if($name !="" && $code!=""){

							$divsion = $this->escape($activeSheet->getCell('C' . $row)->getValue());
							$sub_division = $this->escape($activeSheet->getCell('D' . $row)->getValue());
							$brand = $this->escape($activeSheet->getCell('E' . $row)->getValue());

							
							$data["division_id"] = $this->select_id_by_name("mis_division",$divsion);
							$data["sub_division_id"] = $this->select_id_by_name("mis_sub_division",$sub_division);
							$data["brand_id"] = $this->select_id_by_name("api_brand",$brand);						



							$data["name_arabic"] = $this->escape($activeSheet->getCell('F' . $row)->getValue());
							$data["barcode"] = $this->escape($activeSheet->getCell('G' . $row)->getValue());
							$data["hsn"] = $this->escape($activeSheet->getCell('H' . $row)->getValue());
							
							$data['bulk_upload'] =1;

							// echo "helo";
							

							if($this->check_if_already_exist_f_code($code)){
								$this->update_data_with_where("fcode_request", $data, " where code='$code' ");
								++$updated_count;
							}
							else if($this->insert_data("fcode_request",$data)){
								++$success_count;
							}
						}

					}

					
				

					$this->con->commit();
				}

		}

		ob_start();
		if($error==true){
			unlink($finalFilePath); 
			?>
		<div class="alert alert-danger ">
			<a href="#" class="close" data-dismiss="alert">&times;</a>
			<strong>

			<? 
			if($error_message!="") 
				echo $error_message."<br/>";
			if(count($existing_code_list)>0)
				echo "Following Code already Exist <br/><br/><small>".implode("<br/> ", array_unique($existing_code_list))."</small><br/><hr/>";
			if(count($existing_name_list)>0)
				echo "Following name already Exist <br/><br/><small>".implode("<br/>  ", array_unique($existing_name_list))."</small><br/><hr/>";
			if(count($duplicate_name_in_excel)>0)
				echo "Following Names are duplicate in your uploaded excel file <br/><br/><small>".implode("<br/>  ", array_unique($duplicate_name_in_excel))."</small><br/><hr/>";
			if(count($duplicate_code_in_excel)>0)
				echo "Following Codes are duplicate in your uploaded excel file <br/><br/><small>".implode("<br/>  ", array_unique($duplicate_code_in_excel))."</small><br/><hr/>";
			if(count($divsion_array)>0)
				echo "Following Divsions are not found <br/><br/><small>".implode("<br/>  ", array_unique($divsion_array))."</small><br/><hr/>";
			if(count($sub_divsion_array)>0)
				echo "Following Sub Divsions are not found <br/><br/><small>".implode("<br/>  ", array_unique($sub_divsion_array))."</small><br/><hr/>";
			if(count($brand_array)>0)
				echo "Following Brand are not found <br/><br/><small>".implode("<br/>  ", array_unique($brand_array))."</small><br/><hr/>";
			?>

			</strong>
		</div>
	<? }
		if(isset($success_count) || isset($updated_count)){ ?>
			<div class="alert alert-success ">
				<a href="#" class="close" data-dismiss="alert">&times;</a>
				<? if(isset($success_count)){ ?>
				<strong>
					<? echo $success_count." Record Added";  ?></strong>
				<? }if(isset($updated_count)){?>
				<br /><strong class="text-info">
					<? echo $updated_count." Record Updated";  ?></strong>
				<? } ?>
			</div>
	<? }
	$result = ob_get_contents();
	ob_end_clean();

	return trim($result);

	}



	function process_excel($table, $file, $division_id=null){

		$error  = $uploaded_and_atleast_one_record = false;
		$error_message ="";
		$excel_code_array = $excel_name_array = $existing_name_list = $existing_code_list = $duplicate_name_in_excel = $duplicate_code_in_excel = array();

		$file_name = $file['excel']['name'];
		$file_name = date("y-m-d_H-i-s").$file_name;

		$finalFilePath = 'uploads/temp/'.$file_name;

		if(move_uploaded_file( $file['excel']['tmp_name'], $finalFilePath )){


			$objPHPExcel = new PHPExcel();

			  $inputFileName = $finalFilePath;

			  try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			  } catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .$e->getMessage());
			  }

			  $sheet = $objPHPExcel->getSheet(0);
			  $highestRow = $sheet->getHighestRow();
			  $highestColumn = $sheet->getHighestColumn();
			  $activeSheet = $objPHPExcel->getActiveSheet();


			  $head_1 = trim($activeSheet->getCell('A1')->getValue());
			  $head_2 = trim($activeSheet->getCell('B1')->getValue());

			  	if( $head_1 != "NAME" || $head_2 != "CODE"  ) {
					$error = true;
					$error_message = "Inavlid Excel File";
				}
				else{
					for ($row = 2; $row <= $highestRow; $row++) {
						$name = $this->escape($activeSheet->getCell('A' . $row)->getValue());
						$code = $this->escape($activeSheet->getCell('B' . $row)->getValue());
						if($name !=""){

							$uploaded_and_atleast_one_record =true;

							$excel_name_array[] = $name;
							$excel_code_array[] = $code;

						 	// $existing_name = $this->check_if_already_exist_name($table,$name);

							// if($existing_name !=0){
							// 	$error = true;
							// 	$existing_name_list[]= $name;
							// }

							// $existing_code = $this->check_if_already_exist_code($table,$code);

							// if($existing_code !=0){
							// 	$error = true;
							// 	$existing_code_list[]= $code;
							// }
							
						}
					}

					$duplicate_name_in_excel = $this->get_duplicate($excel_name_array);
					// $duplicate_code_in_excel = $this->get_duplicate($excel_code_array);

					 if(count($duplicate_name_in_excel)>0)
					  	$error = true;
					if(count($duplicate_code_in_excel)>0)
					  	$error = true;

				}

				if($error == false && $uploaded_and_atleast_one_record ==true ){

					$success_count = $updated_count = 0;
					$this->con->autocommit(false);
					for ($row = 2; $row <= $highestRow; $row++) {
							$name = $this->escape($activeSheet->getCell('A' . $row)->getValue());
						  	$code = $this->escape($activeSheet->getCell('B' . $row)->getValue());
							if($name !=""){

								$data['name'] = $name;
								$udata['focus_code'] = $code;

								if( $table == "api_brand"){
									$data['own_brand'] = null;
									if($this->escape($activeSheet->getCell('C' . $row)->getValue()==1))
										$data['own_brand'] = 1;
								}

								$where1 = "";
								if($division_id){
									$data['division_id'] = $division_id;
									$where1 = " AND division_id=".$division_id;
								}
								
								$table_id = $this->select_all_by_field_id_process('fcode_table','f_table',$table);
								$udata['f_table_id'] =  $table_id['id'];
								if($this->check_if_already_exist_name($table,$name,$division_id)){
									$udata['f_table_val'] = $this->get_if_already_exist_id($table,$name,$division_id);
									$this->update_data_with_where($table, $data, " where name='$name' ".$where1);
									++$updated_count;
								}
								else if($table !="mis_division"){
									$udata['f_table_val'] = $this->insert_data($table,$data);
									++$success_count;
								}

								if ($udata['f_table_val']) {
									$udata['is_primary'] =  1;
									$this->insert_data("fcode_table_code", $udata);
								}
							}

					}
					$this->con->commit();
				}

		}

		ob_start();
		if($error==true){
			unlink($finalFilePath); ?>
			<div class="alert alert-danger ">
				<a href="#" class="close" data-dismiss="alert">&times;</a>
				<strong>

				<? 
					if($error_message!="") 
						echo $error_message."<br/>";
					if(count($existing_code_list)>0)
						echo "Following Code already Exist <br/><br/><small>".implode("<br/> ", array_unique($existing_code_list))."</small><br/><hr/>";
					if(count($existing_name_list)>0)
						echo "Following name already Exist <br/><br/><small>".implode("<br/>  ", array_unique($existing_name_list))."</small><br/><hr/>";
					if(count($duplicate_name_in_excel)>0)
						echo "Following Names are duplicate in your uploaded excel file <br/><br/><small>".implode("<br/>  ", array_unique($duplicate_name_in_excel))."</small><br/><hr/>";
					if(count($duplicate_code_in_excel)>0)
						echo "Following Codes are duplicate in your uploaded excel file <br/><br/><small>".implode("<br/>  ", array_unique($duplicate_code_in_excel))."</small><br/><hr/>";
				?>

				</strong>
			</div>
		<? }
		if(isset($success_count) || isset($updated_count)){ ?>
			<div class="alert alert-success ">
				<a href="#" class="close" data-dismiss="alert">&times;</a>
				<? if(isset($success_count)){ ?>
				<strong>
					<? echo $success_count." Record Added";  ?>
				</strong>
				<? }if(isset($updated_count)){?>
				<br />
				<strong class="text-info">
					<? echo $updated_count." Record Updated";  ?>
				</strong>
				<? } ?>
			</div>
		<? }
		$result = ob_get_contents();
		ob_end_clean();

		return trim($result);
	}

	function process_excel_new($file){

		$error   = false;
		$error_message ="";


		$file_name = $file['excel']['name'];

		$divisionId = $file['division_id'];

		$subDivisionId = $file['sub_division_id'];


		if($divisionId=='' || $subDivisionId==''){
	
			$error = true;
			$error_message .= "Division and Subdivision must be choosed";
		}

		if ($error) {
		ob_start();
		?>
		
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert">&times;</a>
				<strong>

				<? 
					if($error_message!="") 
						echo $error_message."<br/>";
					
				?>

				</strong>
			</div>
		<?
		$result = ob_get_contents();
		ob_end_clean();

		return trim($result);

		} 



		// $brandId = $file['brand_id'];
		// if($brandId==''){
		// 	$brandId="brand_id is null";
		// }else{
		// 	$brandId="brand_id ='$brandId'";
		// }


		$file_name = date("y-m-d_H-i-s").$file_name;

		$finalFilePath = 'uploads/temp/'.$file_name;


		$where = "division_id ='$divisionId' and sub_division_id='$subDivisionId'";
		$pattern_array = $this->get_pattern_by_ids($where);

	    $data = $this->select_all_by_field_id_process('mis_sub_division','id',$subDivisionId);

		if($data['show_item_name']==1){
			$fixedHeaders = ["ITEM NAME","NAME ARABIC", "HSN", "BARCODE", "PART NUMBER", "ADDITIONAL ITEM DESCRIPTION"];
		}else{
			$fixedHeaders = ["NAME ARABIC", "HSN", "BARCODE", "PART NUMBER", "ADDITIONAL ITEM DESCRIPTION"];
		}

         $reversedPatternArray = array_reverse($pattern_array);

		foreach ($reversedPatternArray as $columnIndex => $columnName) {
			array_unshift($fixedHeaders, $columnName);
		}

		if(move_uploaded_file( $file['excel']['tmp_name'], $finalFilePath )){
		
			$objPHPExcel = new PHPExcel();

			  $inputFileName = $finalFilePath;
			
			  try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			  } catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .$e->getMessage());
			  }
	
			  $sheet = $objPHPExcel->getSheet(0);
			  $highestRow = $sheet->getHighestRow();
			  $highestColumn = $sheet->getHighestColumn();
			  $activeSheet = $objPHPExcel->getActiveSheet();


		

			  $error = false;
			  $error_message = '';
			//   $fixedHeaders = ["NAME","CODE"];
			foreach ($fixedHeaders as $index => $header) {
				$cellValue = trim($activeSheet->getCellByColumnAndRow($index, 1)->getValue());
		
				if ($cellValue != $header) {
					$error = true;
					$error_message .= "Mismatch in header '$header' at column " . chr($index + 65) . "1. ";
				 
				}
			}
			ob_start();
			if ($error) {
				// echo "Error: " . $error_message;
				// exit;
                    ?>
				<div class="alert alert-danger ">
				<a href="#" class="close" data-dismiss="alert">&times;</a>
				<strong>

				<? 
					if($error_message!="") 
						echo $error_message."<br/>";
					
				?>

				</strong>
			</div>
            <?


			} else{
					
				$division_id = $model_id = $sub_division =$table_array=  null;
			
				//$columnTableMapping = $this->select_all_data('fcode_table');

				$columnTableMappingData = $this->select_all_fcode_table();
				$columnTableMapping = [];
				// $columnTableMapping = [
				// 	"DIVISION" => ["mis_division", "fcode_table"],
				// 	"MODEL" => ["api_model", "fcode_table"],
				// 	"BRAND" => ["api_brand", "fcode_table"],
				// 	"ORIGIN BRAND" => ["api_brand", "fcode_table"],
				// 	"SUB DIVISION" => ["mis_sub_division", "fcode_table"],
				// 	"COLOR" => ["api_color", "fcode_table"],
				// 	"RAM" => ["api_ram", "fcode_table"],
				// 	"STORAGE" => ["api_storage", "fcode_table"],
				// 	"CAPACITY" => ["api_capacity", "fcode_table"],
				// 	"PIN TYPE" => ["api_pin_type", "fcode_table"],
				// 	"QUALITY" => ["api_quality", "fcode_table"],
				// 	"SPEED" => ["api_speed", "fcode_table"],
				// 	"SPARE TYPE" => ["api_sparetype", "fcode_table"],
				// 	"PROCESSOR" => ["api_processor", "fcode_table"],
				// 	"HARD DRIVE" => ["api_hard_drive", "fcode_table"],
				// 	"GRAPHICS CARD" => ["api_graphics_card", "fcode_table"],
				// 	"INTERFACE" => ["api_interface", "fcode_table"],
				// 	"DATA RATE DESKTOP" => ["api_data_rate_desktop", "fcode_table"],
				// 	"DATA RATE LAPTOP" => ["api_data_rate_laptop", "fcode_table"],
				// 	"UNIT" => ["api_unit", "fcode_table"],
				// 	"WARRANTY" => ["api_warranty", "fcode_table"],
				// 	"OPERATING SYSTEM" => ["api_operating_system", "fcode_table"],
				// 	"DUMMY" => ["api_dummy", "fcode_table"],
				// 	"CONDITION" => ["api_condition", "fcode_table"],
					
				// ];

				foreach ($columnTableMappingData as $data) {
					$nameField = $data['name'];  
					$tableField = $data['f_table'];  
				
					if (in_array($nameField, $fixedHeaders)) {
						$columnTableMapping[$nameField] = [$tableField, "fcode_table"];
					}
				}
				$success_count =  0;
				$sub_error = false;





				
				for ($row = 2; $row <= $highestRow; $row++) {
					foreach ($fixedHeaders as $index => $header) {
						$cellValue = trim($activeSheet->getCellByColumnAndRow($index, 1)->getValue());



						if (isset($columnTableMapping[$cellValue]) && in_array($cellValue, $fixedHeaders)) {
							list($tableName, $rowTable) = $columnTableMapping[$cellValue];
				
							$divisionColumnIndex = array_search($cellValue, $fixedHeaders);
							$div = $this->escape($activeSheet->getCellByColumnAndRow($divisionColumnIndex, $row)->getValue());
                           
							
							if ($tableName === "api_condition") {
								// Handle multiple conditions
								$conditions = explode(',', $div);
				
								$allConditions = []; // Array to store all conditions
				
								foreach ($conditions as $condition) {
									$condition = trim($condition);
									$id = $this->Name_Check($tableName, $condition);
									$row_table = $this->select_id_by_fnames($rowTable, $tableName);
				
									if ($id === false) {		
											$sub_error = true;
											$error['error_found'][] = $cellValue . ' - ' . $condition;
									
									} else {
										$allConditions[] = $id; // Collect condition in the array
									}
								}
				
								// Assign concatenated conditions to $table_array[$row_table]
								$table_array[$row_table] = implode(',', $allConditions);
							
							
								
							} else {
								// Handle other columns

								if($tableName === 'mis_sub_division'){
									$id = $this->select_id_by_name_sub_div($tableName, $div,$divisionId);
									//$id = $this->Name_Check($tableName, $div);
								}else{
									$id = $this->Name_Check($tableName, $div);
								}

								// $tableName = mis_brand
								// rowTable =fcode_table
								// echo $rowTable;
								// exit;
								if($cellValue === "ORIGIN BRAND"){
								   $row_table = $this->select_id_by_fnames_new($rowTable, $tableName);
								}else{
									$row_table = $this->select_id_by_fnames($rowTable, $tableName);
								}
								// echo $rowTable;
								// exit;
                                

								if ($id === false) {
									$id = $div;
									$table_array[$row_table] = 'add';
									$POST[$row_table] = $id;
				
									// Handle specific conditions for mis_division and mis_sub_division
									if ($tableName === 'mis_division' || $tableName === 'mis_sub_division') {
										$sub_error = true;
										$error['error_found'][] = $cellValue . ' - ' . $div;
									}
								} else {
									$table_array[$row_table] = $id;

									// echo $table_array[$row_table];
									// exit;
								}
							}



						}

					}
				}
				if ($sub_error) {
					unlink($finalFilePath); 
					
					if(!empty($error)){	
						$report = $this->super_unique($error);	
					?>
					<div class="alert alert-danger ">
						 <a href="#" class="close" data-dismiss="alert">&times;</a>
							<strong>
			
							<? 
								foreach($report as $master => $master_val) { ?>
									<?=$master?> : <?=implode(", ",$master_val)?> <br>
									<?
								}
							?>
			
							</strong>
					</div>
				<? }
				}
				if($error == false){
					for ($row = 2; $row <= $highestRow; $row++) {
						foreach ($fixedHeaders as $index => $header) {
							$cellValue = trim($activeSheet->getCellByColumnAndRow($index, 1)->getValue());

							if (isset($columnTableMapping[$cellValue]) && in_array($cellValue, $fixedHeaders)) {
								list($tableName, $rowTable) = $columnTableMapping[$cellValue];
					
								$divisionColumnIndex = array_search($cellValue, $fixedHeaders);
								$div = $this->escape($activeSheet->getCellByColumnAndRow($divisionColumnIndex, $row)->getValue());


								if ($tableName === "api_condition") {
									// Handle multiple conditions
									$conditions = explode(',', $div);
					
									$allConditions = []; // Array to store all conditions
					
									foreach ($conditions as $condition) {
										$condition = trim($condition);
										$id = $this->Name_Check($tableName, $condition);
										$row_table = $this->select_id_by_fnames($rowTable, $tableName);
					
									
											$allConditions[] = $id; 
										
									}
					
									// Assign concatenated conditions to $table_array[$row_table]
									$table_array[$row_table] = implode(',', $allConditions);
									
								} else {
									// Handle other columns
									if($tableName === 'mis_sub_division'){
										$id = $this->select_id_by_name_sub_div($tableName, $div,$divisionId);
									}else{
										$id = $this->Name_Check($tableName, $div);
									}
									// echo $div;
									// exit;
									// $row_table = $this->select_id_by_fnames($rowTable, $tableName);
									if($cellValue === "ORIGIN BRAND"){
										$row_table = $this->select_id_by_fnames_new($rowTable, $tableName);
									 }else{
										 $row_table = $this->select_id_by_fnames($rowTable, $tableName);
									 }
					
									if ($id === false && $div!='') {
										$id = $div;
										$table_array[$row_table] = 'add';
										$POST[$row_table] = $id;
					
									
									} else {
									
											$table_array[$row_table] = $id;
										
									}
								}

							}

							// if($tableName=='mis_sub_division'){
							// 	echo $id;
							// 	exit;
							// }
							if ($cellValue == "ITEM NAME") {
								$divisionColumnIndex = array_search("ITEM NAME", $fixedHeaders);
								$item_name = $this->escape($activeSheet->getCellByColumnAndRow($divisionColumnIndex, $row)->getValue());
							}

							if ($cellValue == "NAME ARABIC") {
								$divisionColumnIndex = array_search("NAME ARABIC", $fixedHeaders);
								$name_arb = $this->escape($activeSheet->getCellByColumnAndRow($divisionColumnIndex, $row)->getValue());
							}

							if ($cellValue == "HSN") {
								$divisionColumnIndex = array_search("HSN", $fixedHeaders);
								$hsn = $this->escape($activeSheet->getCellByColumnAndRow($divisionColumnIndex, $row)->getValue());
							}
							
							if ($cellValue == "BARCODE") {
								$divisionColumnIndex = array_search("BARCODE", $fixedHeaders);
								$barcode = $this->escape($activeSheet->getCellByColumnAndRow($divisionColumnIndex, $row)->getValue());
							}
							
							if ($cellValue == "PART NUMBER") {
								$divisionColumnIndex = array_search("PART NUMBER", $fixedHeaders);
								$part_num = $this->escape($activeSheet->getCellByColumnAndRow($divisionColumnIndex, $row)->getValue());
							}
							
							if ($cellValue == "ADDITIONAL ITEM DESCRIPTION") {
								$divisionColumnIndex = array_search("ADDITIONAL ITEM DESCRIPTION", $fixedHeaders);
								$add_item = $this->escape($activeSheet->getCellByColumnAndRow($divisionColumnIndex, $row)->getValue());
							}


						}
					
					
							$sub_error = false;

								if (!$sub_error) {
											
												$pattern_id = 0;
												$own_brand = null;
												$pattern_array = [];


												$division_id = $table_array[1];
												$sub_division = isset($table_array[2]) ? $table_array[2] : null;
												$brand_id = ((isset($table_array[3]) && $table_array[3] != "add") ? $table_array[3] : null);

												if($brand_id)  $own_brand = $this->select_field_by_id("api_brand", "own_brand", $brand_id); 
												if ($subDivisionId>0) {
													$pattern_id = $this->select_pattern_id($division_id, $subDivisionId,NULL);
													$pattern_array = $this->select_table_pattern_list($division_id, $subDivisionId, NULL,NULL);
												}


												$data = [];
												$data['pattern_id'] = ($pattern_id?$pattern_id:0);
												$data['division_id'] = $division_id;
												$data['branch_id'] = $_SESSION['branch_id'];
												$data['sub_division_id'] = $subDivisionId;
												$data['brand_id'] = $brand_id;
												$data['own_brand'] =$own_brand;
												$data['name_arabic'] =$name_arb ?? null;
												$data['hsn'] = $hsn ?? null;
												$data['barcode'] =$barcode  ?? null;
												$data['partno'] = $part_num ?? null;
												$data['remarks'] = $add_item ?? null;
												$data['offer'] ="0";
												$data['created_by'] = $_SESSION['user_id'];
												$data['status'] = 0;

											$request_id=$this->insert_data("fcode_request", $data);

											$unique_id = "FCDE-".str_pad($request_id,7,"0", STR_PAD_LEFT);
										
											$this->update_data("fcode_request", ['unique_id'=>$unique_id], $request_id);
											
											

											if(count($pattern_array)>0) {
												$pattern_arr = array();
												foreach ($pattern_array as $val) {
													$data =  array();
													$data['request_id'] = $request_id;
													$data['f_table_id'] = $val=='0' ? 0 : (int) $val;
													$data['item_name']  = null;
													if ($val == 2) $data['item_name'] =  $item_name ?? null; 
													if (isset($POST[$val]) && isset($table_array[$val]) && $table_array[$val] == 'add' && $val!=24) {
														$data['f_table_val'] = $POST[$val];
														$data['is_manual'] = 1;
													} else {
														if ($val == 24  && isset($table_array[$val]) && count($table_array[$val])>0) {
														
															$data['f_table_val'] = $val=='0' ? $table_array[0] : ($table_array[$val]??0);
														} else {
															// if($div!=''){
																$data['f_table_val'] = $val=='0' ? $table_array[0] : (int) ($table_array[$val]??0);
															// }else{
															// 	$data['f_table_val'] = 0;
															// }
														}
														$data['is_manual'] = 0;
													}
													$pattern_arr[$data['f_table_id']] = $data['f_table_val'];
													$this->insert_data("fcode_request_table", $data);
												}
											
											}
											
											$this->update_data("fcode_request", ['requested_pattern'=> serialize($pattern_arr) ], $request_id);
											$success_count++;

											
								} 
							
					}
			    }

			}
			if ($success_count > 0) { ?>
				<div class="alert alert-success ">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<? if(isset($success_count)){ ?>
					<strong>
					<? echo $success_count." Records Added";  ?></strong>
					<? } ?>
				</div>
		<? }

			$result = ob_get_contents();
		    ob_end_clean();

		return trim($result);

		}
		

	}
	// -----------------------
    function insert_multi_data($table, $bulk_data)
	{
		$sql = "";
		foreach($bulk_data as $k => $data){
			$query="INSERT INTO ".$table." (";
			foreach($data as $key => $value)
			{
				$query .= $key.","; 
			}
			$query = substr($query, 0, -1);
			$query .= ") VALUES (";
			foreach($data as $key => $value)
			{
				$query .=  "'".$this->escape($value)."',";
			}
			$query = substr($query, 0, -1);
			$query .= ');';
			
			$sql.=$query;
            
            $inserted = $this->con->query($sql);
            $sql = "";
            if(!$inserted){
                return false;
                exit;
            }
		}
        

        return true;
		

	}


	function insert_data($table_name, $form_data)
	{
		// retrieve the keys of the array (column titles)
		$fields = array_keys($form_data);
		$form_data = array_map(array($this->con, 'real_escape_string'), $form_data);
		$insert_text = " (`".implode('`,`', $fields)."`) VALUES('".implode("','", $form_data)."')";
		// die($insert_text);
		// changing empty to null
	    $insert_text = str_replace("''","null",$insert_text);
		// die($insert_text);
		//var_dump($insert_text); exit;
		// build the query
	 	$sql = "INSERT INTO ".$table_name.$insert_text;
		// run and return the query result resource
		//die($sql);
		mysqli_query($this->con,$sql) or $this->Error();
		return mysqli_insert_id($this->con);
	}
	
	// again where clause is left optional
	function update_data_with_where($table_name, $form_data, $whereSQL=" where 1!=1")
	{
		// check for optional where clause
		
		
			
		if($whereSQL!=""){
		// start the actual SQL statement
		$sql = "UPDATE ".$table_name." SET ";
	
		// loop and build the column /
		$sets = array();
		foreach($form_data as $column => $value){
			if($value){
				$sets[] = "`".$column."` = '".$this->escape($value)."'";
		   }else{
			   $sets[] = "`".$column."` = NULL";
		   }
		}
		
		$sql .= implode(', ', $sets);
	
		// append the where statement
		 $sql .= $whereSQL;
	
		// run and return the query result
		return mysqli_query($this->con,$sql) or $this->Error();
		
		}
	}

	function select_all_table($table, $table_id,$search=" where 1=1 ")
	{	
		//$sql = "SELECT c.*, GROUP_CONCAT(r.id) AS code_id FROM ".$table." AS c INNER JOIN fcode_table_code AS r ON c.id=r.f_table_val AND r.f_table_id =".$table_id." GROUP BY c.id".$search;
		// echo $sql;
		$sql = "SELECT ".$table." .*, (SELECT GROUP_CONCAT(id SEPARATOR ',') FROM fcode_table_code WHERE f_table_val = ".$table.".id and f_table_id = ".$table_id.") as code_id, (SELECT GROUP_CONCAT(id SEPARATOR ',') FROM fcode_table_alternative_name WHERE f_table_val = ".$table.".id and f_table_id = ".$table_id.") as alt_name_id FROM ".$table." ". $search;
		$result = $this->con->query($sql) or $this->Error();
		return $result;
		
	}

	function get_fcode_pattern_code ($table,$table_id,$parent_id,$table_type) {
		
		$sql = "SELECT P.*,".$table.".name FROM ".$table_type." AS P LEFT JOIN ".$table." ON ".$table.".id = P.f_table_val  WHERE P.parent_id ='".$parent_id."' AND  P.f_table_id ='".$table_id."'";
	
		$result = $this->con->query($sql) or $this->Error();
		//echo mysqli_num_rows($result);
		return $result;
	}
	
	function update_data($table_name, $form_data, $id='0')
	{

		if(!empty($id)){
			$whereSQL = " WHERE id=".$id;

			$sql = "UPDATE ".$table_name." SET ";

			$sets = array();
			foreach($form_data as $column => $value){
				if(!is_null($value)){
					$sets[] = "`".$column."` = '".$this->escape($value)."'";
				}else{
					$sets[] = "`".$column."` = NULL";
				}
			}
		
			$sql .= implode(', ', $sets);
	

		    $sql .= $whereSQL;
			
			return mysqli_query($this->con,$sql);
		}
	}
		
	//CONVER TO thousandsCurrencyFormat/	
	function thousandsCurrencyFormat($cash) 
	{
	 // strip any commas 
		$cash = (0 + STR_REPLACE(',', '', $cash));
	 
		// make sure it's a number...
		if(!IS_NUMERIC($cash)){ RETURN FALSE;}
	 
		// filter and format it 
		if($cash>1000000000000){ 
			RETURN ROUND(($cash/1000000000000),2).' T';
		}elseif($cash>1000000000){ 
			RETURN ROUND(($cash/1000000000),2).' B';
		}elseif($cash>1000000){ 
			RETURN ROUND(($cash/1000000),2).' M';
		}elseif($cash>1000){ 
			RETURN ROUND(($cash/1000),1).' K';
		}
	 
		RETURN NUMBER_FORMAT($cash);
	}
	
	function amountFormat($cash, $dec = 2) 
	{
	
		return number_format($cash,$dec);
	}
	
	function re_arrange($arr, $field) 
	{
			$newarr = array(); 

			foreach($arr as $val) {
				$newarr[$val[$field]][] = $val;
			}

			return $newarr;
    }


	public function  super_unique($array)
    {
      $result = array_map("unserialize", array_unique(array_map("serialize", $array)));
    
      foreach ($result as $key => $value)
      {
        if ( is_array($value) )
        {
          $result[$key] = $this->super_unique($value);
        }
      }
    
      return $result;
    }
	
	
	public function __destruct(){
     
        mysqli_close($this->con);
    }

	function existing_brand_pattern($division_id, $sub_division_id, $brand_id){

		$sql = "SELECT id from fcode_pattern where division_id = $division_id";

		if($sub_division_id)
			$sql.= " AND sub_division_id = $sub_division_id ";
		else
			$sql.= " AND sub_division_id is null";

		if($brand_id)
			$sql.= " AND brand_id = $brand_id ";
		else
			$sql.= " AND brand_id is null";

		$result = $this->con->query($sql);
		$row = $result->fetch_assoc();

		if($row)
			return $row['id'];
		else
			return NULL;

	}

	// function generateBarcode ($division_id, $sub_division_id) {
	// 	$sql = "SELECT new_barcode from fcode_request where division_id = $division_id AND sub_division_id=$sub_division_id AND new_barcode IS NOT NULL ORDER BY id DESC";    
	// 	$result = $this->con->query($sql);
	// 	$row = $result->fetch_assoc();
	// 	if ($row) {
	// 		$part_number = $row['new_barcode']; 
	// 		$code = str_pad((int)$part_number+1, 6, '0', STR_PAD_LEFT);
	// 	} else {
	// 		$code = str_pad(1,6, '0', STR_PAD_LEFT);
	// 	}
    //     return $code;
	// }

	function generateBarcode($division_id, $sub_division_id) {
		$sql = "SELECT MAX(new_barcode) AS max_barcode FROM fcode_request WHERE division_id = $division_id AND sub_division_id = $sub_division_id AND new_barcode IS NOT NULL";
		
		$result = $this->con->query($sql);
		$row = $result->fetch_assoc();
	
		if ($row && $row['max_barcode'] !== null) {
			$code = str_pad((int)$row['max_barcode'] + 1, 6, '0', STR_PAD_LEFT);
		} else {
			$code = str_pad(1, 6, '0', STR_PAD_LEFT);
		}
	
		return $code;
	}


	// function generateBarcode($division_id, $sub_division_id) {
	// 	do {
	// 		$sql = "SELECT new_barcode FROM fcode_request WHERE division_id = $division_id AND sub_division_id = $sub_division_id AND new_barcode IS NOT NULL ORDER BY id DESC LIMIT 1";    
	// 		$result = $this->con->query($sql);
	// 		$row = $result->fetch_assoc();
	
	// 		if ($row) {
	// 			$part_number = $row['new_barcode']; 
	// 			$code = str_pad((int)$part_number + 1, 6, '0', STR_PAD_LEFT);
	// 		} else {
	// 			$code = str_pad(1, 6, '0', STR_PAD_LEFT);
	// 		}
	
	// 		// Check if the generated code already exists
	// 		$checkSql = "SELECT COUNT(*) as count FROM fcode_request WHERE division_id = $division_id AND sub_division_id = $sub_division_id AND new_barcode = '$code'";
	// 		$checkResult = $this->con->query($checkSql);
	// 		$checkRow = $checkResult->fetch_assoc();
	// 		$codeExists = $checkRow['count'] > 0;
	
	// 	} while ($codeExists);
	
	// 	return $code;
	// }


	function duplicate_check_where($table, $where){
		$result = $this->con->query("SELECT count(*) as total from $table where $where");
		$row = mysqli_fetch_assoc($result);
		if($row['total']==0)
			return false;
		else
			return true;
	}

	function cloud_users($WHERE = "WHERE 1=1") {
		$con1 = mysqli_connect($this->host, $this->user, $this->pswd);
		mysqli_select_db($con1, 'cloud');
		$result = $con1->query("select id, username from user $WHERE");
		return $result;
	}

	public function cloud_users_isexists($cloud_id,$id=NULL){
		$where = "";
		if ($id>0) {
			$where = "id!=$id";
		}
		$result =  $this->con->query("select count(*) as total from fcode_user where cloud_id = $cloud_id $where");
        $row = mysqli_fetch_assoc($result);
		if($row['total']==0)
		   return false;
        else
            return true;        

    }

	function getAllUsers() {
		$userid = [];
		$type = 1;
		if ($_SESSION['user_type'] == 1) $type = 2;
		$result =  $this->con->query("select id from fcode_user where user_type = ".$type);
		while ($row = mysqli_fetch_assoc($result)) {
			$userid[] =$row['id'];
		}
		return (count($userid)>0?implode("','",$userid):0);
	}

	public function getUnreadMessageCount($request_id = NULL) { 
		$sqlres = $sqlbranch = "";
		if ($request_id>0) $sqlres = " AND request_id=$request_id";
		if($_SESSION['user_type'] == 2) $sqlbranch = 'AND R.branch_id='.$_SESSION['branch_id'];
        $user_ids = $this->getAllUsers();
		$sqlQuery = $this->con->query("SELECT M.*,R.unique_id FROM `fcode_messages` AS M LEFT JOIN fcode_request AS R ON R.id = M.request_id WHERE M.user_id IN('$user_ids') AND M.`readed_by` IS NULL $sqlbranch  $sqlres ORDER BY M.id DESC ");
		$numRows =  mysqli_num_rows($sqlQuery);
		$output = ''; 
		if($numRows > 0){
			$output = $numRows;
		}
		return $output;
	}



	function time_elapsed_string($datetime, $full = false) {
		$now = new DateTime;
		$ago = new DateTime($datetime);
	
		$diff = $now->diff($ago);
		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;
	
		$string = [
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		];
	
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}
		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}

	function getNotification ($limit = NULL,$id = NULL) {
		$user_ids = $this->getAllUsers();
		$sqlbranch = $limitq = $idsql='';
		if($_SESSION['user_type'] == 2) $sqlbranch = 'AND R.branch_id='.$_SESSION['branch_id'];
		if($limit) $limitq = 'limit 0,'.$limit;
		if($id) $idsql = "AND M.id < '".$id."'";
	
		return $this->con->query("SELECT M.*,R.unique_id FROM `fcode_messages` AS M LEFT JOIN fcode_request AS R ON R.id = M.request_id WHERE M.user_id IN('$user_ids') AND M.`readed_by` IS NULL $sqlbranch  $idsql ORDER BY M.id DESC ".$limitq);
	}
		
	function RemoveSpecialChar($str){
		$res = str_replace( array( '\'', '"',',' , ';', '<', '>' ), ' ', $str);
		return $res;
	}
	
	
	

}

