<?php
include "db/connect.php";
$obj = new database();

$page = "settings";

?>

<!doctype html>
<html lang="en">
<head>

<? include "common/js_n_cs.php";?>



</head>
<body>
<?php include "common/header.php";?>
<div class="container-fluid body_bg" >
  <div class="d-flex flex-row">

    
    <?php include "common/nav.php";?>
 

    <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"  data-simplebar>
      <div class="col-sm-12 col-xs-12 inner-pad" >

        <? include "common/title_bar.php";?>
        <? include "common/settings_sub_nav.php";?>


        <div class="row">
            <div class="col">
                <table class="table table-bordered table-dark table-hover">
                    <tbody>
                        <tr>
                            <tr class="bg-dark">
                                <th width="40">#</th>
                                <th width="250">NAME</th>
                                <th>PATTERN</th>
                                <th width="50"></th>
                            </tr>
                        </tr>
                        <? 
                        $i=0;
                        $result = $obj->select_all("mis_division");
                        while($row = $result->fetch_assoc()){  ?> 
                            <tr> 
                                <td><?=++$i?></td>
                                <td><?=$row['name']?></td>
                                <td>
                                     <?=$obj->select_table_pattern($row['id'])?>
                                </td>
                                <td class="text-info cp edit_div" data-id=<?=$row['id']?>> EDIT </td>
                            </tr>
                        <? }?>
                    </tbody>
                </table>
            </div>
        </div>



        <!-- Modal -->
        <div class="modal fade" id="myModal" >
            
        </div>


      </div>
      <? include "common/up_icon.php";?>
    </div>
  </div>
</div>

<script>

    $(document).on("click", ".edit_div", function(e) {

        id = $(this).data("id");

        $.ajax({
            url: "ajax/div_config",
            data:{"id":id},
            success: function(msg){
                $("#myModal").html(msg).modal("show");
                $(".chosen-select").chosen();
            }
        });

    });

    $(document).on("click", ".add_pattern", function(e) {

        f_table = $(this).closest(".row").find(".f_table").val();
        id = $(this).data("id");

        $.ajax({
            url: "ajax/update_fcode_table_pattern",
            data:{"id":id, "f_table":f_table},
            dataType:"json",
            type: "POST",
            success: function(msg){
                if(msg.error)
                    alert(msg.error);
                else if(msg.success){
                    alert(msg.success);
                }
            }
        });

    });

</script>


</body>
</html>
