<?php
include "db/connect.php";
$obj = new database();

if($obj->isAdmin() == false) {
    header("location:" . $obj->cloud_url);
    exit;
}

$page = "settings";

if (isset($_POST['add'])) {

    $data = array();
    $obj->con->autocommit(false);
    $data['name'] = trim($_POST['name']);
    $data['status'] = $_POST['status'];
    $udata['f_table_id'] =  $udata_alt['f_table_id'] = $_POST['f_table_id'];
    $error = [];
    if ($obj->check_if_already_exist_master_name($_POST['table'],$data['name'],$_POST['id'])>0) {
        $error[] = $data['name']." already exists";
    }  else {
        if(isset($_POST['id'])) {
            $obj->update_data($_POST['table'], $data, $_POST['id']);
            if($_POST['ids_'.$udata['f_table_id']]) {
                $ids = trim($_POST['ids_'.$udata['f_table_id']],",");
                $obj->delete_where("fcode_table_code", "WHERE id IN (".$ids.")");
            }
            if($_POST['alt_ids_'.$udata['f_table_id']]) {
                $alt_ids = trim($_POST['alt_ids_'.$udata['f_table_id']],",");
                $obj->delete_where("fcode_table_alternative_name", "WHERE id IN (".$alt_ids.")");
            }
            $udata['f_table_val'] = $udata_alt['f_table_val'] = $_POST['id'];
        } else {
            $udata['f_table_val'] = $udata_alt['f_table_val'] = $obj->insert_data($_POST['table'], $data);	
        }
        $flag = 0;
        for ($i=0;$i<count($_POST['focus_code']);$i++) {
            if ($_POST['focus_code'][$i]) {
                $udata['focus_code'] = $_POST['focus_code'][$i];
                $udata['is_primary'] = ($_POST['is_primary'][$i]?(int)$_POST['is_primary'][$i]:0);
                if($udata['is_primary'] == 1 && $flag == 0 ) $flag = 1;
                if($_POST['focus_code_id'][$i]>0) {
                    $focus_code_id =  $_POST['focus_code_id'][$i];
                    $obj->update_data("fcode_table_code", $udata, $_POST['focus_code_id'][$i]);
                } else {
                    $focus_code_id =  $obj->insert_data("fcode_table_code", $udata);	
                }
                if ($i==0) $fid = $focus_code_id;
            } 
            
        }
        if ($flag == 0) {
            $udata_code['is_primary'] = 1;
            $obj->update_data("fcode_table_code", $udata_code, $fid);
        }
    
        for ($i=0;$i<count($_POST['alternative_name']);$i++) {
            $udata_alt['name'] = $_POST['alternative_name'][$i];
            if ($udata_alt['name']) {
                if ($obj->check_if_already_exist_name($_POST['table'],$udata_alt['name'],NULL)>0) {
                $error[] = $udata_alt['name']." already exists";
                } else {
                    if($udata_alt['name']) {
                        if ($_POST['alternative_name_id'][$i]>0) {
                            $obj->update_data("fcode_table_alternative_name", $udata_alt, $_POST['alternative_name_id'][$i]);
                        } else {
                            $obj->insert_data("fcode_table_alternative_name", $udata_alt);	
                        }
                    } 
                }
            }
        
        }
    }
    if (count($error) == 0 ) {
        $obj->con->commit();
    } else {
        setcookie("error", json_encode($error), time() + 1);  
    }
    header("location:".$obj->thispage());
    exit;

}




?>

<!doctype html>
<html lang="en">
<head>

<? include "common/js_n_cs.php";?>


</head>
<body>
<?php include "common/header.php";?>
<div class="container-fluid body_bg" >
  <div class="d-flex flex-row">

    <?php include "common/nav.php";?>

    <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"  data-simplebar>
      <div class="col-sm-12 col-xs-12 inner-pad" >

        <? include "common/title_bar.php";?>

        <? include "common/settings_sub_nav.php";?>




        <div class="row">
           <div class="col-sm-12">
            <?
                    if(isset($_COOKIE['error'])) {
                        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
                        foreach (json_decode($_COOKIE['error']) as $errors) {
                    
                            echo $errors ."<br/>";
                    }
                    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>';
                    } 
                ?>
            </div>

            <? 
            $table_array = array(
                                "api_pin_type" => "PIN TYPE",
                                "api_capacity" => "CAPACITY",
                                "api_color" => "COLOR",
                                
                                ); 
                                
            foreach($table_array as $table => $title )   {    
                $table_id = $obj->select_id_by_name("fcode_table",$title);
                $result_code = $result_name = "";
                if(isset($_GET['edit']) && $_GET['table'] == $table){
                    $ed = $obj->select_all_by_id($_GET['table'], $_GET['edit']);
                    $result_code = $obj->select_all_by("fcode_table_code","f_table_id='".$table_id."' AND f_table_val='".$_GET['edit']."'");
                    $result_name  = $obj->select_all_by("fcode_table_alternative_name","f_table_id='".$table_id."' AND f_table_val='".$_GET['edit']."'");
                    if (mysqli_num_rows($result_name) == 0) {
                        $result_name = "";
                    }
                } else if(isset($ed)) {
                    unset($ed);     
                }
                      
                    
                ?>
               
                <div class="col-sm-4 col text-white">
                        <h5><?=$title?></h5>
                        <hr>
                        <form action="" method="post">

                            <input type="hidden" name="table" value="<?=$table?>">

                            <div class="form-group">
                                <label>NAME</label>
                                <input type="text" class="form-control form-control-sm" name="name" <? if(isset($ed)){ ?> value="<?=$ed['name']?>" <? } ?>  required>
                            </div>
                            <?php
                            if($result_name){
                                $i = 1;
                                while($row_name = $result_name->fetch_assoc()){ 
                                    ?>
                                    <div class="form-group <?php echo ($i==1?"after-alt-".$table_id."-add-more":"")?> ">
                                        <div class="add_name_txt">
                                            <label>ALTERNATIVE NAME</label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control form-control-sm" name="alternative_name[]"  <? if(isset($ed)){ ?> value="<?=$row_name['name']?>" <? } ?> >
                                                <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  <? if(isset($ed)){ ?> value="<?=$row_name['id']?>" <? } ?>>
                                                <?php 
                                                if($i==1) {
                                                ?>
                                                <div class="input-group-append"  onclick="addAlternativeMore('after-alt-<?=$table_id?>-add-more');">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                                </div>
                                                <?php
                                                } else {
                                                ?>
                                                <div class="input-group-append remove_alt" data-code="<?=$row_name['id']?>" data-ids="alt_ids_<?=$table_id?>">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>  
                                    </div>
                                    <?php
                                $i++;
                            }
                            
                        } else {
                        ?>    
                        <div class="form-group after-alt-<?=$table_id?>-add-more">
                            <label>ALTERNATIVE NAME</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control form-control-sm" name="alternative_name[]">
                                <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  value="0">
                                <div class="input-group-append"  onclick="addAlternativeMore('after-alt-<?=$table_id?>-add-more');">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                </div>
                            </div>
                        </div> 
                        <?php
                        }
                        if($result_code){
                            $i = 1;
                            while($row_code = $result_code->fetch_assoc()){ 
                                ?>
                                <div class="row add_txt <?php echo ($i==1?"after-".$table_id."-add-more":"")?> ">
                                    <div class="col-sm-8"> 
                                        <div class="form-group">
                                            <label>F CODE</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control form-control-sm" name="focus_code[]"  <? if(isset($ed)){ ?> value="<?=$row_code['focus_code']?>" <? } ?> required>
                                                <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  <? if(isset($ed)){ ?> value="<?=$row_code['id']?>" <? } ?>>
                                                <?php 
                                                if($i==1) {
                                                ?>
                                                <div class="input-group-append"   onclick="addMore('after-<?=$table_id?>-add-more');">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                                </div>
                                                <?php
                                                } else {
                                                ?>
                                                <div class="input-group-append remove" data-code="<?=$row_code['id']?>" data-ids="ids_<?=$table_id?>">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label> IS PRIMARY </label>
                                            <div class="input-group">
                                                <input type="radio" class="is_primary" name="is_primary_flag"  value="1" <? if(isset($ed) && $row_code['is_primary'] == 1){ echo "checked";} ?>>
                                                <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  <? if(isset($ed) && $row_code['is_primary'] == 1){ ?> value="1" <? } ?>  >
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <?php
                            $i++;
                            }
                        } else {
                        ?>
                        <div class="row after-<?=$table_id?>-add-more">
                            <div class="col-sm-8"> 
                                <div class="form-group">
                                    <label>F CODE</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control form-control-sm" name="focus_code[]"  required="required">
                                        <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  value="0">
                                        <div class="input-group-append"  onclick="addMore('after-<?=$table_id?>-add-more');">
                                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                        </div>
                                    </div>
                                </div> 
                            </div> 
                            <div class="col-sm-4">
                            <div class="form-group">
                                    <label> IS PRIMARY </label>
                                    <div class="input-group">
                                        <input type="radio" class="is_primary" name="is_primary_flag"  value="1">
                                        <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  value="0">
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <?php } ?>
                            <div class="copy_alt hide">  
                                <div class="add_name_txt">
                                    <label>ALTERNATIVE NAME</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control form-control-sm" name="alternative_name[]">
                                        <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  value="0">
                                        <div class="input-group-append remove_alt">
                                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="copy hide">  
                                <div class="row add_txt">
                                    <div class="col-sm-8"> 
                                        <div class="form-group">
                                            <label>F CODE</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control form-control-sm" name="focus_code[]">
                                                <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  value="0">
                                                <div class="input-group-append remove">
                                                    <span class="input-group-text" id="basic-addof_table_idn2"><i class="fa fa-minus"></i></span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label> IS PRIMARY </label>
                                            <div class="input-group">
                                                <input type="radio" class="is_primary" name="is_primary_flag"  value="1">
                                                <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  id="is_primary" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>  
                            <div class="form-group">
                            <label>STATUS</label> <br>
                            <select class="form-control form-control-sm" name="status" id="status" required="required">
                                <option value="">Select Status</option>
                                <option value="1" <? if(isset($ed) && $ed['status']==1){ ?> selected <? } ?>>Active</option>
                                <option value="0" <? if(isset($ed) && $ed['status']==0){ ?> selected <? } ?>>Inactive</option>
                            </select>
                            </div>      
                            <div class="form-group">
                            <input type="hidden" name="ids_<?=$table_id?>" id="ids_<?=$table_id?>">
                            <input type="hidden" name="alt_ids_<?=$table_id?>" id="alt_ids_<?=$table_id?>">
                                <input type="hidden"  name="f_table_id"  value="<?=$table_id?>">
                                <? if(isset($ed)){ ?>
                                    <input type="hidden" name="id" value="<?=$ed['id']?>">
                                    <button name="add" type="submit" class="btn btn-info">UPDATE</button>
                                <? }else{ ?>
                                    <button name="add" type="submit" class="btn btn-info">ADD</button>
                                <? } ?>
                                    <a href="<?=$obj->thispage()?>" class="btn btn-secondary">CANCEL</a>
                            </div>

                        </form>

                        <table class="table table-dark table-bordered" data-table="<?=$table?>" >
                            <thead>
                                <th>#</th>
                                <th>NAME</th>
                                <th>ALT NAME</th>
                                <th>CODE</th>
                                <th>STATUS</th>
                                <th>ACTION</th>
                            </thead>
                            <tbody>
                                <? $i=0;
                               
                                    //$result = $obj->select_all($table);
                                    $result = $obj->select_all_table($table, $table_id, NULL);
                                    while($row = $result->fetch_assoc()){ 
                                        $focus_code_arr = ($row['code_id']?explode(",",$row['code_id']):[]);
                                        $alt_name_arr = ($row['alt_name_id']?explode(",",$row['alt_name_id']):[]);
                                        ?>
                                    <tr data-id="<?=$row['id']?>">
                                        <td><?=++$i?></td>
                                        <td><?=$row['name']?></td>
                                        <td> 
                                        <? 
                                            if (isset($alt_name_arr)) {
                                            foreach($alt_name_arr as $name_id) { 
                                                $alt_name_result = $obj->select_all_by_field_id_process('fcode_table_alternative_name','id',$name_id);
                                                echo $alt_name_result['name']."<br/>";
                                            }  
                                            }
                                                
                                        ?>
                                        </td>
                                        <td>
                                        <?php
                                        foreach($focus_code_arr as $code_id) {     
                                            $fcode_result = $obj->select_all_by_field_id_process('fcode_table_code','id',$code_id);
                                            ?>
                                            <div class="codecls_<?=$fcode_result['id']?>">
                                            <input type="text" class="full_box code_input ev hide txt_cls_<?=$fcode_result['id']?>" id="txt_val_<?=$fcode_result['id']?>" value="<?=$fcode_result['focus_code']?>"> 
                                            <span class="cv f_code_<?=$fcode_result['id']?> txt_edit_cls_<?=$fcode_result['id']?>">
                                              <?=$fcode_result['focus_code']?>
                                              <? if($fcode_result['is_primary'] && count($focus_code_arr)>1) echo '<span class="badge badge-primary">Primary</span>'; ?>
                                            </span>
                                            <i class="fas fa-pen fa-xs cp float-right ev txt_edit_cls_<?=$fcode_result['id']?>" data-toggle='[".cv",".ev"]'  onclick="editable(<?=$fcode_result['id']?>)"></i>
                                            <span class="cv float-right  hide txt_cls_<?=$fcode_result['id']?>" data-toggle='[".cv",".ev"]'>
                                                <button class="btn btn-xs btn-secondary  mr-2 " onclick="noteditable(<?=$fcode_result['id']?>)">CANCEL</button>
                                                <button class="btn btn-xs btn-primary" onclick="updateTextbox(<?=$fcode_result['id']?>)">UPDATE</button>
                                            </span> 
                                            </div>
                                        <?php
                                            }
                                        ?>
                                    </td>
                                       <td> <span class="badge <?=($row['status']==1?"bg-success":"bg-danger")?> text-white">
                                            <?=($row['status']==1?"ACTIVE":"INACTIVE")?>
                                           </span></td>
                                        <td>
                                            <a href="?edit=<?=$row['id']?>&table=<?=$table?>">EDIT</a>
                                        </td>
                                    </tr>
                                <? } ?>
                            </tbody>
                        </table>

                </div>

            <? } ?>

        </div>





      </div>
      <? include "common/up_icon.php";?>
    </div>
  </div>
</div>
</body>
</html>
