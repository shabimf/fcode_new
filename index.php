<?php
include "db/connect.php";
$obj = new database();
$page = "new request";

if(isset($_SESSION['fcode_new_request']) && $_SESSION['fcode_new_request'] ==0) {
  header("location:" . $obj->cloud_url);
	exit;
}
?>
<!doctype html>
<html lang="en">
<head>
  <? include "common/js_n_cs.php"; ?>
  <style>
    .hide { display: none !important;}
    .error {color:red}
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
      background-color:#840a22 !important;
      border:none !important;
      
    } .nav-tabs .nav-item {
      font-size:16px;
      color:#fff !important;
    }
  </style>
</head>
<body>
  <?php include "common/header.php"; ?>
  <div class="container-fluid body_bg ">
    <div class="d-flex flex-row">
      <?php include "common/nav.php"; ?>
      <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?= $_SESSION['container'] ?>" id="content_box" data-simplebar>
        <div class="col-sm-12 col-xs-12 inner-pad ">
          <!-- <? include "common/title_bar.php"; ?> -->
          <div class="alert alert-success request-msg-cls hide" role="alert">
        </div>
          <div class="p-2 bg-black rounded shadow mb-5">
            <div class="row">
              <div class="col-md-12">
                  <nav class="tabcls">
                      <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                          <a class="nav-item nav-link offer active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true" data-id="0">Normal Request</a>
                          <a class="nav-item nav-link offer" id="nav-profile-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-profile" aria-selected="false" data-id="1">Bundle Offers</a>
                          
                      </div>
                  </nav>
                  <div class="tab-content mt-2" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                      <form action="" method="post" name="requestForm" id="requestForm"  enctype="multipart/form-data">
                        <div class="row mt-3" id="target">
                          <div class="col-sm-6">
                            <div class="contactselect">
                            <label class="text-danger">DIVISION<span class="error">*</span></label>
                            <select class="chosen-select-deselect division_select  f_input" disabled name="f_table[1]" data-placeholder="SELECT DIVISION" required >
                              <option value=""></option>
                              <? $result = $obj->select_all("mis_division");
                              while ($row = $result->fetch_assoc()) { ?>
                                <option value="<?= $row['id'] ?>" <?if(isset($_SESSION['division_id'])) {
                                  if($_SESSION['division_id']==$row['id']) echo "selected";
                                }?>><?= $row['name'] ?></option>
                              <? } ?>
                            </select>
                            </div>
                            <label>SUB DIVISION<span class="error">*</span></label>
                            <select class="chosen-select-deselect subdivision_select f_input" name="f_table[2]" data-placeholder="SELECT SUB DIVISION" required onchange="showModelInput(this.value,2)" id="subdivision_select">
                            <option value=""></option>
                            <option value="add">Add New</option>
                              <? $result = $obj->select_all("mis_sub_division"," where is_fcode=1");
                              while ($row = $result->fetch_assoc()) { ?>
                                <option value="<?= $row['id'] ?>" <?if(isset($_SESSION['sub_division_id'])) {
                                  if($_SESSION['sub_division_id']==$row['id']) echo "selected";
                                }?>><?= $row['name'] ?></option>
                              <? } ?>  
                          </select>
                            <div class="txtcls_2 hide"><label class="text-danger">SUBDIVISION NAME<span class="error">*</span></label><input type="text" class="f_input add-form-control" name="field_2" id="field_2" placeholder="ENTER SUBDIVISION NAME" required onkeyup="checkName(this.value,2); return false;" > <span id="confirmname_2" class="error_name"></span>
                            <div id="msg" class="form-group"></div></div>
                            <label class="show_item hide">ITEM NAME</label>
                            <input type="text" name="item_name" id="item_name" class="f_input show_item hide" value=""  onchange="getCode()"
                            >
                            
                            <label class="brand_title">BRAND<span class="brand-err error"></span></label>
                            <select class="chosen-select-deselect brand_select f_brands f_input" id="brand_id" name="f_table[3]" data-placeholder="SELECT BRAND" onchange="showModelInput(this.value,3),getCode()">
                              <option value=""></option>
                              <option value="add">Add New</option>
                              <? $result = $obj->select_all("api_brand", "where status = 1");
                              while ($row = $result->fetch_assoc()) { ?>
                                <option value="<?= $row['id'] ?>" <?if(isset($_SESSION['brand_id'])) {
                                    if($_SESSION['brand_id']==$row['id']) echo "selected";
                                }?>><?= $row['name'] ?></option>
                              <? } ?>
                            </select>
                            <div class="txtcls_3 hide">
                              <label class="text-danger">BRAND NAME</label>
                              <input type="text" class="f_input add-form-control" name="field_3" id="field_3" placeholder="ENTER BRAND NAME" onkeyup="getCode(),checkNameNew(this.value,3); return false;"  >
                            </div>
                            <div id="txtExists_name" class="form-group">
          
            </div>
                            <label class="model_title hide">MODEL<span class="model-err error"></span></label>
                            <select class="chosen-select-deselect  f_input hide" id="model_id" name="f_table[5]" data-placeholder="SELECT MODEL" onchange="showModelInput(this.value,5),getCode()">
                              <option value=""></option>
                              <option value="add">Add New</option>
                              <? $result = $obj->select_all("api_model", "where status = 1");
                              while ($row = $result->fetch_assoc()) { ?>
                                <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                              <? } ?>
                            </select>
                            <div class="txtcls_5 model_title_other hide">
                              <label class="text-danger">MODEL NAME</label>
                              <input type="text" class="f_input add-form-control" name="field_5" id="field_5" placeholder="ENTER MODEL NAME" onkeyup="getCode(),checkName(this.value,5); return false;">
                            </div>
                            <div class="f_tables">
                            </div>

                            <div class="post_result">

                            </div>

                          </div>

                          <div class="col">
                            <h4 class="code_head"></h4>
                            <h4 class="name_head"></h4>
                            <label class="field-input hide">ADDITIONAL ITEM DESCRIPTION</label>
                            <textarea class="f_input form-control form-control-sm mb-3 field-input hide" name="remarks" id="remarks" rows="3"></textarea>
                            <button type="submit"  class="btn btn-sm btn-primary mt-2 submit_request field-input hide float-right" id="send">SUBMIT</button>
                          </div>
                        </div>
                        <input type="hidden" name="offer" id="offer" value="0"/>
                        <input type="hidden" name="img_val" id="img_val" value="" />
                      </form>
                    </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
      <? include "common/up_icon.php"; ?>
    </div>
  </div>
  </div>
  
</body>

<?php include "common/choosen-init.php";?>
<script type="text/javascript" src="js/html2canvas.js"></script>
<script type="text/javascript" src="js/jquery.plugin.html2canvas.js"></script>
<script>


function checkNameNew(val, table_id) {
    var message = $('#confirmname_' + table_id);
    // document.getElementById("send").disabled = true;
    $.ajax({
        url: "ajax/check_name.php",
        type: "POST",
        data: {
            val: val,
            table_id: table_id
        },
        success: function(response) {
            if (response == 0) {
                $("#txtExists_name").show();
                $("#txtExists_name").html("<p style='color: green;'>Valid Name</p>");
                // document.getElementById("send").disabled = false;
            } else if (response == 1) {
                $("#txtExists_name").show();
                $("#txtExists_name").html("<p style='color: red;'>Name Already Exists</p>");
                // document.getElementById("send").disabled = true;
            }
        }
    });
}
<?php 
if(isset($_SESSION['sub_division_id'])) {
?>
  showPattern();
<?php 
} 
?>



$("#model_id_chosen").hide();
$(".offer").click(function(e) {
    $("#offer").val($(this).attr('data-id'));
    $(".division_select").attr("disabled", true);
    $(".code_head").html('');
    $(".name_head").html('');
    $('.division_select,.subdivision_select,.brand_select').val('').trigger("chosen:updated");
    $('.subdivision_select option[value="add"]').remove();
    $(".show_item").addClass('hide');
    $(".txtcls_2").hide();
    $(".model_title").hide();
    $(".model_title_other").hide();
    $("#model_id_chosen").hide();
    $(".field-input").addClass("hide");
    showPattern();
});
$(document).on("change", ".division_select, .subdivision_select, .own_brand, .brand_select", function(e) {
  showPattern();
});

function showPattern () {
  division_id = $(".division_select").val();
  if($(".subdivision_select").val() == null){
  var sub_division_id = <?=((isset($_SESSION['sub_division_id'])&&$_SESSION['sub_division_id']!="")?$_SESSION['sub_division_id']:0)?>;
  } else {
  var sub_division_id = $(".subdivision_select").val();
  }
  brand_id = $(".brand_select").val();
  var offer = $("#offer").val();
  own_brand = $(".own_brand").prop("checked") == true ? 1 : 0;
  if(offer=="1"){
    $('.subdivision_select option[value="add"]').remove();
    $(".subdivision_select").trigger("chosen:updated");
  } 

  if(offer=="0"){
    
    if($("#subdivision_select option[value='add']").length==0){
      $('.subdivision_select').prepend('<option value="add">Add New</option>');
      $(".subdivision_select").trigger("chosen:updated");
    }
  }
  
  if (sub_division_id == "") {
    
    $(".f_tables").html("");
    return false;
  }
  $.ajax({
    url: "ajax/f_table_form",
    data: {
      "division_id": division_id,
      "sub_division_id": sub_division_id,
      "brand_id": brand_id,
      "own_brand": own_brand,
      "offer" : offer
    },
    success: function(msg) {
      
      $(".f_tables").html(msg);
      $(".f_table_selects").chosen({
        allow_single_deselect: true
      });
      $(".chosen-select").chosen();
      $(".f_brands").prop('required', false);
      $(".brand-err").html("");
      $('.code_head').html("");
      $('.name_head').html("");
      if(offer != 1) {
        $(".field-input").removeClass("hide");
      }
      
      if(typeof($("#pattern_ids").val())  === "undefined" && offer!=1) 
      {

        $("#newcode").prop('required', true);
        $("#description").prop('required', true);
        $(".model_title").removeClass('hide');
        $(".model_title").show();
        $(".model_title_other").show();
        $("#model_id_chosen").show();
        $("#model_id").trigger("chosen:updated");
      } else {
        $("#newcode").prop('required', false);
        $("#description").prop('required', false);
        $("#brand_id_chosen").show();
        $(".brand_title").show();
        $("#brand_id_chosen").show();
        $(".f_brands").trigger("chosen:updated");
        $(".model_title").hide();
        $(".model_title_other").hide();
        $("#model_id_chosen").hide();
        $("#model_id option").prop("selected", false);
        if ($('#brand_required').val()=="1") {
          $(".f_brands").prop('required', true);
          $(".brand-err").html("*");
        }
      }

    }
  });
  } 


</script>

</html>