<?php
require_once 'excelsupport/PHPExcel.php';
require_once 'excelsupport/PHPExcel/IOFactory.php';

include "db/connect.php";
$obj = new database();
if($obj->isAdmin() == false) {
    header("location:" . $obj->cloud_url);
    exit;
}

$page = "settings";

$exlel_message="";
if(isset($_POST['table'])){
   
    if($_FILES['excel']['error'] == 0){
       $exlel_message = $obj->process_excel($_POST['table'],$_FILES);
    }
}

if(isset($_POST['division_id']) && $_POST['division_id'] != ""){
   
    if($_FILES['excel']['error'] == 0){
       $exlel_message = $obj->process_excel("mis_sub_division",$_FILES, $_POST['division_id']);
    }
}

if(isset($_FILES['focus_excel'])){
    if($_FILES['focus_excel']['error'] == 0){
        $exlel_message = $obj->process_focus_excel($_FILES);
     }
}



?>

<!doctype html>
<html lang="en">

<head>

    <? include "common/js_n_cs.php";?>


</head>

<body>
    <?php include "common/header.php";?>
    <div class="container-fluid body_bg">
        <div class="d-flex flex-row">

            <?php include "common/nav.php";?>

            <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"
                data-simplebar>
                <div class="col-sm-12 col-xs-12 inner-pad">

                    <? include "common/title_bar.php";?>

                    <? include "common/settings_sub_nav.php";?>


                    <?=$exlel_message?>


                    <div class="row">

                        <div class="col-sm-4 col text-white">
                            <h5>EXCEL UPLOAD</h5>
                            <hr>
                            <form action="" method="post" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label>TABLE</label>
                                    <select class="form-control form-control-sm" name="table" required >
                                        <option value="">SELECT</option>
                                        <? $result = $obj->select_all("fcode_table"," where id!=2 && id!=4");
                                            while($row = $result->fetch_assoc()){ ?>
                                            <option value="<?=$row['f_table']?>"  ><?=$row['name']?></option>
                                        <? } ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>EXCEL</label>
                                    <input type="file" class="form-control  form-control-sm" name="excel" required />

                                    <a href="master_format.xlsx" class="text-warning float-right mt-2">DOWNLOAD FORMAT</a>
                                </div>

                                <div class="form-group">
                                    <button name="upload" type="submit" class="btn btn-info">ADD</button>
                                </div>

                            </form>
                        </div>

                        <div class="col-sm-4 col text-white">
                            <h5>SUB DIVISION UPLOAD</h5>
                            <hr>
                            <form action="" method="post" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label>DIVISION</label>
                                    <select class="form-control form-control-sm" name="division_id" required >
                                        <option value="">SELECT</option>
                                        <? $result = $obj->select_all("mis_division");
                                            while($row = $result->fetch_assoc()){ ?>
                                            <option value="<?=$row['id']?>"  ><?=$row['name']?></option>
                                        <? } ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>EXCEL</label>
                                    <input type="file" class="form-control  form-control-sm" name="excel" required />
                                    <a href="master_format.xlsx" class="text-warning float-right mt-2">DOWNLOAD FORMAT</a>
                                </div>

                                <div class="form-group">
                                    <button name="upload" type="submit" class="btn btn-info">ADD</button>
                                </div>

                            </form>
                        </div>

                        <div class="col-sm-4 col text-white">
                            <h5>FOCUS CODE UPLOAD</h5>
                            <hr>
                            <form action="" method="post" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label>EXCEL</label>
                                    <input type="file" class="form-control  form-control-sm" name="focus_excel" required />
                                    <a href="fcode_format.xlsx" class="text-warning float-right mt-2">DOWNLOAD FORMAT</a>
                                </div>

                                <div class="form-group">
                                    <button name="upload" type="submit" class="btn btn-info">ADD</button>
                                </div>

                            </form>
                        </div>

                    </div>
                    <? include "common/up_icon.php";?>
                </div>
            </div>
        </div>
</body>

</html>