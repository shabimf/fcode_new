<?php
include "db/connect.php";
$obj = new database();
if($obj->isAdmin() == false) {
    header("location:" . $obj->cloud_url);
    exit;
}
$page = "request_report";


?>

<!doctype html>
<html lang="en">
<head>
  <? include "common/js_n_cs.php";?>
  <? include("common/data_table.php"); ?>
</head>
<body>
  <?php include "common/header.php";?>
  <div class="container-fluid body_bg ">
    <div class="d-flex flex-row">
      <?php include "common/nav.php";?>
      <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"
        data-simplebar>
        <div class="col-sm-12 col-xs-12 inner-pad ">
          <? include "common/title_bar.php";?>
          <form action="" method="post" id="reportForm" name="reportForm">
 
            <div class="row">
                <div class="form-group col-md-3">
                    <label class="col-sm-12 pl-0 pr-0 text-white">FROM DATE</label>
                    <div class="col-sm-12 pl-0 pr-0">
                        <input type="date" name="form_date" id="form_date" class="f_input" value=""  />
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label class="col-sm-12 pl-0 pr-0 text-white">TO DATE</label>
                    <div class="col-sm-12 pl-0 pr-0">
                        <input type="date" name="to_date" id="to_date" class="f_input" value=""  />
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label class="col-sm-12 pl-0 pr-0 text-white">Ticket No</label>
                    <div class="col-sm-12 pl-0 pr-0">
                        <input type="text" name="ticket_no" id="ticket_no" class="f_input" value=""  />
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label class="col-sm-12 pl-0 pr-0 text-white">STATUS</label>
                    <div class="col-sm-12 pl-0 pr-0">
                        <select data-placeholder="Choose a Status..." class="chosen-select" tabindex="-1" id="status" name="status" >
                            <option value=""></option>
                            <option value="3">Pending</option>
                            <option value="1">Approved</option>
                            <option value="2">Rejected</option>
                        </select>
                    </div>
                </div>
               
            </div>
        
                        
            <div class="form-group mt-3">
                <button name="add" type="button" class="btn btn-info btn-cls" >Search</button>
                <a href="request_report" class="btn btn-secondary">Reset</a>
            
            </div> 
                        
        </form>
            <table class="table table-bordered text-white" id="requestList">
              <thead>
                <tr class="bg-grey">
                  <th>Requested Date</th>
                  <th>Requested Time</th>
                  <th>Ticket No</th>
                  <th>Branch</th>
                  <th>Status</th>
                  <th>Closed Date</th>
                  <th>Closed Time</th>
                </tr>
              </thead>

            </table>
        </div>
        <!-- <? include "common/up_icon.php";?> -->
      </div>
    </div>
  </div>

</body>
<?php include "common/choosen-init.php";?>
<script type="text/javascript" language="javascript" src="js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/jszip.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/buttons.html5.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var requestData = $('#requestList').DataTable({
            "lengthChange": false,
            "bProcessing": true,
            "bServerSide": true,
            "order":[],
            "ajax":{
                url:"<?=$obj->base_url?>ajax/list_request",
                dataType: 'json',
                type: 'POST',
                data: function (d) {
                    d.form_date = $("#form_date").val();
                    d.to_date = $("#to_date").val();
                    d.status = $("#status").val();
                    d.ticket_no=$.trim($("#ticket_no").val());
                }
                
            },
            dom: 'Bfrtip',
            buttons: [
                { extend: 'excelHtml5', className: 'btn-sm btn-success mb-2' },
                { extend: 'csvHtml5', className: 'btn-sm btn-success mb-2' }
               
                
            ],
            "pageLength": 10,
            searching: false
        });
        $('.btn-cls').click(function(){
            requestData.draw();
        } );
    });
    
</script>
</html>