<?php
include "db/connect.php";
$obj = new database();
 
// Filter the excel data 
function filterData(&$str){ 
    $str = preg_replace("/\t/", "\\t", $str); 
    $str = preg_replace("/\r?\n/", "\\n", $str); 
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"'; 
} 
 
// Excel file name for download 
$fileName = "configuration-data_" . date('Y-m-d') . ".xls"; 
 
// Column names 
$fields = array('ID', 'DIVISION', 'SUB DIVISION', 'BRAND', 'CODE', 'DESCRIPTION','HASPATTERN'); 
 
// Display column names as first row 
$excelData = implode("\t", array_values($fields)) . "\n"; 

$division_id = $_REQUEST['division_id'];
$sub_division_id = $_REQUEST['sub_division_id'];
$brand_id = $_REQUEST['brand_id'];
// Fetch records from database 
$result = $obj->select_all_pattern($division_id,$sub_division_id,$brand_id);
if($result){ 
    // Output each row of the data 
    $i = 1;
    while($row = $result->fetch_assoc()){
        $hasPattern="YES";
        if(strip_tags($obj->get_pattern($row['id']))=="" && strip_tags($obj->get_pattern_description($row['id']))=="") { 
            $hasPattern="NO";
        }
        $lineData = array($i, $row['divsion'], $row['sub_division'], $row['brand_name'],strip_tags($obj->get_pattern($row['id'])),strip_tags($obj->get_pattern_description($row['id'])),$hasPattern); 
        array_walk($lineData, 'filterData'); 
        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
        $i++;
    } 
}else{ 
    $excelData .= 'No records found...'. "\n"; 
} 
 
// Headers for download 
header("Content-Type: application/vnd.ms-excel"); 
header("Content-Disposition: attachment; filename=\"$fileName\""); 
 
// Render excel data 
echo $excelData; 
 
exit;