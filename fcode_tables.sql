-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 12, 2021 at 10:20 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sales_report`
--

-- --------------------------------------------------------

--
-- Table structure for table `fcode_pattern`
--

DROP TABLE IF EXISTS `fcode_pattern`;
CREATE TABLE IF NOT EXISTS `fcode_pattern` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) DEFAULT NULL,
  `sub_division_id` int(11) DEFAULT NULL,
  `pattern_ids` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `division_id` (`division_id`,`sub_division_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fcode_pattern`
--

INSERT INTO `fcode_pattern` (`id`, `division_id`, `sub_division_id`, `pattern_ids`, `created_at`) VALUES
(1, 1, NULL, '1,2,3,4,5,6', '2021-07-04 11:16:14'),
(3, 1, 133, '1,2,3,4,7', '2021-07-04 11:21:38'),
(6, 1, 156, '1,2,3,4', '2021-07-04 11:52:21'),
(7, 1, 113, '1,2,3,8,9', '2021-07-10 14:58:14');

-- --------------------------------------------------------

--
-- Table structure for table `fcode_pattern2`
--

DROP TABLE IF EXISTS `fcode_pattern2`;
CREATE TABLE IF NOT EXISTS `fcode_pattern2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `division_id` (`division_id`,`table_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fcode_pattern2`
--

INSERT INTO `fcode_pattern2` (`id`, `division_id`, `table_id`, `sort_order`) VALUES
(1, 1, 6, 1),
(3, 1, 2, 2),
(4, 1, 5, 3),
(5, 1, 7, 4),
(6, 1, 1, 5),
(7, 2, 1, 1),
(8, 2, 2, 2),
(9, 2, 3, 3),
(10, 2, 4, 4),
(11, 2, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `fcode_request`
--

DROP TABLE IF EXISTS `fcode_request`;
CREATE TABLE IF NOT EXISTS `fcode_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `code` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `branch_id` (`branch_id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fcode_request`
--

INSERT INTO `fcode_request` (`id`, `branch_id`, `status`, `code`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(3, 1, 1, NULL, 1, '2021-07-08 18:12:26', NULL, NULL),
(4, 1, 1, NULL, 1, '2021-07-10 13:01:10', NULL, NULL),
(5, 1, 1, NULL, 1, '2021-07-10 13:01:35', NULL, NULL),
(6, 1, 1, NULL, 1, '2021-07-10 18:45:47', NULL, NULL),
(7, 1, NULL, NULL, 1, '2021-07-10 18:58:38', NULL, '2021-07-11 15:48:03');

-- --------------------------------------------------------

--
-- Table structure for table `fcode_request_table`
--

DROP TABLE IF EXISTS `fcode_request_table`;
CREATE TABLE IF NOT EXISTS `fcode_request_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) DEFAULT NULL,
  `f_table_id` int(11) DEFAULT NULL,
  `f_table_val` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `request_id` (`request_id`,`f_table_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fcode_request_table`
--

INSERT INTO `fcode_request_table` (`id`, `request_id`, `f_table_id`, `f_table_val`) VALUES
(1, 3, 1, 1),
(2, 3, 2, 156),
(3, 3, 3, 90),
(4, 3, 4, 1),
(5, 4, 1, 1),
(6, 4, 2, 156),
(7, 4, 3, 91),
(8, 4, 4, 1),
(9, 5, 1, 1),
(10, 5, 2, 0),
(11, 5, 3, 91),
(12, 5, 4, 1),
(13, 5, 5, 1),
(14, 5, 6, 2),
(15, 6, 1, 1),
(16, 6, 2, 156),
(17, 6, 3, 91),
(18, 6, 4, 1),
(19, 7, 1, 1),
(20, 7, 2, 113),
(21, 7, 3, 90),
(22, 7, 8, 1),
(23, 7, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fcode_table`
--

DROP TABLE IF EXISTS `fcode_table`;
CREATE TABLE IF NOT EXISTS `fcode_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `f_table` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `table` (`f_table`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fcode_table`
--

INSERT INTO `fcode_table` (`id`, `name`, `f_table`) VALUES
(1, 'DIVISION', 'mis_division'),
(2, 'SUB DIVSION', 'mis_sub_division'),
(3, 'BRAND', 'api_brand'),
(4, 'ORIGIN BRAND', 'api_origin_brand'),
(5, 'API MODEL', 'api_model'),
(6, 'CAPACITY', 'api_capacity'),
(7, 'COLOR', 'api_color'),
(8, 'PIN TYPE', 'api_pin_type'),
(9, 'QUALITY', 'api_quality');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
