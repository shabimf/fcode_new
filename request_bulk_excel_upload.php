<?php
require_once 'excelsupport/PHPExcel.php';
require_once 'excelsupport/PHPExcel/IOFactory.php';

include "db/connect.php";
$obj = new database(); 
$page = "bulk request";
$exlel_message="";
if (isset($_SESSION['fcode_new_request']) && $_SESSION['fcode_new_request'] == 0) {
    header("location:" . $obj->cloud_url);
    exit;
}


if (isset($_POST['submit'])) {

    $data['division_id'] = $_POST['division_id'];
    $data['sub_division_id'] = ($_POST['sub_division_id'] ? $_POST['sub_division_id'] : NULL);  
    $data['brand_id'] = ($_POST['brand_id']? $_POST['brand_id'] : NULL);
    $data['excel'] = $_FILES['excel'];


    if ($_FILES['excel']['error'] == 0) {

        // print_r($data);
        // exit;
        $exlel_message = $obj->process_excel_new($data);
    }
}
if(isset($_GET['error'])){

        $exlel_message1="Configuration Not Found";

}

?>
<!doctype html>
<html lang="en">

<head>
    <? include "common/js_n_cs.php"; ?>
    <style>
    .hide { display: none !important;}
    .error {color:red}
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
      background-color:#840a22 !important;
      border:none !important;
      
    } .nav-tabs .nav-item {
      font-size:16px;
      color:#fff !important;
    }
  </style>
</head>

<body>
    <?php include "common/header.php"; ?>
    <div class="container-fluid body_bg ">
        <div class="d-flex flex-row">
            <?php include "common/nav.php"; ?>
            <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?= $_SESSION['container'] ?>" id="content_box" data-simplebar>
                <div class="col-sm-12 col-xs-12 inner-pad ">
                    <!-- <? include "common/title_bar.php"; ?> -->
                    <div class="alert alert-success request-msg-cls hide" role="alert">
                    </div>
                    <?php if(isset($_GET['error'])){ ?>
                    <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>
                                    <?php echo  $exlel_message1; ?>
                                </strong>
                    </div>
                    <?php } ?>

                    <?=$exlel_message?>



                    <div class="p-2 bg-black rounded shadow mb-5">
                        <div class="row">
                            <div class="col-md-12">
                                <nav class="tabcls">
                                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link offer active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true" data-id="0">Normal Request</a>


                                    </div>
                                </nav>
                                <div class="tab-content mt-2" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

                                        <form action="" method="post" enctype="multipart/form-data">
                                            <div class="col-sm-6">
                   

                                                <label> DIVISION<span class="error">*</span></label>
                                                <select class="chosen-select-deselect division_select f_input"  name="division_id" data-placeholder="SELECT  DIVISION"  >
                                                    <option value=""></option>
                                                    <option value="add">Add New</option>
                                                    <? $result = $obj->select_all("mis_division");
                                                        while ($row = $result->fetch_assoc()) { ?>
                                                            <option value="<?= $row['id'] ?>" <?if(isset($_SESSION['division_id'])) {
                                                            if($_SESSION['division_id']==$row['id']) echo "selected";
                                                            }?>><?= $row['name'] ?></option>
                                                        <? } ?>
                                                </select>

                                              
                                             

                                                <label>SUB DIVISION<span class="error">*</span></label>
                                                <select class="chosen-select-deselect subdivision_select f_input" name="sub_division_id" data-placeholder="SELECT SUB DIVISION"  onchange="showModelInput(this.value,2);" id="subdivision_select">
                                                    <option value=""></option>
                                                    <option value="add">Add New</option>
                                                    <? $result = $obj->select_all("mis_sub_division"," where is_fcode=1");
                                                        while ($row = $result->fetch_assoc()) { ?>
                                                            <option value="<?= $row['id'] ?>" <?if(isset($_SESSION['sub_division_id'])) {
                                                            if($_SESSION['sub_division_id']==$row['id']) echo "selected";
                                                            }?>><?= $row['name'] ?></option>
                                                        <? } ?>  
                                                </select>

<!--                                                 
                                                <label class="brand_title">BRAND<span class="brand-err error"></span></label>
                                                <select class="chosen-select-deselect brand_select f_brands f_input" id="brand_id" name="brand_id" data-placeholder="SELECT BRAND" onchange="showModelInput(this.value,3),getCode()">
                                                    <option value=""></option>
                                                    <option value="add">Add New</option>
                                                    <? $result = $obj->select_all("api_brand", "where status = 1");
                                                    while ($row = $result->fetch_assoc()) { ?>
                                                        <option value="<?= $row['id'] ?>" <?if(isset($_SESSION['brand_id'])) {
                                                            if($_SESSION['brand_id']==$row['id']) echo "selected";
                                                        }?>><?= $row['name'] ?></option>
                                                    <? } ?>
                                                </select> -->


                                                <div id="uploadField">
                                                    <div class="form-group">
                                                        <label>UPLOAD FILE :</label>
                                                        <input type="file" class="form-control  form-control-sm" name="excel"  />
                                                        <p><a href="#" onclick="downloadData()">Download Sample Excel</a></p>
                                                    </div>

                                                    <div class="form-group">
                                                        <button type="submit" name="submit" class="btn btn-sm btn-primary" id="send">SUBMIT</button>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <? include "common/up_icon.php"; ?>
            </div>
        </div>
    </div>

</body>

<?php include "common/choosen-init.php"; ?>
<script type="text/javascript" src="js/html2canvas.js"></script>
<script type="text/javascript" src="js/jquery.plugin.html2canvas.js"></script>
<script>
    $(document).ready(function () {
        $("form").submit(function () {
            $(".division_select").prop('disabled', false);
        });
    });
</script>
<script>
    function downloadData() {
        var form = document.createElement('form');
        form.action = 'ajax/fetch_data.php';
        form.method = 'GET';

        var divisionIdInput = document.createElement('input');
        divisionIdInput.type = 'hidden';
        divisionIdInput.name = 'division_id';
        divisionIdInput.value = document.querySelector('.division_select').value;
        form.appendChild(divisionIdInput);

        var subDivisionIdInput = document.createElement('input');
        subDivisionIdInput.type = 'hidden';
        subDivisionIdInput.name = 'sub_division_id';
        subDivisionIdInput.value = document.querySelector('.subdivision_select').value;
        form.appendChild(subDivisionIdInput);

       /* var brandIdInput = document.createElement('input');
        brandIdInput.type = 'hidden';
        brandIdInput.name = 'brand_id';
        brandIdInput.value = document.querySelector('.brand_select').value;
        form.appendChild(brandIdInput); */

        document.body.appendChild(form);

        form.submit();

        document.body.removeChild(form);
    }



</script>


</html>