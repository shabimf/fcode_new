<?php
include "db/connect.php";
$obj = new database();
if($obj->isAdmin() == false) {
    header("location:" . $obj->cloud_url);
    exit;
}
$page = "user";
$cloud_res = $obj->cloud_users();
$branch_list 		= array();
$where_b = " shipment_only=0";
$branch_res = $obj->select_all_active_where("mis_branches", $where_b);
while($b = mysqli_fetch_assoc($branch_res)){
	$branch_list[] = $b;
}

?>

<!doctype html>
<html lang="en">
<head>
  <? include "common/js_n_cs.php";?>
  <? include("common/data_table.php"); ?>
</head>
<body>
  <?php include "common/header.php";?>
  <div class="container-fluid body_bg ">
    <div class="d-flex flex-row">
      <?php include "common/nav.php";?>
      <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"
        data-simplebar>
        <div class="col-sm-12 col-xs-12 inner-pad ">
          <? include "common/title_bar.php";?>
          <form action="" method="post" id="userForm" name="userForm">
            <input name="action" id="action" value="add" type="hidden"/>   
            <input name="id" id="id" value="" type="hidden"/>    
            <div class="row">
                <div class="form-group col-md-4">
                    <label class="col-sm-12 pl-0 pr-0 text-white">NAME</label>
                    <div class="col-sm-12 pl-0 pr-0">
                        <input type="text" name="name" id="name" class="f_input" value="" required />
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label class="col-sm-12 pl-0 pr-0 text-white">USER TYPE</label>
                    <div class="col-sm-12 pl-0 pr-0">
                        <select data-placeholder="Choose a Type..." class="chosen-select" tabindex="-1" id="user_type" name="user_type" >
                            <option value=""></option>
                            <option value="1">Admin</option>
                            <option value="2">Normal User</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label class="col-sm-12 pl-0 pr-0 text-white">ASHCLOUD ID</label>
                    <div class="col-sm-12 pl-0 pr-0">
                        <select name="cloud_id" id="cloud_id" data-placeholder="Choose a User..." class="chosen-select" tabindex="-1">
                        <option value=""></option>
                        <? while($cloud_usr=mysqli_fetch_assoc($cloud_res)){ 
      
                        ?>
                        <option <?=$sel?> value="<?=$cloud_usr['id']?>"><?=$cloud_usr['username']?></option>
                        <? } ?>  
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label class="col-sm-12 pl-0 pr-0 text-white">STATUS</label>
                    <div class="col-sm-12 pl-0 pr-0">
                        <select  name="status" id="status" data-placeholder="Choose a Status..." class="chosen-select" tabindex="-1">
                            <option value=""></option>
                            <option value="1">Active</option>
                            <option value="2">Inactive</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label class="col-sm-12 pl-0 pr-0 text-white">BRANCH</label>
                    <div class="col-sm-12 pl-0 pr-0">
                        <select name="branch_id" id="branch_id"  data-placeholder="Choose a Branch..." class="chosen-select" tabindex="-1">
                        <option value=""></option>
                        <? foreach($branch_list as $branch){ 
                           
                            ?>
                            <option <?=$sel?>  value="<?=$branch['id']?>"><?=$branch['name']?></option>
                        <? } ?>  
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label class="col-sm-12 pl-0 pr-0 text-white">EMAIL ID</label>
                    <div class="col-sm-12 pl-0 pr-0">
                        <input type="text" name="email" id="email" class="f_input" value="" required />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label class="col-sm-12 pl-0 pr-0 text-white">Phone No</label>
                    <div class="col-sm-12 pl-0 pr-0">
                        <input type="text" name="phone_no" id="phone_no" class="f_input" value="" required />
                    </div>
                </div>
               <div class="form-group col-md-6">
                  <label class="col-sm-12 pl-0 pr-0 text-white">ACCESS</label>
                    <div class="col-sm-12 pl-0 pr-0">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="new_request" value="1"  name="new_request"/>
                            <label class="form-check-label text-white" for="new_request">NEW REQUEST</label>
                        </div>

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="manage_request" value="1" name="manage_request"/>
                            <label class="form-check-label text-white" for="manage_request"> MANAGE REQUESTS</label>
                        </div>

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="manage_fcode" value="1" name="manage_fcode" />
                            <label class="form-check-label text-white" for="manage_fcode">MANAGE FCODE</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="cancel_request" value="1" name="cancel_request"  />
                            <label class="form-check-label text-white" for="cancel_request">CANCEL REQUEST</label>
                        </div>
                    </div>
               </div>
            </div>
                        
            <div class="form-group mt-3">
                <button name="add" type="submit" class="btn btn-info btn-cls">ADD</button>
                <a href="user" class="btn btn-secondary">CANCEL</a>
            
            </div> 
                        
        </form>
            <table class="table table-bordered text-white" id="userList">
              <thead>
                <tr class="bg-grey">
                  <th>Sl NO</th>
                  <th>NAME</th>
                  <th>EMAIL</th>
                  <th>PHONE NO</th>
                  <th>TYPE</th>
                  <th>BRANCH</th>
                  <th>ACCESS</th>
                  
                  <th>STATUS</th>
                  <th width="8%">ACTION</th>
                </tr>
              </thead>

            </table>
        </div>
        <!-- <? include "common/up_icon.php";?> -->
      </div>
    </div>
  </div>

</body>
<?php include "common/choosen-init.php";?>
<link rel="stylesheet" href="<?=$obj->base_url?>css/simple-notify.min.css" />
<script src="<?=$obj->base_url?>js/notify.js"></script>
<script src="<?=$obj->base_url?>js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    $(document).ready(function(){
        var userData = $('#userList').DataTable({
            "lengthChange": false,
            "processing":true,
            "serverSide":true,
            "order":[],
            "ajax":{
                url:"<?=$obj->base_url?>ajax/list_user",
                type:"POST",
                dataType:"json"
            },
            columnDefs: [{
                "targets": 0,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            }],
            "pageLength": 10
        });	
        $("#userForm").validate({
			rules: {
				name: "required",
                user_type: "required",
                cloud_id: "required",
                status: "required",
                branch_id: "required",
                phone_no: {
                    required: true,
                    minlength: 10,
                    maxlength:20
                },
                email: {
                    required: true,
                    email: true
                }
			},
			messages: {
				name: "Please enter name",
                user_type: "Please select type",
                cloud_id:  "Please select user",
				status:  "Please select status",
                branch_id: "Please select branch",
                email: "Please enter a valid email address",
                phone_no: {
                    required:  "Please enter phone no"
                   
                }
			},
            errorPlacement: function(error, element) {
            var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertBefore(element);
                }
            },
            submitHandler: function(form) { 
                $.ajax({
                    url:'<?=$obj->base_url?>ajax/add_user',
                    data: $(form).serialize(),
                    type:'POST',
                    dataType: 'json',
                    success: function(response) {
                        if(response.status == 1){
                            $("#userForm")[0].reset();
                            $('#user_type,#branch_id,#cloud_id,#status').val('').trigger("chosen:updated");
                        } 
                        userData.ajax.reload();
                        $(".btn-cls").html("ADD");
                        $('#action').val('add');
                        $("#id").val('');
                        
                        var notify =   new Notify ({
                                    status: response.class,
                                    title: '',
                                    text: response.message,
                                    effect: 'slide',
                                    speed: 300,
                                    customClass: '',
                                    customIcon: '',
                                    showIcon: true,
                                    showCloseButton: true,
                                    autoclose: true,
                                    autotimeout: 3000,
                                    gap: 20,
                                    distance: 20,
                                    type: 3,
                                    position: 'right top'
                            });
                    }
                });
                return false; 
            }
		});
        $("#userList").on('click', '.edit', function(){
            var userId = $(this).attr("id");
            $('#manage_request,#new_request,#manage_fcode,#cancel_request').prop('checked', false);
            $.ajax({
                url:'<?=$obj->base_url?>ajax/add_user',
                data:{userId:userId, action:'edit'},
                type:'POST',
                dataType: 'json',
                success: function(response) {
                    $("#name").val(response.name).focus();
                    $('#user_type').val(response.user_type).trigger("chosen:updated");
                    $('#branch_id').val(response.branch_id).trigger("chosen:updated");
                    $('#cloud_id').val(response.cloud_id).trigger("chosen:updated");
                    $('#status').val(response.status).trigger("chosen:updated");
                    $("#email").val(response.email);
                    $("#phone_no").val(response.phone_no);
                    
                    if(response.new_request == 1) $('#new_request').prop('checked', true);
                    if(response.manage_request == 1) $('#manage_request').prop('checked', true);
                    if(response.manage_fcode == 1) $('#manage_fcode').prop('checked', true);
                    if(response.cancel_request == 1) $('#cancel_request').prop('checked', true);
                    $(".btn-cls").html("UPDATE");
                    $('#action').val('update');
                    $("#id").val(userId);
                }
            });
        });
        $(document).on('click','.delete',function(){
            var userId = $(this).attr("id");		
            var action = "delete";
            if(confirm("Are you sure you want to delete this user?")) {
                $.ajax({
                    url:'<?=$obj->base_url?>ajax/add_user',
                    method:"POST",
                    data:{id:userId, action:action},
                    success:function(response) {
                        var notify =   new Notify ({
                                    status: response.class,
                                    title: '',
                                    text: response.message,
                                    effect: 'slide',
                                    speed: 300,
                                    customClass: '',
                                    customIcon: '',
                                    showIcon: true,
                                    showCloseButton: true,
                                    autoclose: true,
                                    autotimeout: 3000,
                                    gap: 20,
                                    distance: 20,
                                    type: 3,
                                    position: 'right top'
                            });					
                        userData.ajax.reload();
                    }
                })
            } else {
                return false;
            }
        });
    });
</script>
</html>