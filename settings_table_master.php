<?php
include "db/connect.php";
$obj = new database();
if($obj->isAdmin() == false) {
    header("location:" . $obj->cloud_url);
    exit;
}


$page = "settings";



?>

<!doctype html>
<html lang="en">

<head>

    <? include "common/js_n_cs.php";?>

</head>

<body>
    <?php include "common/header.php";?>
    <div class="container-fluid body_bg">
        <div class="d-flex flex-row">

            <?php include "common/nav.php";?>

            <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"
                data-simplebar>
                <div class="col-sm-12 col-xs-12 inner-pad">

                    <? include "common/title_bar.php";?>
                    <? include "common/settings_sub_nav.php";?>


                   
                    <div class="row">
                        <div class="col-3">
                            <table class="table table-bordered table-dark table-hover">
                                    <tr class="bg-dark">
                                        <th width="40">#</th>
                                        <th width="250">MASTER</th>
                                        <th width="50"></th>
                                    </tr>                                    
                                    <? 
                                    $i=0;
                                    $result = $obj->select_all("fcode_table");
                                    while($row = $result->fetch_assoc()){  ?>
                                        <tr>
                                            <td> <?=++$i?> </td>
                                            <td> <?=$row['name']?> </td>
                                            <td></td>
                                        </tr>
                                    <? }?>
                                
                            </table>
                        </div>
                    </div>

                </div>
                <? include "common/up_icon.php";?>
            </div>
        </div>
    </div>



</body>


<?php include "common/choosen-init.php";?>

</html>