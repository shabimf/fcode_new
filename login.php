<?php
include "db/connect.php";
$obj = new database();

if (isset($_REQUEST['token'])) {
	if (trim($_REQUEST['token']) != "") {
		$decode  = base64_decode($_REQUEST['token']);
		$auth_array = explode("-", $decode);
		if (count($auth_array) == 2) {
			$time = $auth_array[0];
			if (time() - $time < 600) {
				$id   = $auth_array[1];
				$result = $obj->login_check_cloud_user($id);
				$num_row = mysqli_num_rows($result);
	
				if ($num_row > 0) {

					/*$row=mysqli_fetch_assoc($result);

					$result = $obj->login_check( $row['username'], $row['password'] );
					$num_row=mysqli_num_rows($result);

					if($num_row>0){*/

					$row = mysqli_fetch_assoc($result);

					$_SESSION['user_id'] = $row['id'];
					$_SESSION['name'] = $row['name'];
					$_SESSION['branch_id'] = $row['branch_id'];
					$_SESSION['fcode_new_request'] = $row['new_request'];
					$_SESSION['fcode_manage_request'] = $row['manage_request'];
					$_SESSION['manage_fcode'] = $row['manage_fcode'];
					$_SESSION['fcode_request_cancel'] = $row['cancel_request'];
					$_SESSION['user_type'] = $row['user_type'];
					
					$branch_name = $obj->select_name_by_id("mis_branches", $row['branch_id']);
					list($br_prefix) = explode('-', $branch_name);

					$_SESSION['branch_name'] = $br_prefix;

					$_SESSION['branch_search'] = "";
					if ($obj->isAdmin() == false)
						$_SESSION['branch_search'] = " AND branch.id =" . $_SESSION['branch_id'];

                  
					header("location:" . $obj->base_url);
					exit;


				}
			} else {
				$error = "Invalid Token, Please try again!";
			}
		} else {
			$error = "Invalid Token, Please try again!";
		}
	} else {
		$error = "Invalid Url, Please try again!";
	}

	if ($error != "") {
		header("Location:" . $cloud_link);
		exit;
	}
} else {
	header("location:" . $obj->cloud_url);
	exit;
}
