<?php
include "db/connect.php";
$obj = new database();

$page = "fcode_request_created";
if (isset($_SESSION['fcode_manage_request']) && $_SESSION['fcode_manage_request'] == 0) {
    header("location:" . $obj->cloud_url);
    exit;
}



if ($_GET['from_date'] != '') {
    $where .= " AND DATE(req.is_create_date) >= '" . $_GET['from_date'] . "'";
}
if ($_GET['to_date'] != '') {
    $where .= " AND DATE(req.is_create_date) <= '" . $_GET['to_date'] . "'";
}


$limit = 10;
$page_num = (isset($_GET['page_num']) && is_numeric($_GET['page_num'])) ? $_GET['page_num'] : 1;
$paginationStart = ($page_num - 1) * $limit;

// Prev + Next
$prev = $page_num - 1;
$next = $page_num + 1;





?>

<!doctype html>
<html lang="en">

<head>

    <? include "common/js_n_cs.php"; ?>

</head>

<body>
    <?php include "common/header.php"; ?>
    <div class="container-fluid body_bg ">
        <div class="d-flex flex-row">

            <?php include "common/nav.php"; ?>

            <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?= $_SESSION['container'] ?>" id="content_box" data-simplebar>
                <div class="col-sm-12 col-xs-12 inner-pad ">

                    <? include "common/title_bar.php"; ?>

                    <div class="card">
                        <div class="card-body">
                            <form action="" method="get">
                                <div class="row">

                                    <div class="col-sm-2">
                                    <label class="text-danger">FROM DATE</label>
                                        <input type="date" class="f_input" name="from_date" value="<?php echo $_GET['from_date'] ?>">
                                    </div>
                                    <div class="col-sm-2">
                                    <label class="text-danger">TO DATE</label>
                                        <input type="date" class="f_input" name="to_date" value="<?php echo $_GET['to_date'] ?>">
                                    </div>
 
                                    <div class="col-sm-2">
                                    <label class="text-danger">&nbsp</label>
                                        <button class="btn btn-primary btn-block search_btn">SEARCH</button>
                                    </div>
                                    <div class="col-sm-2">
                                    <label class="text-danger">&nbsp</label>
                                        <a href="export_created_list?conditions=<?=$where?>" class="btn btn-success btn-block">EXPORT</a>
                                    </div>
                                 
                                 
                                </div>

                            </form>

                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover  table-dark">
                            <tbody>

                                <tr class="bg-dark">
                                    <th>Ticket No</th>
                                    <th>DATE</th>
                                    <th>CREATED AT</th>
                                    <th>BRANCH</th>
                                    <th>REQUESTED</th>
                                    <th>NEW REQUESTED</th>
                                    <th>NAME</th>
                                    <th>CODE</th>


                                    <th>HSN</th>
                                    <th>PART NO</th>
                                    <th>PRODUCT BARCODE</th>
                                    <th>BARCODE</th>
                                    <th>REMARKS</th>

                                </tr>

                                <?
                                $result = $obj->select_all_request_approved($where,$paginationStart, $limit);
                                $allRecrods = $obj->count_all_request_approved($where);
                                $totoalPages = ceil($allRecrods / $limit);
                                $i = $paginationStart;
                                while ($row = $result->fetch_assoc()) {
                                ?>
                                    <tr data-id="<?= $row['id'] ?>">
                                        <td><?= $row['unique_id'] ?><br />

                                        </td>
                                        <td> <?= date("d-M-Y h:i:a", strtotime($row['created_at'])) ?>

                                            <? if ($row['offer'] == 1) { ?>
                                                <span class="w3-badge label-danger">Bundle Offer</span>
                                            <? } ?>
                                        </td>
                                        <td> <?= date("d-M-Y h:i:a", strtotime($row['is_create_date'])) ?></td>
                                        <td> <?= $obj->select_name_by_id("mis_branches", $row['branch_id']); ?>
                                            <br />(<?= $row['user_name'];
                                                    if ($row['email']) echo "<br/>" . $row['email'];
                                                    if ($row['phone_no']) echo "<br/>" . $row['phone_no'];
                                                    ?>)<br />
                                            <? if ($row['approved_by'] > 0) {
                                                echo ($row['status'] == 1 ? "Approved By: " : "Rejected BY: ");
                                                echo "<span class='text-danger'>" . $obj->select_field_by_id("fcode_user", "name", $row['approved_by']) . "</span>";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <a data-image="screenshots/<?= $row['id'] . ".png"; ?>" href="javascript:void(0)" class="imagecls"><?
                                                                                                                                                $color = "";
                                                                                                                                                $pattern = [];
                                                                                                                                                $pattern_arr = array();
                                                                                                                                                $result_table = $obj->select_requested_pattern($row['id']);
                                                                                                                                                $is_manual = 0;
                                                                                                                                                while ($row_table = $result_table->fetch_assoc()) {
                                                                                                                                                    if ($row_table['f_table_id'] == 0) { // manual
                                                                                                                                                        $pattern[] = "MANUAL  => <small class='text-warning'>" . $row_table['f_table_val'] . "   </small> <br/>";
                                                                                                                                                    } else {
                                                                                                                                                        $row_p = $obj->select_all_by_id("fcode_table", $row_table['f_table_id']);
                                                                                                                                                        if ($row_table['is_manual'] == 1) {
                                                                                                                                                            $is_manual = 1;
                                                                                                                                                            $pattern[] = $row_p['name'] . "  => <small class='text-warning'> " . $row_table['f_table_val'] . " </small> <br/>";
                                                                                                                                                        } else {
                                                                                                                                                            $code = "";
                                                                                                                                                            if ($row_table['f_table_val'] > 0 && $row_table['f_table_id'] > 0) {
                                                                                                                                                                $fcode_result = $obj->select_all_data("fcode_pattern_code", " parent_id='" . $row['pattern_id'] . "' AND f_table_id='" . $row_table['f_table_id'] . "' AND f_table_val='" . $row_table['f_table_val'] . "' AND code_id IS NOT NULL");
                                                                                                                                                                if ($fcode_result) {
                                                                                                                                                                    $code_id = $fcode_result['code_id'];
                                                                                                                                                                    $code_row = $obj->select_all_by_id("fcode_table_code", $code_id);
                                                                                                                                                                    $code = $code_row['focus_code'];
                                                                                                                                                                } else {
                                                                                                                                                                    if ($row_table['f_table_id'] == 24) {
                                                                                                                                                                        $color = "text-danger fs-3";
                                                                                                                                                                        $code = "<span class='text-danger font-weight-bold'>" . $obj->groupConcatCondition($row_table['f_table_id'], $row_table['f_table_val']) . "</span>";
                                                                                                                                                                    } else {
                                                                                                                                                                        $fcode_result = $obj->select_all_data("fcode_table_code", " f_table_id='" . $row_table['f_table_id'] . "' AND f_table_val='" . $row_table['f_table_val'] . "' AND is_primary=1");
                                                                                                                                                                        $code = ($fcode_result ? $fcode_result['focus_code'] : "");
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                            // if($code) {
                                                                                                                                                            $pattern[] = $row_p['name'] . "  => <small class='text-warning'> " . $code . " </small> <br/>";
                                                                                                                                                            // }

                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                    $pattern_arr[$row_table['f_table_id']] = $row_table['f_table_val'];
                                                                                                                                                }
                                                                                                                                                $requested_pattern = $row['requested_pattern'];
                                                                                                                                                $requested_pattern_arr = unserialize($requested_pattern);
                                                                                                                                                foreach ($requested_pattern_arr as $k => $v) {
                                                                                                                                                    $row_p = $obj->select_all_by_id("fcode_table", $k);
                                                                                                                                                    if (is_int($v) || $k == 24) {
                                                                                                                                                        $fcode_result = $obj->select_all_data("fcode_pattern_code", " parent_id='" . $row['pattern_id'] . "' AND f_table_id='" . $k . "' AND f_table_val='" . $v . "' AND code_id IS NOT NULL");
                                                                                                                                                        if ($fcode_result) {
                                                                                                                                                            $code_id = $fcode_result['code_id'];
                                                                                                                                                            $code_row = $obj->select_all_by_id("fcode_table_code", $code_id);
                                                                                                                                                            $code = $code_row['focus_code'];
                                                                                                                                                        } else {
                                                                                                                                                            if ($k == 24) {
                                                                                                                                                                $color = "text-danger fs-3";
                                                                                                                                                                $code = "<span class='text-danger font-weight-bold'>" . $obj->groupConcatCondition($k, $v) . "</span>";
                                                                                                                                                            } else {

                                                                                                                                                                $fcode_result = $obj->select_all_data("fcode_table_code", " f_table_id='" . $k . "' AND f_table_val='" . $v . "' AND is_primary=1");
                                                                                                                                                                $code = ($fcode_result ? $fcode_result['focus_code'] : "");
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    } else {
                                                                                                                                                        $code = $v;
                                                                                                                                                    }
                                                                                                                                                    echo $row_p['name'] . "  => <small class='text-warning'> ";
                                                                                                                                                    echo $code . " </small> <br/>";
                                                                                                                                                }
                                                                                                                                                ?>
                                        </td>
                                        <td>
                                            <?
                                            $diff = array_diff_assoc($requested_pattern_arr, $pattern_arr);

                                            if (count($diff) > 0) {
                                            ?>
                                                <a href='edit_request?request_id=<?= base64_encode($row['id']) ?>&view=1'><?= implode("  ", $pattern) ?></a>
                                            <?
                                            }

                                            ?>
                                        </td>
                                        <td>
                                            <?
                                            $res = $obj->generate_description($row['id'], $row['pattern_id']);
                                            $modifiedRes = preg_replace_callback('/\b(\d+)([a-zA-Z])/', function ($matches) {
                                                return $matches[1] . strtoupper($matches[2]);
                                            }, ucwords(strtolower($res)));

                                            echo ($row['name'] ? ucwords(strtolower($row['name'])) : $modifiedRes);

                                            ?>
                                        </td>
                                        <td class="code_td">
                                            <span class="<?= $color ?> text-center">
                                                <?
                                                echo $row['code'] ? strtoupper($row['code']) : strtoupper($obj->generate_fcode($row['id'], $row['pattern_id']));
                                                ?>
                                            </span>

                                            <i class="far fa-copy float-right fa-lg cp hide copy_icon" style="line-height: unset;"></i>
                                        </td>



                                        <td>
                                            <?= ($row['hsn'] ? $row['hsn'] : "--") ?>
                                        </td>
                                        <td>
                                            <?= ($row['partno'] ? $row['partno'] : "--") ?>
                                        </td>
                                        <td>
                                            <?= ($row['barcode'] ? $row['barcode'] : "--") ?>
                                        </td>
                                        <td>
                                            <?= ($row['new_barcode'] ? $row['serialcode'] . $row['new_barcode'] : "--") ?>
                                        </td>
                                        <td>
                                            <?= ($row['remarks'] ? $row['remarks'] : "--") ?>
                                        </td>

                                    </tr>
                                <? } ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- Pagination -->
                    <!-- <nav aria-label="Page navigation example mt-5 float-right">
                        <ul class="pagination justify-content-end">
                            <li class="page-item <?php if ($page_num <= 1) {
                                                        echo 'disabled';
                                                    } ?>">
                                <a class="page-link" href="<?php if ($page_num <= 1) {
                                                                echo '#';
                                                            } else {
                                                                echo "?page_num=" . $prev;
                                                            } ?>">Previous</a>
                            </li>

                            <?php for ($i = 1; $i <= $totoalPages; $i++) : ?>
                                <li class="page-item <?php if ($page_num == $i) {
                                                            echo 'active';
                                                        } ?>">
                                    <a class="page-link" href="fcode_request_all?page_num=<?= $i; ?>"> <?= $i; ?> </a>
                                </li>
                            <?php endfor; ?>

                            <li class="page-item <?php if ($page_num >= $totoalPages) {
                                                        echo 'disabled';
                                                    } ?>">
                                <a class="page-link" href="<?php if ($page_num >= $totoalPages) {
                                                                echo '#';
                                                            } else {
                                                                echo "?page_num=" . $next;
                                                            } ?>">Next</a>
                            </li>
                        </ul>
                    </nav> -->
                    <nav aria-label="Page navigation example mt-5 float-right">
    <ul class="pagination justify-content-end">
        <li class="page-item <?php if($page_num <= 1){ echo 'disabled'; } ?>">
            <a class="page-link"
                href="<?php if($page_num <= 1){ echo '#'; } else { echo "?page_num=" . $prev; } ?>">Previous</a>
        </li>

        <?php
        $maxPagesToShow = 5; // You can adjust this number based on your preference
        $halfMax = floor($maxPagesToShow / 2);
        $startPage = max(1, $page_num - $halfMax);
        $endPage = min($totoalPages, $startPage + $maxPagesToShow - 1);

        if ($startPage > 1) {
            echo '<li class="page-item"><a class="page-link" href="?page_num=1&code=' . $_GET['code'] . '&branch=' . $_GET['branch'] . '&status=' . $_GET['status'] . '&from_date=' . $_GET['from_date'] . '&to_date=' . $_GET['to_date'] . '">1</a></li>';
            if ($startPage > 2) {
                echo '<li class="page-item disabled"><span class="page-link">...</span></li>';
            }
        }

        for ($i = $startPage; $i <= $endPage; $i++):
        ?>
            <li class="page-item <?php if($page_num == $i) {echo 'active'; } ?>">
                <a class="page-link" href="fcode_request_all?page_num=<?= $i; ?>&code=<?= $_GET['code']; ?>&branch=<?= $_GET['branch'] ?>&status=<?= $_GET['status'] ?>&from_date=<?= $_GET['from_date'] ?>&to_date=<?= $_GET['to_date'] ?>"> <?= $i; ?> </a>
            </li>
        <?php
        endfor;

        if ($endPage < $totoalPages) {
            if ($endPage < $totoalPages - 1) {
                echo '<li class="page-item disabled"><span class="page-link">...</span></li>';
            }
            echo '<li class="page-item"><a class="page-link" href="?page_num=' . $totoalPages . '&code=' . $_GET['code'] . '&branch=' . $_GET['branch'] . '&status=' . $_GET['status'] . '&from_date=' . $_GET['from_date'] . '&to_date=' . $_GET['to_date'] . '">' . $totoalPages . '</a></li>';
        }
        ?>

        <li class="page-item <?php if($page_num >= $totoalPages) { echo 'disabled'; } ?>">
            <a class="page-link"
                href="<?php if($page_num >= $totoalPages){ echo '#'; } else {echo "?page_num=". $next; } ?>">Next</a>
        </li>
    </ul>
</nav>


                </div>
                <? include "common/up_icon.php"; ?>
            </div>
        </div>
    </div>
    <div id="imagemodal" class="modal">
        <span class="close" data-dismiss="modal" aria-label="Close">&times;</span>

        <img class="modal-content" id="img01">
        <div id="caption"></div>
    </div>

    <!-- Modal -->

    </div>


</body>
<?php include "common/choosen-init.php"; ?>

<script>



</script>

</html>