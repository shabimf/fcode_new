<?php
require_once 'excelsupport/PHPExcel.php';
require_once 'excelsupport/PHPExcel/IOFactory.php';

include "db/connect.php";
$obj = new database();

if($obj->isAdmin() == false) {
    header("location:" . $obj->cloud_url);
    exit;
}

$page = "settings";

  
// if(isset($_FILES['code_pattern']) && isset($_POST['description'])){
//     if($_FILES['code_pattern']['error'] == 0){
//         $report = $obj->process_code_pattern_excel($_FILES);
//     }
// }

if(isset($_FILES['code_pattern'])){
    if($_FILES['code_pattern']['error'] == 0){
        $report = $obj->process_code_pattern_excel($_FILES);
    }
}





?>

<!doctype html>
<html lang="en">

<head>

    <? include "common/js_n_cs.php";?>


</head>

<body>
    <?php include "common/header.php";?>
    <div class="container-fluid body_bg">
        <div class="d-flex flex-row">

            <?php include "common/nav.php";?>

            <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"
                data-simplebar>
                <div class="col-sm-12 col-xs-12 inner-pad">

                    <? include "common/title_bar.php";?>

                    <? include "common/settings_sub_nav.php";?>


                    <?
                    if(isset($report['error'])){             
                            $error =$report['error'];
                        ?>
                    <div class="alert alert-danger" role="alert">
                        <?=$error?>
                    </div>
                    <? } else if(isset($report['success'])){ ?>
                    <div class="alert alert-success" role="alert">
                        <?=$report['success']?>
                    </div>
                    <? }?>

                   
                    <div class="row">


                        <div class="col-sm-6 col text-white">
                            <h5>CODE/DESCRIPTION PATTERN UPLOAD</h5>
                            <hr>
                            <form action="" method="post" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label>EXCEL</label>
                                    <input type="file" class="form-control  form-control-sm" name="code_pattern" required />
                                    <a href="Sample_pattern_example_format.xlsx" class="text-warning float-right mt-2">DOWNLOAD FORMAT</a>
                                </div>

                                <div class="form-group">
                                    <button name="upload" type="submit" class="btn btn-info">ADD</button>
                                </div>

                            </form>
                        </div>

                        <div class="col-sm-5 col text-white hide">
                            <h5>DESCRIPTION PATTERN UPLOAD</h5>
                            <hr>
                            <form action="" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="description" value="1">
                                <div class="form-group">
                                    <label>EXCEL</label>
                                    <input type="file" class="form-control  form-control-sm" name="code_pattern" required />
                                    <a href="Sample_pattern_example.xlsx" class="text-warning float-right mt-2">DOWNLOAD FORMAT</a>
                                </div>

                                <div class="form-group">
                                    <button name="upload" type="submit" class="btn btn-info">ADD</button>
                                </div>

                            </form>
                        </div>

                    </div>
                    <? include "common/up_icon.php";?>
                </div>
            </div>
        </div>
</body>

</html>