<script type="text/javascript">
$( document ).ready(function() {
	   $("#toggle_menu").click(function() {
		   
				var expand = $(this).attr("data-expand");
				if(expand==1){
					$(".content_box").css({'-ms-flex':'0 0 97%', 'flex':'0 0 97%'});
					$("#left_nav").animate({marginLeft:'-13.5%'}, { duration: 300, queue: true });
				    $(".content_box").animate({'max-width':'97%'}, { duration: 1000, queue: true });
					$(this).attr("data-expand",0);
					$(this).html('<img src="<?=$obj->base_url?>images/arrow_open.png" border="0" width="90%">');
				}
				else{
					
					$(this).attr("data-expand",1);
					$(".content_box").css({'-ms-flex':'0 0 83.333333%', 'flex':'0 0 83.333333%'});
					$(".content_box").animate({'max-width':'83.33333333%'});
				    $("#left_nav").animate({marginLeft:'0'});
					$(this).html('<img src="<?=$obj->base_url?>images/arrow_close.png" border="0" width="90%">');  
				}
				
				$.ajax({
					 type : "POST",
					 url: "ajax/set-leftnav.php",
					 data: {"expand":expand},
					 success: function(response){
						
					 }
				});
				
	   });		   
	   $(".simplebar-content-wrapper").scroll(function() {
			if($(this).scrollTop()>50){
				 document.getElementById("myBtn").style.display = "block";
			}else{
				 document.getElementById("myBtn").style.display = "none";
			}
	   });		
});	



$(document).on("click",".mov_to_detail",function(e){
	var box = $(".simplebar-content").height();
	var target_ = $(this).attr("data-id");
	var target  = $('.'+target_);
	if(target.length>0){
			var h 		 = target.offset().top;
			var container = $('.content_box .simplebar-content');
			var x 		 = h-container.offset().top+container.scrollTop()-150;
			if (target.length) {
						$('.simplebar-content-wrapper').animate({
									  scrollTop: x
						}, 1000);
						return false;
			}
	}			
});

function topFunction(){
	var container = $('.content_box .simplebar-content');
	$('.simplebar-content-wrapper').animate({ scrollTop: 0 }, 1000);
}

</script>

<div class="col-sm-2 d-none d-sm-block left_nav <?=$_SESSION['nav']?>"  id="left_nav" >

<div class="sidebar-nav" >
  <ul class="nav_ nav-list">
    <li ><a href="javascript:void(0)" class="menu_ menu_label menu_inactive">MENU</a> </li>
	<?if(isset($_SESSION['fcode_new_request']) && $_SESSION['fcode_new_request'] ==1 ) { ?>
    <li>
      	<a href="index" class="menu_list_box <?=(isset($page) && $page == 'new request') ? 'actv_nav' : ''?>">
        	NEW
        </a> 
    </li>
	<?php }if(isset($_SESSION['fcode_manage_request']) && $_SESSION['fcode_manage_request'] ==1 ) { ?>
    <li>
    	<a href="request" class="menu_list_box  <?=(isset($page) && $page == 'request' || $page == 'EDIT REQUEST' || $page == 'VIEW REQUEST') ? 'actv_nav' : ''?>" >
        	REQUEST 
        </a>
    </li>
	<? } if(isset($_SESSION['manage_fcode']) && $_SESSION['manage_fcode'] ==1 ) { ?>
    <li>
    	<a href="fcode" class="menu_list_box  <?=(isset($page) && $page == 'fcode') ? 'actv_nav' : ''?>" >
        	FCODE 
        </a>
    </li>

	<? } if($obj->isAdmin() == true)  { ?>
	<li>
    	<a href="request_report"  class="menu_list_box <?=(isset($page) && $page == 'request_report') ? 'actv_nav' : ''?>" >
        	REPORT 
        </a>
    </li>
    <li>
    	<a href="settings_table_master"  class="menu_list_box <?=(isset($page) && $page == 'settings') ? 'actv_nav' : ''?>" >
        	SETTINGS 
        </a>
    </li>
    <? } ?>

    <li><a  class="logo_pos menu_inactive">POSBOS</a></li>
  </ul>
</div>
<div class="menu_toggle" id="toggle_menu" data-expand="<?=$_SESSION['csh_nav']?>">
	<? if($_SESSION['csh_nav']==1){ ?>
		<img src="<?=$obj->base_url?>images/arrow_close.png" border="0" width="90%">
	<? }else{ ?> 
		<img src="<?=$obj->base_url?>images/arrow_open.png" border="0" width="90%">
	<? } ?> 
</div>

</div>