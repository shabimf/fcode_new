<?php
include "db/connect.php";
$obj = new database();

if($obj->isAdmin() == false) {
    header("location:" . $obj->cloud_url);
    exit;
}

$page = "settings";

if(isset($_POST['add_brand'])){
	$data = array();
	$data['name'] = trim($_POST['name']);
    $data['own_brand'] = $_POST['own_brand'] ?? null;
    $data['status'] = $_POST['status'];
    $obj->con->autocommit(false);
    $error = [];
    if ($obj->check_if_already_exist_master_name("api_brand",$data['name'],$_POST['id'])>0) {
        $error[] = $data['name']." already exists";
    }  else {
        if(isset($_POST['id'])) {
            $obj->update_data("api_brand", $data, $_POST['id']);
            if($_POST['brand_ids']) {
                $brand_idss = trim($_POST['brand_ids'],",");
                $obj->delete_where("fcode_table_code", "WHERE id IN (".$brand_idss.")");
            }
            if($_POST['brand_alt_ids']) {
                $brand_alt_ids = trim($_POST['brand_alt_ids'],",");
                $obj->delete_where("fcode_table_alternative_name", "WHERE id IN (".$brand_alt_ids.")");
            }
            $udata['f_table_val'] = $udata_alt['f_table_val'] = $_POST['id'];
        } else {
            $udata['f_table_val']  = $udata_alt['f_table_val'] = $obj->insert_data("api_brand", $data);	
        }
    
    $flag = 0;
    for ($i=0;$i<count($_POST['focus_code']);$i++) {
        $udata['f_table_id'] = 3;
        if ($_POST['focus_code'][$i]) {
            $udata['focus_code'] = $_POST['focus_code'][$i];
            $udata['is_primary'] = ($_POST['is_primary'][$i]?(int)$_POST['is_primary'][$i]:0);
            if($udata['is_primary']) $flag = 1;
            if($_POST['focus_code_id'][$i]>0) {
                $focus_code_id =  $_POST['focus_code_id'][$i];
                $obj->update_data("fcode_table_code", $udata, $_POST['focus_code_id'][$i]);
            } else {
                $focus_code_id =  $obj->insert_data("fcode_table_code", $udata);	
            }
            if ($i==0) $fid = $focus_code_id;
           
        } 
    }
   

   
    for ($i=0;$i<count($_POST['alternative_name']);$i++) {
        $udata_alt['name'] = $_POST['alternative_name'][$i];
        $udata_alt['f_table_id'] = 3;
        if ($udata_alt['name']) {
            if ($_POST['alternative_name_id'][$i]>0) {
                $obj->update_data("fcode_table_alternative_name", $udata_alt, $_POST['alternative_name_id'][$i]);
            } else {
                $obj->insert_data("fcode_table_alternative_name", $udata_alt);	
            }
        }
       
    }
    if ($flag == 0) {
        $udata_code['is_primary'] = 1;
        $obj->update_data("fcode_table_code", $udata_code, $fid);
    }
    }
    if (count($error) == 0 ) {
        $obj->con->commit();
    } else {
        setcookie("error_brand", json_encode($error), time() + 1);  
    }
   
    header("location:".$obj->thispage());	
	exit;
}

if (isset($_POST['add'])) {

    $data = array();
    $error = [];
    $obj->con->autocommit(false);
    $data['name'] = trim($_POST['name']);
    $data['status'] = $_POST['status'];
    if ($obj->check_if_already_exist_master_name($_POST['table'],$data['name'],$_POST['id'])>0) {
        $error[] = $data['name']." already exists";
    }  else {
        if(isset($_POST['id'])) {
            $obj->update_data($_POST['table'], $data, $_POST['id']);
            if($_POST['model_ids']) {
                $model_idss = trim($_POST['model_ids'],",");
                $obj->delete_where("fcode_table_code", "WHERE id IN (".$model_idss.")");
            }
            if($_POST['model_alt_ids']) {
                $model_alt_ids = trim($_POST['model_alt_ids'],",");
                $obj->delete_where("fcode_table_alternative_name", "WHERE id IN (".$model_alt_ids.")");
            }
            $udata['f_table_val'] = $udata_alt['f_table_val'] = $_POST['id'];
        } else {
            $udata['f_table_val'] = $udata_alt['f_table_val'] = $obj->insert_data($_POST['table'], $data);	
        }


    $flag = 0;
    for ($i=0;$i<count($_POST['focus_code']);$i++) {
        $udata['f_table_id'] = 5;
        if ($_POST['focus_code'][$i]) {
            $udata['focus_code'] = $_POST['focus_code'][$i];
            $udata['is_primary'] = ($_POST['is_primary'][$i]?(int)$_POST['is_primary'][$i]:0);
            if($udata['is_primary']) $flag = 1;
            if($_POST['focus_code_id'][$i]>0) {
                $focus_code_id =  $_POST['focus_code_id'][$i];
                $obj->update_data("fcode_table_code", $udata, $_POST['focus_code_id'][$i]);
            } else {
                $focus_code_id =  $obj->insert_data("fcode_table_code", $udata);	
            }
            if ($i==0) $fid = $focus_code_id;
        } 
    }
    //exit;
   
    for ($i=0;$i<count($_POST['alternative_name']);$i++) {
        $udata_alt['name'] = $_POST['alternative_name'][$i];
        $udata_alt['f_table_id'] = 5;
        if ($udata_alt['name']) {
            if ($_POST['alternative_name_id'][$i]>0) {
                $obj->update_data("fcode_table_alternative_name", $udata_alt, $_POST['alternative_name_id'][$i]);
            } else {
                $obj->insert_data("fcode_table_alternative_name", $udata_alt);	
            }   
        }
       
    }
    if ($flag == 0) {
        $udata_code['is_primary'] = 1;
        $obj->update_data("fcode_table_code", $udata_code, $fid);
    }
}
    if (count($error) == 0 ) {
        $obj->con->commit();
    } else {
        setcookie("error_model", json_encode($error), time() + 1);  
    }
  
    header("location:".$obj->thispage());
    exit;
}

$result_code = $result_name = "";
if(isset($_REQUEST['edit_brand'])) {
    $ed_brand = $obj->select_all_by_id("api_brand", $_REQUEST['edit_brand']);
    $result_code = $obj->select_all_by("fcode_table_code","f_table_id='3' AND f_table_val='".$_REQUEST['edit_brand']."' order by id ASC");
    $result_name = $obj->select_all_by("fcode_table_alternative_name","f_table_id='3' AND f_table_val='".$_REQUEST['edit_brand']."' order by id ASC");
    if (mysqli_num_rows($result_name) == 0) {
        $result_name = "";
    }
    if (mysqli_num_rows($result_code) == 0) {
        $result_code = "";
    }
}
?>

<!doctype html>
<html lang="en">
<head>

<? include "common/js_n_cs.php";?>


</head>
<body>
<?php include "common/header.php";?>
<div class="container-fluid body_bg" >
  <div class="d-flex flex-row">

    <?php include "common/nav.php";?>

    <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"  data-simplebar>
      <div class="col-sm-12 col-xs-12 inner-pad" >

      <? include "common/title_bar.php";?>

      <? include "common/settings_sub_nav.php";?>

      


        <div class="row">

            <div class="col-sm-6 col text-white">
                    <h5>BRAND</h5>
                    <hr>
                    <?
                    if(isset($_COOKIE['error_brand'])) {
                        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
                        foreach (json_decode($_COOKIE['error_brand']) as $errors) {
                       
                            echo $errors ."<br/>";
                       }
                       echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>';
                    } 
                    ?>
                        <form action="" method="post">
                        
                            <div class="form-group w-75">
                                <label>NAME</label>
                                <input type="text" class="form-control form-control-sm" name="name"  <? if(isset($ed_brand)){ ?> value="<?=$ed_brand['name']?>" readonly <? } ?> required>
                            </div>
                            <?php
                            if(isset($_REQUEST['edit_brand']) && $result_name){
                                    $i = 1;
                                    while($row_name = $result_name->fetch_assoc()){ 
                                        ?>
                                        <div class="form-group w-75 <?php echo ($i==1?"after-add-brand-name-more":"")?> ">
                                            <div class="add_name_txt">
                                                <label>ALTERNATIVE NAME</label>
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control form-control-sm" name="alternative_name[]"  <? if(isset($ed_brand)){ ?> value="<?=$row_name['name']?>" <? } ?> >
                                                    <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  <? if(isset($ed_brand)){ ?> value="<?=$row_name['id']?>" <? } ?>>
                                                    <?php 
                                                    if($i==1) {
                                                    ?>
                                                    <div class="input-group-append"  onclick="addAlternativeMore('after-add-brand-name-more');">
                                                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                                    </div>
                                                    <?php
                                                    } else {
                                                    ?>
                                                    <div class="input-group-append remove_alt" data-code="<?=$row_name['id']?>" data-ids="brand_alt_ids">
                                                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>  
                                        </div>
                                        <?php
                                    $i++;
                                }
                                
                            } else {
                              

                            ?>    
                            <div class="form-group w-75 after-add-brand-name-more">
                                <label>ALTERNATIVE NAME</label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control form-control-sm" name="alternative_name[]">
                                    <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  value="0">
                                    <div class="input-group-append"  onclick="addAlternativeMore('after-add-brand-name-more');">
                                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div>
                            </div> 
                            <?php
                            }
                            if($result_code){
                                $i = 1;
                                while($row_code = $result_code->fetch_assoc()){ 
                                    ?>
                                    <div class="row add_txt <?php echo ($i==1?"after-add-more":"")?>">
                                    <div class="col-sm-8"> 
                                        <div class="form-group">
                                            <label>F CODE</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control form-control-sm" name="focus_code[]"  <? if(isset($ed_brand)){ ?> value="<?=$row_code['focus_code']?>" <? } ?> required>
                                                <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  <? if(isset($ed_brand)){ ?> value="<?=$row_code['id']?>" <? } ?>>
                                                <?php 
                                                if($i==1) {
                                                ?>
                                                <div class="input-group-append"  onclick="addMore('after-add-more');">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                                </div>
                                                <?php
                                                } else {
                                                ?>
                                                <div class="input-group-append remove" data-code="<?=$row_code['id']?>" data-ids="brand_ids">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label> IS PRIMARY </label>
                                            <div class="input-group">
                                                <input type="radio" class="is_primary" name="is_primary_flag"  value="1" <? if(isset($ed_brand) && $row_code['is_primary'] == 1){ echo "checked";} ?>>
                                                <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  <? if(isset($ed_brand)  && $row_code['is_primary'] == 1 ){ ?> value="1" <? } ?> >
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            <?php
                                $i++;
                                }
                        
                            } else {
                        
                            ?> 
                               
                            <div class="row after-add-more">
                                <div class="col-sm-8"> 
                                    <div class="form-group">
                                        <label>F CODE</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-sm" name="focus_code[]"  required="required">
                                            <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  value="0">
                                            <div class="input-group-append"  onclick="addMore('after-add-more');">
                                                <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                            </div>
                                        </div>
                                    </div> 
                                </div> 
                                <div class="col-sm-4">
                                   <div class="form-group">
                                        <label> IS PRIMARY </label>
                                        <div class="input-group">
                                            <input type="radio" class="is_primary" name="is_primary_flag"  value="1">
                                            <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  value="0">
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            
                            <?php } ?>
                            <div class="copy hide">  
                                <div class="row add_txt">
                                    <div class="col-sm-8"> 
                                        <div class="form-group">
                                            <label>F CODE</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control form-control-sm" name="focus_code[]">
                                                <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  value="0">
                                                <div class="input-group-append remove">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label> IS PRIMARY </label>
                                            <div class="input-group">
                                                <input type="radio" class="is_primary" name="is_primary_flag"  value="1">
                                                <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  id="is_primary" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div> 
                            <div class="copy_alt hide">  
                                <div class="add_name_txt w-75">
                                    <label>ALTERNATIVE NAME</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control form-control-sm" name="alternative_name[]">
                                        <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  value="0">
                                        <div class="input-group-append remove_alt">
                                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <input type="hidden" name="brand_ids" id="brand_ids"/>  
                            <input type="hidden" name="brand_alt_ids" id="brand_alt_ids"/> 
                            <div class="form-group w-75">
                            <label>STATUS</label> <br>
                            <select class="form-control form-control-sm" name="status" id="status" required="required">
                                <option value="">Select Status</option>
                                <option value="1" <? if(isset($ed_brand) && $ed_brand['status']==1){ ?> selected <? } ?>>Active</option>
                                <option value="0" <? if(isset($ed_brand) && $ed_brand['status']==0){ ?> selected <? } ?>>Inactive</option>
                            </select>
                            </div>       
                            <div class="form-group">
                            <label>OWN BRAND</label> <br>
                            <input type="checkbox" class="" name="own_brand"  <? if(isset($ed_brand) && $ed_brand['own_brand']==1){ ?> checked <? } ?> value="1"   >
                            </div>  
                            <div class="form-group">
                            <? if(isset($ed_brand)){ ?>
                            <input type="hidden" name="id" value="<?=$ed_brand['id']?>">
                            <button name="add_brand" type="submit" class="btn btn-info">UPDATE</button>
                            <? }else{ ?>
                            <button name="add_brand" type="submit" class="btn btn-info">ADD</button>
                            <? } ?>
                            <a href="settings_brand" class="btn btn-secondary">CANCEL</a>
                           
                            </div> 
                        </form>
                      
                        <table class="table table-dark table-bordered" data-table="api_brand" id="api_brand" >
                            <thead>
                             <tr>
                                <th>#</th>
                                <th>NAME</th>
                                <th>ALTERNATIVE NAME</th>
                                <th>CODE</th>
                                <th>STATUS</th>
                                <th class="tableexport-ignore">ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                                <? $i=0;
                                    $result = $obj->select_all_table("api_brand", 3," order by api_brand.own_brand desc");
                                    while($row = $result->fetch_assoc()){ 
                                       
                                    $focus_code_arr = ($row['code_id']?explode(",",$row['code_id']):[]);
                                    $alt_name_arr = ($row['alt_name_id']?explode(",",$row['alt_name_id']):[]);
                                    ?>
                                    <tr data-id="<?=$row['id']?>" class=" <? if($row['own_brand']==1) echo 'text-warning' ?>">
                                        <td><?=++$i?></td>
                                        <td><?=$row['name']?></td>
                                        <td>
                                            <? 
                                             $hasComma = false;
                                              if (isset($alt_name_arr)) {
                                                foreach($alt_name_arr as $name_id) { 
                                                    $alt_name_result = $obj->select_all_by_field_id_process('fcode_table_alternative_name','id',$name_id);
                                                    if ($hasComma){ 
                                                        echo ","; 
                                                    }
                                                    echo '<span>'.ucfirst($alt_name_result['name']).'</span>';
                                                    $hasComma=true;
                                                }  
                                              }
                                                
                                            ?>
                                            
                                        </td>
                                       
                                        <? 
                                            $code_arr = [];
                                            foreach($focus_code_arr as $code_id) {  
                                                $fcode_result = $obj->select_all_by_field_id_process('fcode_table_code','id',$code_id);
                                                $primary = "";
                                                if($fcode_result['is_primary'] && count($focus_code_arr)>1) $primary='(Primary)';
                                                $code_arr[$fcode_result['id']] = $fcode_result['focus_code'].$primary;
                                            }  
                                           
                                            if (isset($code_arr)) {
                                              echo '<td class="hide">'.implode(',',$code_arr).'</td>';
                                            }
                                                
                                        ?> <td  class="tableexport-ignore"> 
                                             <?php
                                           
                                            foreach($code_arr as $cid => $cname) {  
                                                $carr = explode("(",$cname);     
                                            ?>
                                            
                                            <div class="codecls_<?=$cid?> tableexport-empty tableexport-ignore">
                                            <input type="text" class="full_box code_input ev hide txt_cls_<?=$cid?>" id="txt_val_<?=$cid?>" value="<?=$carr[0]?>"> 
                                            <span class="cv f_code_<?=$cid?> txt_edit_cls_<?=$cid?>">
                                               <?=$cname?>
                                             
                                            </span>
                                            <i class="fas fa-pen fa-xs cp float-right ev txt_edit_cls_<?=$cid?>" data-toggle='[".cv",".ev"]'  onclick="editable(<?=$cid?>)"></i>
                                            <span class=" cv float-right  hide txt_cls_<?=$cid?>" data-toggle='[".cv",".ev"]'>
                                                <button class="btn btn-xs btn-secondary  mr-2 " onclick="noteditable(<?=$cid?>)">CANCEL</button>
                                                <button class="btn btn-xs btn-primary " onclick="updateTextbox(<?=$cid?>)">UPDATE</button>
                                            </span> 
                                            </div>
                                        <?php
                                            }
                                        
                                        ?>
                                        
                                        </td>
                                        <td >
                                           <span class="badge <?=($row['status']==1?"bg-success":"bg-danger")?> text-white">
                                            <?=($row['status']==1?"ACTIVE":"INACTIVE")?>
                                           </span>
                                        </td>
                                        <td class="tableexport-ignore">
                                            <a href="?edit_brand=<?=$row['id']?>">EDIT</a>
                                        </td>
                                    </tr>
                                <? } ?>
                            </tbody>
                        </table>
                        
            </div>
        
        
            <? 
            $table_array = array(
                                // "api_origin_brand" => "ORIGIN BRAND",
                                "api_model" => "MODEL"
                                ); 
            $result_model_code = $result_model_name = "";                  
            foreach($table_array as $table => $title )   {    
                
                if(isset($_GET['edit']) && $_GET['table'] == $table){
                    $ed = $obj->select_all_by_id($_GET['table'], $_GET['edit']);
                    $result_model_code = $obj->select_all_by("fcode_table_code","f_table_id='5' AND f_table_val='".$_REQUEST['edit']."' order by id ASC");
                    $result_model_name = $obj->select_all_by("fcode_table_alternative_name","f_table_id='5' AND f_table_val='".$_REQUEST['edit']."' order by id ASC");
                    
                    if (mysqli_num_rows($result_model_name) == 0) {
                        $result_model_name = "";
                    }
                    if (mysqli_num_rows($result_model_code) == 0) {
                        $result_model_code = "";
                    }
                } else if(isset($ed)){
                    unset($ed);  
                }
                          

                ?>

                <div class="col-sm-6 col text-white">
                   
                        <h5><?=$title?></h5>
                        <?
                        if(isset($_COOKIE['error_model'])) {
                            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
                            foreach (json_decode($_COOKIE['error_model']) as $errors) {
                        
                                echo $errors ."<br/>";
                        }
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>';
                        } 
                        ?>
                        <hr>
                        <form action="" method="post">
                            <input type="hidden" name="table" value="<?=$table?>">
                            <div class="form-group w-75 ">
                                <label>NAME</label>
                                <input type="text" class="form-control form-control-sm" name="name" <? if(isset($ed)){ ?> value="<?=$ed['name']?>" <? } ?>  required>
                            </div>
                            <?php
                            if(isset($_REQUEST['edit']) && $result_model_name){
                                    $i = 1;
                                    while($row_model_name = $result_model_name->fetch_assoc()){ 
                                        ?>
                                        <div class="form-group <?php echo ($i==1?"after-add-model-name-more":"")?> ">
                                            <div class="add_name_txt w-75">
                                                <label>ALTERNATIVE NAME</label>
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control form-control-sm" name="alternative_name[]"  <? if(isset($ed)){ ?> value="<?=$row_model_name['name']?>" <? } ?> >
                                                    <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  <? if(isset($ed)){ ?> value="<?=$row_model_name['id']?>" <? } ?>>
                                                    <?php 
                                                    if($i==1) {
                                                    ?>
                                                    <div class="input-group-append"  onclick="addAlternativeMore('after-add-model-name-more');">
                                                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                                    </div>
                                                    <?php
                                                    } else {
                                                    ?>
                                                    <div class="input-group-append remove_alt" data-code="<?=$row_model_name['id']?>" data-ids="model_alt_ids">
                                                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>  
                                        </div>
                                        <?php
                                    $i++;
                                }
                                
                            } else {
                            ?>    
                            <div class="form-group w-75  after-add-model-name-more">
                                <label>ALTERNATIVE NAME</label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control form-control-sm" name="alternative_name[]">
                                    <input type="hidden" class="form-control form-control-sm" name="alternative_name_id[]"  value="0">
                                    <div class="input-group-append"  onclick="addAlternativeMore('after-add-model-name-more');">
                                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div>
                            </div> 

                            <?php
                            }
                            if($result_model_code){
                                $i = 1;
                                while($row_model_code = $result_model_code->fetch_assoc()){ 
                                    ?>
                                    <div class="row add_txt <?php echo ($i==1?"after-model-add-more":"")?>">
                                        <div class="col-sm-8"> 
                                            <div class="form-group">
                                                <label>F CODE</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control form-control-sm" name="focus_code[]"  <? if(isset($ed)){ ?> value="<?=$row_model_code['focus_code']?>" <? } ?> required>
                                                    <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  <? if(isset($ed)){ ?> value="<?=$row_model_code['id']?>" <? } ?>>
                                                    <?php 
                                                    if($i==1) {
                                                    ?>
                                                    <div class="input-group-append"  onclick="addMore('after-model-add-more');">
                                                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                                    </div>
                                                    <?php
                                                    } else {
                                                    ?>
                                                    <div class="input-group-append remove" data-code="<?=$row_model_code['id']?>" data-ids="model_ids">
                                                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-minus"></i></span>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div> 
                                        </div> 
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label> IS PRIMARY </label>
                                                <div class="input-group">
                                                    <input type="radio" class="is_primary" name="is_primary_flag"  value="1" <? if(isset($ed) && $row_model_code['is_primary'] == 1){ echo "checked";} ?>>
                                                    <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  <? if(isset($ed)  && $row_model_code['is_primary'] == 1 ){ ?> value="1" <? } ?> >
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <?php
                                $i++;
                                }
                            } else {
                            ?> 
                              <div class="row after-model-add-more">
                                <div class="col-sm-8"> 
                                    <div class="form-group">
                                        <label>F CODE</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-sm" name="focus_code[]"  required="required">
                                            <input type="hidden" class="form-control form-control-sm" name="focus_code_id[]"  value="0">
                                            <div class="input-group-append"  onclick="addMore('after-model-add-more');">
                                                <span class="input-group-text" id="basic-addon2"><i class="fa fa-plus"></i></span>
                                            </div>
                                        </div>
                                    </div> 
                                </div> 
                                <div class="col-sm-4">
                                   <div class="form-group">
                                        <label> IS PRIMARY </label>
                                        <div class="input-group">
                                            <input type="radio" class="is_primary" name="is_primary_flag"  value="1">
                                            <input type="hidden" class="form-control is_primary_flag"  name="is_primary[]"  value="0">
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <? } ?>
                            <input type="hidden" name="model_ids" id="model_ids"/>    
                            <input type="hidden" name="model_alt_ids" id="model_alt_ids"/>   
                            <div class="form-group w-75 ">
                            <label>STATUS</label> <br>
                            <select class="form-control form-control-sm" name="status" id="status" required="required">
                                <option value="">Select Status</option>
                                <option value="1" <? if(isset($ed) && $ed['status']==1){ ?> selected <? } ?>>Active</option>
                                <option value="0" <? if(isset($ed) && $ed['status']==0){ ?> selected <? } ?>>Inactive</option>
                            </select>
                            </div>        
                            <div class="form-group">
                                <? if(isset($ed)){ ?>
                                    <input type="hidden" name="id" value="<?=$ed['id']?>">
                                    <button name="add" type="submit" class="btn btn-info">UPDATE</button>
                                <? }else{ ?>
                                    <button name="add" type="submit" class="btn btn-info">ADD</button>
                                <? } ?>
                                    <a href="<?=$obj->thispage()?>" class="btn btn-secondary">CANCEL</a>
                            </div>
                        </form>

                        <table class="table table-dark table-bordered" data-table="<?=$table?>" >
                            <thead>
                                <th>#</th>
                                <th>NAME</th>
                                <th>ALTERNATIVE NAME</th>
                                <th>CODE</th>
                                <th>STATUS</th>
                                <th>ACTION</th>
                            </thead>
                            <tbody>
                                <? $i=0;
                                    $result = $obj->select_all_table($table, 5, NULL);
                                    while($row = $result->fetch_assoc()){ 
                                        $focus_code_arr = ($row['code_id']?explode(",",$row['code_id']):[]);
                                        $alt_name_arr = ($row['alt_name_id']?explode(",",$row['alt_name_id']):[]);
                                    ?>
                                    <tr data-id="<?=$row['id']?>">
                                        <td><?=++$i?></td>
                                        <td><?=$row['name']?></td>
                                        <td>
                                            <? 
                                              if (isset($alt_name_arr)) {
                                                foreach($alt_name_arr as $name_id) { 
                                                    $alt_name_result = $obj->select_all_by_field_id_process('fcode_table_alternative_name','id',$name_id);
                                                    echo $alt_name_result['name']."<br/>";
                                                }  
                                              }
                                                
                                            ?>
                                        </td>
                                        <td>
                                        <?php
                                        
                                        foreach($focus_code_arr as $code_id) {     
                                            $fcode_result = $obj->select_all_by_field_id_process('fcode_table_code','id',$code_id);
                                            ?>
                                            <div class="codecls_<?=$fcode_result['id']?>">
                                            <input type="text" class="full_box code_input ev hide txt_cls_<?=$fcode_result['id']?>" id="txt_val_<?=$fcode_result['id']?>" value="<?=$fcode_result['focus_code']?>"> 
                                            <span class="cv f_code_<?=$fcode_result['id']?> txt_edit_cls_<?=$fcode_result['id']?>">
                                                <?=$fcode_result['focus_code']?>
                                                <? if($fcode_result['is_primary']  && count($focus_code_arr)>1) echo '<span class="badge badge-primary">Primary</span>'; ?>
                                            </span>
                                            <i class="fas fa-pen fa-xs cp float-right ev txt_edit_cls_<?=$fcode_result['id']?>" data-toggle='[".cv",".ev"]'  onclick="editable(<?=$fcode_result['id']?>)"></i>
                                            <span class="cv float-right  hide txt_cls_<?=$fcode_result['id']?>" data-toggle='[".cv",".ev"]'>
                                                <button class="btn btn-xs btn-secondary  mr-2 " onclick="noteditable(<?=$fcode_result['id']?>)">CANCEL</button>
                                                <button class="btn btn-xs btn-primary" onclick="updateTextbox(<?=$fcode_result['id']?>)">UPDATE</button>
                                            </span> 
                                            </div>
                                        <?php
                                            }
                                        
                                        ?>
                                        </td>
                                        <td>
                                        <span class="badge <?=($row['status']==1?"bg-success":"bg-danger")?> text-white">
                                            <?=($row['status']==1?"ACTIVE":"INACTIVE")?>
                                           </span>
                                        </td>
                                        <td>
                                            <a href="?edit=<?=$row['id']?>&table=<?=$table?>">EDIT</a>
                                        </td>
                                    </tr>
                                <? } ?>
                            </tbody>
                        </table>

                </div>

            <? } ?>





      </div>
      <? include "common/up_icon.php";?>
    </div>
  </div>
</div>
<script src="js/FileSaver.min.js"></script>
<script src="js/xls.core.min.js"></script>
<script src="js/tableexport.js"></script>
<script>
$(document).ready(function(){
    
    $("#api_brand").tableExport({ position: "top",bootstrap: true,formats: ["xlsx","csv"], trimWhitespace: true,fileName:"brand" ,ignoreCSS: ".tableexport-ignore", bootstrap: true,       emptyCSS: ".tableexport-empty",        });
    
});

</script>
</body>

</html>
