<?php
include "db/connect.php";
$obj = new database();
if(isset($_SESSION['fcode_manage_request']) && $_SESSION['fcode_manage_request'] ==0) {
  header("location:" . $obj->cloud_url);
	exit;
}
$page = "fcode";



?>

<!doctype html>
<html lang="en">

<head>

  <? include "common/js_n_cs.php";?>
  <? include("common/data_table.php"); ?>


  <script>
    $(document).ready(function () {
      $('#example').DataTable({
        "ajax": {

          "url": '<?=$obj->base_url?>ajax/code_list',
          "type": "POST",
          'data': function (data) {

            // Append to data

            // data.from = $(".from").val();
            // data.to = $(".to").val();
            // data.status = $(".status").val();

          }

        },
        "serverSide": true,
        "pageLength": 30,
        "bSort": false,
        "bLengthChange": false,
        "autoWidth": false,
        'createdRow': function (row, data, dataIndex) {


          /*			  var password = data[2];
          			  
          			  if(password.includes("bg-danger")){
          			  	$(row).addClass( 'bg-danger' );
          			  }*/

        }
      });
    });


    $(document).on("click", ".search_btn", function () {

      $('#example').DataTable().clear().draw();

    });

    $(document).on("click", ".rest_btn", function () {
      $('#example').DataTable().state.clear();
      window.location = window.location.href.split("?")[0];
    });
  </script>

</head>

<body>
  <?php include "common/header.php";?>
  <div class="container-fluid body_bg ">
    <div class="d-flex flex-row">

      <?php include "common/nav.php";?>

      <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"
        data-simplebar>
        <div class="col-sm-12 col-xs-12 inner-pad ">

          <? include "common/title_bar.php";?>

          <!-- <h6>SEARCH</h6>
        <div class="card">
          <div class="card-body">
            <form action="" method="get">
              <div class="row">

                <div class="col-sm-3">
                    <input type="text" class="f_input" name="code"  placeholder="CODE" >
                </div>

                <div class="col-sm-2">
                    <button class="btn btn-primary btn-block search_btn">SEARCH</button>
                </div>

                <div class="col-sm-2">
                    <a href="request" class="btn btn-secondary btn-block">RESET</a>
                </div>

              </div>

            </form>

          </div>
        </div>
        <hr> -->
        
            <table class="table table-bordered text-white" id="example">
              <thead>
                <tr class="bg-grey">
                  <th>#</th>
                  <th>DATE</th>
                  <th>CODE</th>
                  <th>NAME</th>
                  <th>ARABIC</th>
                  <th>BARCODE</th>
                  <th>DIVISION</th>
                  <th>SUB DIVISION</th>
                  <th>BRAND</th>
                  
                </tr>
              </thead>

            </table>
         






        </div>
        <!-- <? include "common/up_icon.php";?> -->
      </div>
    </div>
  </div>



</body>

<?php include "common/choosen-init.php";?>



</html>