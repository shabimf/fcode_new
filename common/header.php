<nav class="navbar navbar-light fixed-top navbar-dark bg-dark">
    <a class="navbar-brand" href="<?=$obj->base_url?>" tabindex="-1" style="color:#fff">
    <img src="<?=$obj->base_url?>images/fcode_icon.png" tabindex="-1"  height="40px" style="margin-right:15px">  
    <span > FCODE </span>
  </a>

    <div class="notification-drop dropdown pull-right">
        <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/">
          <i class="fa fa-bell notification-bell text-white" aria-hidden="true"></i><span class="btn__badge pulse-button notifi-count"><?=$obj->getUnreadMessageCount()?></span>
        </a>

        <ul class="dropdown-menu dropdown-menu-right notifications" role="menu" aria-labelledby="dLabel">
            
        </ul>

    </div>
  
    <button class="navbar-toggler d-block d-sm-none" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" tabindex="-1">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse " id="navbarNavDropdown">
        <ul class="nav navbar-nav ml-auto">
          <?if(isset($_SESSION['fcode_new_request']) && $_SESSION['fcode_new_request'] ==1 ) { ?>
            <li class="nav-item <?=(isset($page) && $page == 'new request') ? 'active' : ''?>">
                <a class="nav-link" href="index">NEW</a>
            </li>
            <?php } if(isset($_SESSION['fcode_manage_request']) && $_SESSION['fcode_manage_request'] ==1 ) { ?>
                <li class="nav-item <?=(isset($page) && $page == 'request') ? 'active' : ''?>">
                    <a class="nav-link" href="request">REQUEST</a>
                </li>
            <? } if(isset($_SESSION['manage_fcode']) && $_SESSION['manage_fcode'] ==1 ) { ?>
                <li class="nav-item <?=(isset($page) && $page == 'fcode') ? 'active' : ''?>">
                    <a class="nav-link" href="fcode">FCODE</a>
                </li>
            <? } if($obj->isAdmin() == true)  { ?>
                <li class="nav-item <?=(isset($page) && $page == 'settings') ? 'active' : ''?>">
                    <a class="nav-link" href="settings_table_master">SETTINGS</a>
                </li>
                <? } ?>
            <li class="nav-item">
                <a class="nav-link" href="<?=$obj->base_url?>logout">LOGOUT</a>
            </li>
        </ul>
    </div>

    <div class="dropdown d-none d-sm-block">
        <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown">
            <?=$_SESSION['name']?> -
                <?=$_SESSION['branch_name']?>
        </button>
        <div class="dropdown-menu">
            <?if($obj->isAdmin() == true) {?>
            <a class="dropdown-item  <?=(isset($page) && $page == 'user') ? 'active' : ''?>" href="<?=$obj->base_url?>user">Users</a>
            <? }?>
            <a class="dropdown-item" href="<?=$obj->base_url?>logout">Logout</a>
        </div>
    </div>

</nav>
<button class="chatbox-close">
  <i class="fa fa-times fa-2x" aria-hidden="true"></i>
</button>
<section class="chatbox-popup">
  <header class="chatbox-popup__header">
    <aside style="flex:8">
      <h1 class="chat-title">Chat</h1>
    </aside>
    <aside >
      <button class="chatbox-panel-close"><i class="fa fa-times" aria-hidden="true"></i></button>
    </aside>
  </header>
  <main class="chatbox-popup__main chat-box box box-danger direct-chat direct-chat-danger">
    <div class="box-body">
    </div>
  </main>
  <form action="#" class="typing-area" id="typing-area" enctype="multipart/form-data">
    <input id="request_incoming_id" name="request_incoming_id" value="0" type="hidden">
    <footer class="chatbox-popup__footer">
      <aside style="flex:10">
        <textarea type="text"  name="message" placeholder="Type your message here..." class="input-field"  autocomplete="off"></textarea>
      </aside>
      <aside style="color:#888;text-align:center;">
        <input style="display:none" type="file" id="fileupload" accept="image/*" name="image"  />
        <button><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
        <button  id="uploadButton"><i class="fa fa-paperclip icon1"></i></button>
      </aside>
    </footer>
  </form>
</section>
<? $thispage = $obj->currentPage();?>