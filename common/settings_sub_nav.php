<ul class="nav justify-content-end">

        <li class="nav-item ">
            <a class="nav-link <? if($thispage=="settings_table_master") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_table_master">MASTER</a>
        </li>
        <li class="nav-item ">  
            <a class="nav-link " style="padding: 0.5rem 0rem;" >|</a> 
        </li>      
        <li class="nav-item ">
            <a class="nav-link <? if($thispage=="settings") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings">CONFIG</a>
        </li>
        <li class="nav-item ">  
            <a class="nav-link " style="padding: 0.5rem 0rem;" >|</a> 
        </li>      
        <li class="nav-item ">
            <a class="nav-link <? if($thispage=="settings_division") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_division">DIVISION</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <? if($thispage=="settings_brand") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_brand">BRAND</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  <? if($thispage=="settings_spec1") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_spec1">SPEC 1</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  <? if($thispage=="settings_spec2") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_spec2">SPEC 2</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  <? if($thispage=="settings_spec3") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_spec3">SPEC 3</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  <? if($thispage=="settings_spec4") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_spec4">SPEC 4</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  <? if($thispage=="settings_spec5") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_spec5">SPEC 5</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  <? if($thispage=="settings_spec6") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_spec6">SPEC 6</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  <? if($thispage=="settings_spec7") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_spec7">SPEC 7</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  <? if($thispage=="settings_spec8") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_spec8">SPEC 8</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  <? if($thispage=="settings_excel") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_excel">EXCEL</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  <? if($thispage=="settings_excel_pattern") echo 'text-warning'; else echo 'text-primary'; ?>" href="settings_excel_pattern">EXCEL PATTERN</a>
        </li>

</ul>
<hr>