<script src="js/chat.js"></script>
<link rel="stylesheet" href="<?=$obj->base_url?>css/simple-notify.min.css" />
<script src="<?=$obj->base_url?>js/notify.js"></script>
<script src="<?=$obj->base_url?>js/jquery.validate.min.js"></script>
<script>
 var config = {
   '.chosen-select': {
      width: "100%",
      placeholder_text_single: "Select an option",
      search_contains: true,
      enable_split_word_search: true
   },
   '.chosen-select-deselect': {
      allow_single_deselect: true,
      width: "100%"
   },
   '.chosen-select-no-single': {
      disable_search_threshold: 10
   },
   '.chosen-select-no-results': {
      no_results_text: 'Oops, nothing found!'
   },
   '.chosen-select-width': {
      width: "100%"
   },


}
for (var selector in config) {
   $(selector).chosen(config[selector]);
}
$(document).ready(function () {

   function load_unseen_notification(view = '') {
      $.ajax({
         url: "ajax/fetchnotification",
         method: "POST",
         data: {
            view: view
         },
         dataType: "json",
         success: function (data) {
            $('.notifications').html(data.notification);
            $('.notifi-count').html(data.unseen_notification);
         }
      });
   }
   load_unseen_notification();
   $(document).on('click', '.notifications', function () {
      $('.notifi-count').html('');
      load_unseen_notification('yes');
   });
   setInterval(function () {
      load_unseen_notification();
   }, 5000);
});
$.validator.setDefaults({
    ignore: ":hidden:not(select)"
});
$("#requestForm").validate({
   rules: {
      newcode: "required",
      description: "required"
   },
   messages: {
      newcode: "Please enter code",
      description: "Please enter description"
   },
   errorPlacement: function (error, element) {
      var placement = $(element).data('error');
      if (placement) {
         $(placement).append(error)
      } else {
         error.insertBefore(element);
      }
   },
   submitHandler: function (form) {
      $(':disabled').each(function (e) {
        $(this).removeAttr('disabled');
      });
      if($('#target').length>0) {
         $('#target').html2canvas({
            onrendered: function (canvas) {
               //Set hidden field's value to image data (base-64 string)
               $('#img_val').val(canvas.toDataURL("image/png"));
               //Submit the form manually
               //document.getElementById("myForm").submit();
            }
         });
      }
      setTimeout(function () {
         var form = $('#requestForm')[0];
         var formData = new FormData(form);
         $.ajax({
            type: 'post', 
            url: 'ajax/post_request',
            data: formData,
            data: formData,
            processData: false,
            contentType: false,
            success: function (msg) {
               var myArray = msg.split("~~");
               if (myArray[1]) {
               $(".division_select").attr("disabled", true).trigger("chosen:updated");
               $(".request-msg-cls").removeClass('hide');
               $(".request-msg-cls").html("New request sent succesfully (Code : " + myArray[0] + " , Name : " + myArray[1] + ")");
               $('#requestForm')[0].reset();
               $(".f_table_selects").trigger("chosen:updated");
               $(".chosen-select-deselect").trigger("chosen:updated");
                  setTimeout(function () {
                     $('.request-msg-cls').addClass('hide');
                     $('.code_head').html("");
                     $('.name_head').html("");

                     location.href = "index?success=true";
                  }, 5000);
               
               } else {
                  var notify = new Notify({
                     status: "success",
                     title: '',
                     text: "Request was successfully updated.",
                     effect: 'slide',
                     speed: 300,
                     customClass: '',
                     customIcon: '',
                     showIcon: true,
                     showCloseButton: true,
                     autoclose: true,
                     autotimeout: 3000,
                     gap: 20,
                     distance: 20,
                     type: 3,
                     position: 'right top'
                  });
                  setTimeout(function () {
                     // window.location.reload();
                     window.location.href = 'request.php';
                  }, 2000);
               }
            }
         });
      }, 1000);

   }
});

function getCode() {
   var offer = $("#offer").val();
   var form = $('#requestForm')[0];
   var subdivision = $(".subdivision_select").chosen().find("option:selected").val();
   var brand = $(".brand_select").chosen().find("option:selected").val();
   var formData = new FormData(form);
   formData.append('subdivision', subdivision);
   formData.append('brand', brand);
   $('.code_head').html("");
   $('.name_head').html("");
   if(offer==0) {
      $.ajax({
         type: 'POST',
         url: 'ajax/getcode.php',
         data: formData,
         processData: false,
         contentType: false,
         success: function (html) {
            if (html) {
               var arr = html.split("~~");
               if (arr[0]) {
               $('.code_head').html(arr[0]);
               $(".code_head").append('<hr/>');
               }
               if (arr[1]) {
               $('.name_head').html(arr[1]);
               $(".name_head").append('<hr/>');
               }
            }
         }
      });
   }
  
}
</script>