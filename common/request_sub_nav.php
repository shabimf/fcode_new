<ul class="nav justify-content-end">

    <li class="nav-item ">
        <a class="nav-link  cp status_link <?=$status==1 ? 'text-warning' : 'text-primary' ?>" data-status="1" >ALL</a>
    </li>
    <li class="nav-item ">  
        <a class="nav-link " style="padding: 0.5rem 0rem;" >|</a> 
    </li> 
    <li class="nav-item ">
        <a class="nav-link cp status_link  <?=$status==2 ? 'text-warning' : 'text-primary' ?>" data-status="2" >PENDING</a>
    </li>
    <li class="nav-item ">
        <a class="nav-link cp status_link <?=$status==3 ? 'text-warning' : 'text-primary' ?>" data-status="3" >APPROVED</a>
    </li>
    <li class="nav-item ">
        <a class="nav-link cp status_link  <?=$status==4 ? 'text-warning' : 'text-primary' ?>" data-status="4" >REJECTED</a>
    </li>
    <li class="nav-item ">
        <a class="nav-link cp status_link  <?=$status==5 ? 'text-warning' : 'text-primary' ?>" data-status="5" >CANCEL</a>
    </li>
</ul>
<hr>