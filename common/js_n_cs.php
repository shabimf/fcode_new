<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="shortcut icon" type="image/png" href="<?=$obj->base_url?>images/MIS.png"/>
<!-- Bootstrap CSS -->
<script src="<?=$obj->base_url?>js/jquery-3.3.1.js" ></script>
<link rel="stylesheet" href="<?=$obj->base_url?>vendor/bootstrap/css/bootstrap.min.css">
<script src="<?=$obj->base_url?>vendor/bootstrap/js/bootstrap.bundle.min.js" ></script>
<link rel="stylesheet" href="<?=$obj->base_url?>vendor/fontawesome/css/all.min.css">

<link rel="stylesheet" href="<?=$obj->base_url?>vendor/scroller/simplebar.css">
<script src="<?=$obj->base_url?>vendor/scroller/simplebar.min.js"></script>
<link rel="stylesheet" href="<?=$obj->base_url?>vendor/multiselect/bootstrap-multiselect.css" />
<script src="<?=$obj->base_url?>vendor/multiselect/bootstrap-multiselect.js"></script>
<script src="<?=$obj->base_url?>js/jquery-ui.min.js" ></script>


<link rel="stylesheet" href="<?=$obj->base_url?>vendor/choosen/chosen.css">
<script src="<?=$obj->base_url?>vendor/choosen/chosen.jquery.js"></script>


<!-- select2  -->
<!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->

<script src="<?=$obj->base_url?>js/common.js" ></script>

<link href="<?=$obj->base_url?>css/style.css" rel="stylesheet">


<title>FCODE | <?=strtoupper($page)?></title>