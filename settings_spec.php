<?php
include "db/connect.php";
$obj = new database();

if($obj->isAdmin() == false)
    header("location:./");

$page = "settings";

if (isset($_POST['add_pin_type'])) {

    $data = array();
    $data['name'] = $_POST['name'];
    $data['focus_code'] = $_POST['focus_code'];

    if (isset($_POST['id'])) {
        $obj->update_data("api_pin_type", $data, $_POST['id']);
    } else {
        $obj->insert_data("api_pin_type", $data);
    }

    header("location:settings_spec");
    exit;

}

if (isset($_POST['add_capacity'])) {

    $data = array();
    $data['name'] = $_POST['name'];
    $data['focus_code'] = $_POST['focus_code'];

    if (isset($_POST['id'])) {
        $obj->update_data("api_capacity", $data, $_POST['id']);
    } else {
        $obj->insert_data("api_capacity", $data);
    }

    header("location:settings_spec");
    exit;

}

if (isset($_POST['add_color'])) {

    $data = array();
    $data['name'] = $_POST['name'];
    $data['focus_code'] = $_POST['focus_code'];

    if (isset($_POST['id'])) {
        $obj->update_data("api_color", $data, $_POST['id']);
    } else {
        $obj->insert_data("api_color", $data);
    }

    header("location:settings_spec");
    exit;

}

if (isset($_POST['add_quality'])) {

    $data = array();
    $data['name'] = $_POST['name'];
    $data['focus_code'] = $_POST['focus_code'];

    if (isset($_POST['id'])) {
        $obj->update_data("api_quality", $data, $_POST['id']);
    } else {
        $obj->insert_data("api_quality", $data);
    }

    header("location:settings_spec");
    exit;

}

if (isset($_REQUEST['edit_pin_type'])) {
    $ed_pin_type = $obj->select_all_by_id("api_pin_type", $_REQUEST['edit_pin_type']);
}

if (isset($_REQUEST['edit_capacity'])) {
    $ed_capacity = $obj->select_all_by_id("api_capacity", $_REQUEST['edit_capacity']);
}

if (isset($_REQUEST['edit_color'])) {
    $ed_color = $obj->select_all_by_id("api_color", $_REQUEST['edit_color']);
}

if (isset($_REQUEST['edit_quality'])) {
    $ed_quality = $obj->select_all_by_id("api_quality", $_REQUEST['edit_quality']);
}

?>

<!doctype html>
<html lang="en">
<head>

<? include "common/js_n_cs.php";?>


</head>
<body>
<?php include "common/header.php";?>
<div class="container-fluid body_bg" >
  <div class="d-flex flex-row">

    <?php include "common/nav.php";?>

    <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"  data-simplebar>
      <div class="col-sm-12 col-xs-12 inner-pad" >

        <? include "common/title_bar.php";?>

        <? include "common/settings_sub_nav.php";?>




        <div class="row">

                <div class="col-sm-3 col text-white">
                <h5 class="">PIN TYPE</h5>
                <hr>
                    <form action="" method="post">

                        <div class="form-group">
                          <label>NAME</label>
                          <input type="text" class="form-control form-control-sm" name="name"  <? if(isset($ed_pin_type)){ ?> value="<?=$ed_pin_type['name']?>" <? } ?> required>
                        </div>


                        <div class="form-group">
                          <label>F CODE</label>
                          <input type="text" class="form-control form-control-sm" name="focus_code"  <? if(isset($ed_pin_type)){ ?> value="<?=$ed_pin_type['focus_code']?>" <? } ?> required>
                        </div>


                        <div class="form-group">
                          <? if(isset($ed_pin_type)){ ?>
                          <input type="hidden" name="id" value="<?=$ed_pin_type['id']?>">
                          <button name="add_pin_type" type="submit" class="btn btn-info">UPDATE</button>
                          <? }else{ ?>
                          <button name="add_pin_type" type="submit" class="btn btn-info">ADD</button>
                          <? } ?>
                          <a href="settings_spec" class="btn btn-secondary">CANCEL</a>

                        </div>

                    </form>

                    <table class="table table-dark table-bordered" data-table="api_pin_type" >
                        <thead>
                            <th>#</th>
                            <th>NAME</th>
                            <th>CODE</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <? $i=0;
                                $result = $obj->select_all("api_pin_type");
                                while($row = $result->fetch_assoc()){ ?>
                                <tr data-id="<?=$row['id']?>">
                                    <td><?=++$i?></td>
                                    <td><?=$row['name']?></td>
                                    <td>
                                        <input type="text" class="full_box code_input ev hide" value="<?=$row['focus_code']?>">
                                        <span class="cv f_code"><?=$row['focus_code']?></span>

                                        <i class="fas fa-pen fa-xs cp float-right sToggleTdArray ev" data-toggle='[".cv",".ev"]'></i>

                                        <span class="sToggleTdArray cv float-right  hide" data-toggle='[".cv",".ev"]'>
                                            <button class="btn btn-xs btn-secondary  mr-2 ">CANCEL</button>
                                            <button class="btn btn-xs btn-primary  update_code">UPDATE</button>
                                        </span>
                                    </td>
                                    <td>
                                        <a href="?edit_pin_type=<?=$row['id']?>">EDIT</a>
                                    </td>
                                </tr>
                            <? } ?>
                        </tbody>
                    </table>
                </div>


                <div class="col-sm-3 col text-white">
                <h5 >CAPACITY</h5>
                <hr>
                    <form action="" method="post">

                        <div class="form-group">
                          <label>NAME</label>
                          <input type="text" class="form-control form-control-sm" name="name"  <? if(isset($ed_capacity)){ ?> value="<?=$ed_capacity['name']?>" <? } ?> required>
                        </div>


                        <div class="form-group">
                          <label>F CODE</label>
                          <input type="text" class="form-control form-control-sm" name="focus_code"  <? if(isset($ed_capacity)){ ?> value="<?=$ed_capacity['focus_code']?>" <? } ?> required>
                        </div>


                        <div class="form-group">
                          <? if(isset($ed_capacity)){ ?>
                          <input type="hidden" name="id" value="<?=$ed_capacity['id']?>">
                          <button name="add_capacity" type="submit" class="btn btn-info">UPDATE</button>
                          <? }else{ ?>
                          <button name="add_capacity" type="submit" class="btn btn-info">ADD</button>
                          <? } ?>
                          <a href="settings_spec" class="btn btn-secondary">CANCEL</a>

                        </div>

                    </form>

                    <table class="table table-dark table-bordered" data-table="api_capacity" >
                        <thead>
                            <th>#</th>
                            <th>NAME</th>
                            <th>CODE</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <? $i=0;
                                $result = $obj->select_all("api_capacity");
                                while($row = $result->fetch_assoc()){ ?>
                                <tr data-id="<?=$row['id']?>">
                                    <td><?=++$i?></td>
                                    <td><?=$row['name']?></td>
                                    <td>
                                        <input type="text" class="full_box code_input ev hide" value="<?=$row['focus_code']?>">
                                        <span class="cv f_code"><?=$row['focus_code']?></span>

                                        <i class="fas fa-pen fa-xs cp float-right sToggleTdArray ev" data-toggle='[".cv",".ev"]'></i>

                                        <span class="sToggleTdArray cv float-right  hide" data-toggle='[".cv",".ev"]'>
                                            <button class="btn btn-xs btn-secondary  mr-2 ">CANCEL</button>
                                            <button class="btn btn-xs btn-primary  update_code">UPDATE</button>
                                        </span>
                                    </td>
                                    <td>
                                        <a href="?edit_capacity=<?=$row['id']?>">EDIT</a>
                                    </td>
                                </tr>
                            <? } ?>
                        </tbody>
                    </table>
                </div>



                <div class="col-sm-3 col text-white">
                <h5 class="">COLOR</h5>
                <hr>
                    <form action="" method="post">

                        <div class="form-group">
                          <label>NAME</label>
                          <input type="text" class="form-control form-control-sm" name="name"  <? if(isset($ed_color)){ ?> value="<?=$ed_color['name']?>" <? } ?> required>
                        </div>


                        <div class="form-group">
                          <label>F CODE</label>
                          <input type="text" class="form-control form-control-sm" name="focus_code"  <? if(isset($ed_color)){ ?> value="<?=$ed_color['focus_code']?>" <? } ?> required>
                        </div>


                        <div class="form-group">
                          <? if(isset($ed_color)){ ?>
                          <input type="hidden" name="id" value="<?=$ed_color['id']?>">
                          <button name="add_color" type="submit" class="btn btn-info">UPDATE</button>
                          <? }else{ ?>
                          <button name="add_color" type="submit" class="btn btn-info">ADD</button>
                          <? } ?>
                          <a href="settings_spec" class="btn btn-secondary">CANCEL</a>

                        </div>

                    </form>

                    <table class="table table-dark table-bordered" data-table="api_color" >
                        <thead>
                            <th>#</th>
                            <th>NAME</th>
                            <th>CODE</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <? $i=0;
                                $result = $obj->select_all("api_color");
                                while($row = $result->fetch_assoc()){ ?>
                                <tr data-id="<?=$row['id']?>">
                                    <td><?=++$i?></td>
                                    <td><?=$row['name']?></td>
                                    <td>
                                        <input type="text" class="full_box code_input ev hide" value="<?=$row['focus_code']?>">
                                        <span class="cv f_code"><?=$row['focus_code']?></span>

                                        <i class="fas fa-pen fa-xs cp float-right sToggleTdArray ev" data-toggle='[".cv",".ev"]'></i>

                                        <span class="sToggleTdArray cv float-right  hide" data-toggle='[".cv",".ev"]'>
                                            <button class="btn btn-xs btn-secondary  mr-2 ">CANCEL</button>
                                            <button class="btn btn-xs btn-primary  update_code">UPDATE</button>
                                        </span>
                                    </td>
                                    <td>
                                        <a href="?edit_color=<?=$row['id']?>">EDIT</a>
                                    </td>
                                </tr>
                            <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="col-sm-3 col text-white">
                <h5>QUALITY</h5>
                <hr>
                    <form action="" method="post">

                        <div class="form-group">
                          <label>NAME</label>
                          <input type="text" class="form-control form-control-sm" name="name"  <? if(isset($ed_quality)){ ?> value="<?=$ed_quality['name']?>" <? } ?> required>
                        </div>


                        <div class="form-group">
                          <label>F CODE</label>
                          <input type="text" class="form-control form-control-sm" name="focus_code"  <? if(isset($ed_quality)){ ?> value="<?=$ed_quality['focus_code']?>" <? } ?> required>
                        </div>


                        <div class="form-group">
                          <? if(isset($ed_quality)){ ?>
                          <input type="hidden" name="id" value="<?=$ed_quality['id']?>">
                          <button name="add_quality" type="submit" class="btn btn-info">UPDATE</button>
                          <? }else{ ?>
                          <button name="add_quality" type="submit" class="btn btn-info">ADD</button>
                          <? } ?>
                          <a href="settings_spec" class="btn btn-secondary">CANCEL</a>

                        </div>

                    </form>

                    <table class="table table-dark table-bordered" data-table="api_quality" >
                        <thead>
                            <th>#</th>
                            <th>NAME</th>
                            <th>CODE</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <? $i=0;
                                $result = $obj->select_all("api_quality");
                                while($row = $result->fetch_assoc()){ ?>
                                <tr data-id="<?=$row['id']?>">
                                    <td><?=++$i?></td>
                                    <td><?=$row['name']?></td>
                                    <td>
                                        <input type="text" class="full_box code_input ev hide" value="<?=$row['focus_code']?>">
                                        <span class="cv f_code"><?=$row['focus_code']?></span>

                                        <i class="fas fa-pen fa-xs cp float-right sToggleTdArray ev" data-toggle='[".cv",".ev"]'></i>

                                        <span class="sToggleTdArray cv float-right  hide" data-toggle='[".cv",".ev"]'>
                                            <button class="btn btn-xs btn-secondary  mr-2 ">CANCEL</button>
                                            <button class="btn btn-xs btn-primary  update_code">UPDATE</button>
                                        </span>
                                    </td>
                                    <td>
                                        <a href="?edit_quality=<?=$row['id']?>">EDIT</a>
                                    </td>
                                </tr>
                            <? } ?>
                        </tbody>
                    </table>
                </div>




        </div>





      </div>
      <? include "common/up_icon.php";?>
    </div>
  </div>
</div>
</body>
</html>
