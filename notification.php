<?php
include "db/connect.php";
$obj = new database();
$page = "UNREAD notification";
?>
<!doctype html>
<html lang="en">
<head>
  <? include "common/js_n_cs.php";?>
  <? include("common/data_table.php"); ?>
</head>
<body>
  <?php include "common/header.php";?>
  <div class="container-fluid body_bg ">
    <div class="d-flex flex-row">
      <?php include "common/nav.php";?>
      <div class="col-sm-10 col-xs-12 content_box no-padding-lr <?=$_SESSION['container']?>" id="content_box"
        data-simplebar>
        <div class="col-sm-12 col-xs-12 inner-pad ">
          <? include "common/title_bar.php";?>
          <div class="notification-ui_dd-content" id="post-data">
            <?php
                $result = $obj->getNotification(8);
                include('ajax/notification-data.php');
            ?>
           
           
          </div>
          <div class="ajax-load text-center" style="display:none">
            <p><img src="<?=$obj->base_url?>images/loader.gif">Loading More post</p>
         </div>
        </div>
        <!-- <? include "common/up_icon.php";?> -->
      </div>
    </div>
  </div>
</body>
<?php include "common/choosen-init.php";?>

<script type="text/javascript">
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() >= $(document).height()) {
            var last_id = $(".post-id:last").attr("id");
            loadMoreData(last_id);
        }
    });


    function loadMoreData(last_id){
       $.ajax(
        {
            url: 'ajax/loadMoreNotificationData.php?last_id=' + last_id,
            type: "get",
            beforeSend: function()
            {
                $('.ajax-load').show();
            }
        })
        .done(function(data)
        {
            $('.ajax-load').hide();
            $("#post-data").append(data);
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
                alert('server not responding...');
        });
    }
</script>
</html>